<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class ClearCaches extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'caches:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear all Laravel caches:
        php artisan optimize --force
        php artisan cache:clear
        php artisan route:cache (optional)
        php artisan view:clear
        php artisan config:cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $exitCodes = [];

        //$exitCodes[] = Artisan::call('optimize');
        $exitCodes[] = Artisan::call('optimize', ['--force'=>'']);
        $exitCodes[] = Artisan::call('cache:clear');
        //$exitCodes[] = Artisan::call('route:cache');
        $exitCodes[] = Artisan::call('route:clear');
        $exitCodes[] = Artisan::call('view:clear');
        $exitCodes[] = Artisan::call('config:cache');

        $info = in_array(true, $exitCodes) ? 'Somothing wrong!' : 'Caches cleared';
        $this->info($info);
    }
}
