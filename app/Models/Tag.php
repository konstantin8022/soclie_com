<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name','post_id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function generateSearchLink()
    {
        return route('index', ['tag', $this->name]);
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'post_has_tag')->withTimestamps();
    }



    
}
