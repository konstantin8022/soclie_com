<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use File;
use Illuminate\Support\Facades\Storage;
use Image;

class Photo extends Model
{
    const SAVE_TO = '/uploads';

    const EXTENSIONS = ['jpg', 'png', 'jpeg', 'gif', 'mp4' ];

    const NO_IMAGE_NAME = 'no-image.png';

    const MAX_FILE_SIZE = 50000000000;

    const PHOTO_MAX_WIDTH = 1920;

    const PHOTO_MAX_HEIGHT = 1080;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'position', 'i_id', 'i_type', 'i_description' , 'is_from_instagram', 'i_created_time', 'photable_id', 'photable_type', 'post_id', 'user_id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    public function getBiggestSide()
    {
        $image = Image::make(public_path() . $this->getImageSrc());
        $height = $image->height();
        $width = $image->width();

        if ($height > $width)
        {
            $isEqual = false;
            $biggestSide = 'height';
            $number = $height;
        }
        elseif ($width > $height)
        {
            $isEqual = false;
            $biggestSide = 'width';
            $number = $width;
        }
        else
        {
            $isEqual = true;
            $number = $height;
//            $biggestSide = 'height=' . 300 . ' width=' . 300 . '';
            $biggestSide = '';
        }

        $return = [
            'isEqual' => $isEqual,
            'biggestSide' => $biggestSide,
            'number' => 300,
        ];

        return '';
//        return $return;
    }

    public static function findBySrc($src)
    {
        $name = File::name($src);

        if (is_numeric($name) && $model = self::find($name))
            return $model;
        elseif(substr($name, 0, 5) == 'thumb')
        {
            return self::find(substr($name, 5));
        }
        else
            return null;
    }



    public function hasImage()
    {
        return $this->getImageSrc() != self::SAVE_TO . '/' . self::NO_IMAGE_NAME;
    }

    public function getImageSrc()
    {
        $ext = self::getImageExt();
        if (!$ext)
        {
            $imageSrc = self::SAVE_TO . '/' . self::NO_IMAGE_NAME;
        }
        else
        {
            $imageSrc = self::getPathWithPhotoId(false) . ".$ext";
        }

        return $imageSrc;
    }


private function getImageExt()
    {
        $ext = false;
        foreach(self::EXTENSIONS as $e)
        {
            if(File::exists(self::getPathWithPhotoId() . '.' . $e))
            {
                $ext = $e;
                break;
            }
        }
        return $ext;
    }


private function getPathWithPhotoId($withPublicPath = true)
    {
        $imageSrc = self::getPathForImage($withPublicPath);

            if ($this->isInstagramCarousel())
            {
                $imageSrc .= '/instcoll'.$this->id.'/1';
            }
            elseif($this->is_from_instagram)
            {
                $imageSrc .= '/inst'.$this->id;
            }
            else
            {
                $imageSrc .= '/'.$this->id;
            }

        return $imageSrc;
    }



    public function  saveImageFromRequest($image)
    {
        $fullIimage = Image::make($image)->orientate();

        if ($fullIimage->width() > self::PHOTO_MAX_WIDTH || $fullIimage->height() > self::PHOTO_MAX_HEIGHT) {
            $fullIimage->widen(self::PHOTO_MAX_WIDTH);
        }

        $thumbnail = Image::make($image)->fit(96, 96)->orientate();
        //$ext = $image->extension();
        $savePath = self::getPathForImage();
        if (!File::exists($savePath)) File::makeDirectory($savePath, 0755, true);


        if ($fullIimage->save($savePath . '/' .$this->id . ".jpg", 80))
        {
            $thumbnail->save($savePath . '/thumb' . $this->id . '.jpg', 85);
            return $this->id;
        }
        else
        {
            $thumbnail->destroy();
            return false;
        }
    }

    public function getThumbnailSrc()
    {

        $src = self::getPathForImage(false) . '/thumb' . $this->id . '.jpg';
//        $src = self::SAVE_TO . '/' . self::NO_IMAGE_NAME;

        if ($this->isInstagramImage())
            $src = self::getPathForImage(false) . '/thumbinst' . $this->id . '.jpg';


        if(!File::exists(public_path() . $src))
        {
            $src = $this->getImageSrc();
//            $src = self::SAVE_TO . '/' . self::NO_IMAGE_NAME;
        }

        //dd($src);
        return $src;
    }



    public function savePhotoToFilesystem($url, $is_carousel = false)
    {
        if (!$is_carousel) {
            $this->saveImageFromUrl($url, self::getPathForImage() . '/', 'inst' . $this->id);

        } else {
            $i=1;
            foreach ($url as $u) {
                $this->saveImageFromUrl($u, self::getPathForImage() . '/instcoll' . $this->id . '/', $i);
                $i++;
            }
        }
    }


    public function saveVideoToFilesystem($photo_url){

        $this->saveImageFromUrl1($photo->videos->standard_resolution->url,
                self::getPathForImage() . '/',
                $this->id);

    }




    private function saveImageFromUrl1($url, $savePath = false, $name = false)
    {
        if ($savePath == false) {
            $savePath = self::getPathForImage() . '/';
        }

        if ($name == false) {
            $name = $this->id;
        }

        $ext = File::extension($url);
        
        if (in_array($ext, self::EXTENSIONS))
        {
            //$thumbnail = Image::make($url);
            //$width = $thumbnail->width();
            //$height = $thumbnail->height();

//            if ($width > $height)
//                $thumbnail->widen(640);
//                $thumbnail->widen(round($width - $width * 0.2));
//            else
//                $thumbnail->heighten(640);
//                $thumbnail->heighten(round($height - $height * 0.2));


//            $thumbnail = Image::make($url)->fit(300, 300);
//            $thumbnail = Image::make($url)->resizeCanvas(640, 640, 'center', false, '000000');
            if (!File::exists($savePath)) File::makeDirectory($savePath, 0755, true);
            $fullSavePath = $savePath . $name . ".$ext";
           // $thumbnail->save($savePath . 'thumb' . $name . '.jpg', 60);

          //  $fullImage = Image::make($url);
           // $width = $fullImage->width();
            //$height = $fullImage->height();

//            if ($width || $height >= 2000)
//                $fullImage->widen($width - $width * 0.5);

            return $fullImage->save($fullSavePath, 85);
        }
        return false;
    }














    public function savePhotoOrCollectionToFilesystem($photo)
    {
        if ($this->isInstagramCarousel())
        {
            $i=1;
            foreach ($photo->carousel_media as $carouselPhoto)
            {
                $this->saveImageFromUrl($carouselPhoto->images->standard_resolution->url,
                    self::getPathForImage() . '/instcoll'.$this->id . '/',
                    $i);
                $i++;
            }
        }
        elseif($this->is_from_instagram)
        {
            $this->saveImageFromUrl($photo->images->standard_resolution->url,
                self::getPathForImage() . '/',
                'inst'.$this->id);
        }
        elseif($this->isDescriptionImage())
        {
            $this->saveImageFromUrl($photo->images->standard_resolution->url,
                self::getPathForImage() . '/',
                'desc'.$this->id);
        }
        else
        {
            $this->saveImageFromUrl($photo->images->standard_resolution->url,
                self::getPathForImage() . '/',
                $this->id);
        }
    }

    private function saveImageFromUrl($url, $savePath = false, $name = false)
    {
        if ($savePath == false) {
            $savePath = self::getPathForImage() . '/';
        }

        if ($name == false) {
            $name = $this->id;
        }

        $ext = File::extension($url);
        
        if (in_array($ext, self::EXTENSIONS))
        {
            $thumbnail = Image::make($url);
            $width = $thumbnail->width();
            $height = $thumbnail->height();

//            if ($width > $height)
//                $thumbnail->widen(640);
//                $thumbnail->widen(round($width - $width * 0.2));
//            else
//                $thumbnail->heighten(640);
//                $thumbnail->heighten(round($height - $height * 0.2));


//            $thumbnail = Image::make($url)->fit(300, 300);
//            $thumbnail = Image::make($url)->resizeCanvas(640, 640, 'center', false, '000000');
            if (!File::exists($savePath)) File::makeDirectory($savePath, 0755, true);
            $fullSavePath = $savePath . $name . ".$ext";
            $thumbnail->save($savePath . 'thumb' . $name . '.jpg', 60);

            $fullImage = Image::make($url);
            $width = $fullImage->width();
            $height = $fullImage->height();

//            if ($width || $height >= 2000)
//                $fullImage->widen($width - $width * 0.5);

            return $fullImage->save($fullSavePath, 85);
        }
        return false;
    }




    public function deleteImage()
    {
        if ($this->hasImage())
        {
            File::delete(public_path() . self::getThumbnailSrc());
            return File::delete(public_path() . self::getImageSrc());
        }
        return true;
    }

    

    private function getPathForImage($withPublicPath = true)
    {
        if ($this->isDescriptionImage())
        {
            $path = self::SAVE_TO . '/' . $this->user_id . '/post' . $this->photable->id . '/' . $this->getPhotableTypeString();
        }
        else
            $path = self::SAVE_TO . '/' . $this->user_id . '/' . $this->getPhotableTypeString() . $this->photable->id;

        return $withPublicPath ? public_path() . $path : $path;

    }

    

    public function isInstagramCarousel()
    {
        return $this->i_type == 'carousel';
    }

    public function isInstagramImage()
    {
        return $this->i_type == 'image';
    }

    public function isSiteImage()
    {
        return (!isset($this->i_type));
    }

    public function isDescriptionImage()
    {
        return $this->getPhotableTypeString() == 'description';
    }

    public function isCommentImage()
    {
        return $this->getPhotableTypeString() == 'comment';
    }

    private function getPhotableTypeString()
    {
        $type = '';
        switch ($this->photable_type)
        {
            case('App\Models\Post'):
                $type = 'post';
                break;
            case('App\Models\Comment'):
                $type = 'comment';
                break;
            case('App\Models\PostDescription'):
                $type = 'description';
                break;
        }
        return $type;
    }

    public function getInstagramCarouselSrcs($thumbnails = false)
    {
        if ($this->isInstagramCarousel())
        {
            $path = substr($this->getPathWithPhotoId(false), 0, -1);
            $files = File::allFiles(public_path().$path);

            $srcs = [];
            if ($thumbnails)
            {
                foreach ($files as $file)
                {
                    if (!preg_match('~thumb~', $file->getFileName())) continue;
                    $srcs[] = $path . $file->getFilename();
                }
            }
            else
            {
                foreach ($files as $file)
                {
                    if (preg_match('~thumb~', $file->getFileName())) continue;
                    $srcs[] = $path . $file->getFilename();
                }
            }

            return $srcs;
        }
        else return false;
    }

    public function photable()
    {
        return $this->morphTo('photable', 'photable_type', 'photable_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
