<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;

use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'i_login', 'i_real_name', 'i_id','i_count_subscribes','i_count_subscribers',
        'i_publications', 'i_private', 'rating', 'favorites', 'password', 'email', 'synced_at', 'sync_error'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'favorites' => 'array',
    ];
    
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function followedUsers() //  следуют за пользователями  
    {
        return $this->belongsToMany(User::class, 'user_has_user', 'follower_id', 'following_id')->withTimestamps();
    }

    public function followingUsers()  // сами следующие пользователи
    {
        return $this->belongsToMany(User::class, 'user_has_user', 'following_id', 'follower_id')->withTimestamps();
    }

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function follow(User $user)
    {
        $this->followedUsers()->syncWithoutDetaching($user);
    }

    public function unfollow(User $user)
    {
        $this->followedUsers()->detach($user);
    }

    public function isFollower(User $user)   // является последователем ОН
    {
        return $this->followedUsers->contains($user); //contains() определяет, содержит ли коллекция заданное значение:
    }


    public function setProfilePhoto($url)
    {
        $savePath = public_path().'/uploads/' . $this->id . '/';
        if (!File::exists($savePath)) File::makeDirectory($savePath, 0755, true);
        Image::make($url)->save($savePath . 'avatar.jpg');
    }

    public function getAvatarSrc()
    {
        return File::exists(public_path() . '/uploads/' . $this->id . '/avatar.'. 'jpg') ?
            '/uploads/' . $this->id . '/avatar.'. 'jpg' :
            Photo::SAVE_TO . '/' . Photo::NO_IMAGE_NAME;
    }

    public function getFavoritePosts()
    {
        $collection = collect();

        foreach ((array)$this->favorites as $postId)
        {
            $collection->push(Post::findOrFail($postId));
        }

        $collection->sortBy('likes');
        return $collection;
    }

    public function getAllOnlyPosts($by = null)
    {
        return Post::getPostsByParams($by, 'all', true)->where(['po.user_id' => $this->id]);
    }

    public function getPostsLikesCount()
    {
//        $posts = $this->posts()->get();
        $posts = $this->getAllOnlyPosts()->get();
        $count = 0;
        foreach ($posts as $post)
        {
            $count += $post->likes;
        }
        return $count;
    }







    public function getPostsDislikesCount()
    {
//        $posts = $this->posts()->get();
        $posts = $this->getAllOnlyPosts()->get();
        $count = 0;
        foreach ($posts as $post)
        {
            $count += $post->dislikes;
        }
        return $count;
    }

    /**
     * @return int
     */
    public function getUserFavoritesCount()
    {
        $count = 0;
        $count = count((array)$this->favorites);
        return $count;
    }

    /**
     * @return int
     */
    public function getPostsFavoritesCount()
    {
//        $posts = $this->posts()->get();
        $posts = $this->getAllOnlyPosts()->get();
        $count = 0;
        foreach ($posts as $post)
        {
            $count += $post->favorites_count;
        }
        return $count;
    }

    public function getCommentsCount()
    {
//        $posts = $this->posts()->get();
        $posts = $this->getAllOnlyPosts()->get();
        $count = 0;
        foreach ($posts as $post)
        {
            $count += $post->getCommentsCount();
        }
        return $count;
    }

    public function getCommentsLikesCount()
    {
        $comments = $this->comments()->get();

        $likes = 0;
        foreach ($comments as $comment)
        {
            $likes += $comment->getLikesCount();
        }
        return $likes;
    }

    public function getCommentsDislikesCount()
    {
        $comments = $this->comments()->get();

        $likes = 0;
        foreach ($comments as $comment)
        {
            $likes += $comment->getDislikesCount();
        }
        return $likes;
    }

    public function generateSearchLink()
    {
        return route('index', $this->i_login);
    }

    /*
    public function getLikesCount()
    {
        $sql = 'SELECT `likes_users` FROM posts WHERE `user_id`=1';
        $data = DB::select($sql);

        $count = 0;

        foreach ($data as $d)
        {
            $d = is_null($d->likes_users) ? [] : $d->likes_users;
            if(in_array($this->id, json_decode($d)))
                $count++;
        }
        return $count;
    }

    public function getDislikesCount()
    {
        $sql = 'SELECT `dislikes_users` FROM posts WHERE `user_id`=1';
        $data = DB::select($sql);

        $count = 0;

        foreach ($data as $d)
        {
            $d = is_null($d->dislikes_users) ? [] : $d->dislikes_users;
            if(in_array($this->id, json_decode($d)))
                $count++;
        }
        return $count;
    }
    */

    public function getAddingToFavoritesCount()
    {
        $posts = $this->posts()->get();
    }

    public function isUserFavoritesFound(Post $post)
    {
        $postId = $post->id;

        $favourites = is_null($this->favorites) ? [] : $this->favorites;

        return (in_array($postId, $favourites));

    }

    public function addToFavorites(Post $post)
    {
        $favourites = is_null($this->favorites) ? [] : $this->favorites;

        $favourites[] = $post->id;
        $this->favorites = $favourites;
        $this->save();
    }

    public function removeFromFavorites(Post $post)
    {
        $favourites = is_null($this->favorites) ? [] : $this->favorites;

        unset($favourites[array_search($post->id, $favourites)]);
        $this->favorites = $favourites;
        $this->save();
    }

    public function getSyncStatus()
    {
        if ($this->i_private) {
            return 'is-private';
        }

        if (is_null($this->sync_error) && is_null($this->synced_at)) {
            return 'in-progress';

        } elseif (is_null($this->sync_error) && !is_null($this->synced_at)) {
            return 'done';

        } elseif (!is_null($this->sync_error)) {
            return 'error';
        }
    }

    public function getSyncedAt()
    {
        Carbon::setLocale(session('lang'));
        return Carbon::now()->diffForHumans(Carbon::parse($this->synced_at), false, false);
    }

}
