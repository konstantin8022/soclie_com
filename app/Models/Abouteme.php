<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use Auth;
use Mews\Purifier\Facades\Purifier;

class Abouteme extends Model
{
    protected $table = 'aboutemes';

    protected $fillable = [
        'id', 'content', 'user_id',
    ];


 public function setContentAttribute($value)
    {

        $value = preg_replace('~(<br\s*\/?>\s*)+~', '<br />', $value); // replace many <br /> to only one
        if ($value == '<br />') $value = '';

        $this->attributes['content'] = $value;

        //$value = clean($value, 'post_description');
    }





    public function getDescriptionLength()
    {
        return strlen($this->content);
    }

    public function getSmallDescription($length)
    {
        $string = $this->content;
        if (strlen($string) > $length)
        {
            $string = substr($string, 0, $length);
            $string = rtrim($string, "!,.-");
            $string = substr($string, 0, strrpos($string, ' '));
            $string .= "…";
        }
        return $string;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function photos()
    {
        return $this->morphMany(Photo::class, 'photable', 'photable_type', 'photable_id');
    }

    public function saveImage($commentImage)
    {
        $photo = $this->photos()->create([
            'is_from_instagram'=>0,
            'user_id'=>Auth::user()->id,
        ]);

        return $photo->saveImageFromRequest($commentImage);
    }



}
