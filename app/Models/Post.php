<?php

namespace App\Models;

use App\Services\CalculateRatingsService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    const LIKE = 1;
    const DISLIKE = 2;

    const MAX_PHOTOS = 9;

    const POSTS_PER_PAGE = 20;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'likes', 'dislikes', 'i_likes_count', 'i_comments_count', 'i_created_time', 'is_from_instagram', 'i_link', 'likes_users', 'dislikes_users' , 'favorites_count', 'rating', 'is_empty', 'post_description_id' , 'user_id', 'created_at', 'updated_at'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    protected $casts = [
        'likes_users' => 'array',
        'dislikes_users' => 'array'
    ];

    public function description()
    {
        return $this->hasOne(PostDescription::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function photos()
    {
        return $this->morphMany(Photo::class, 'photable', 'photable_type', 'photable_id');
    }

    public function instagramPhotos()
    {
        return $this->morphMany(Photo::class, 'photable', 'photable_type', 'photable_id')
            ->where('is_from_instagram', 1);
    }

    public function instagramAndImagePhotos()
    {
        return $this->morphMany(Photo::class, 'photable', 'photable_type', 'photable_id')
            //->where('i_type', 'NOT LIKE' , 'carousel')
            ->orderBy('position');
    }

    public function instagramCarouselPhotos()
    {
        return $this->morphMany(Photo::class, 'photable', 'photable_type', 'photable_id')
            ->where('i_type', 'carousel');
    }

    public function instagramImagePhotos()
    {
        return $this->morphMany(Photo::class, 'photable', 'photable_type', 'photable_id')
            ->where('i_type', 'image');
    }

    public function imagePhotos()
    {
        return $this->morphMany(Photo::class, 'photable', 'photable_type', 'photable_id')
            ->where('is_from_instagram', 0)->orderBy('position');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'post_has_tag')->withTimestamps();
    }


	public function tagsCount()
	{
  	return $this->tags()->selectRaw('tag_id, count(*) as aggregate')->groupBy('tag_id');
	}




    public static function toprezTags()
    {
        

 /*      $topVal = Tag::join('post_has_tag', 'post_has_tag.tag_id', '=', 'tags.id')
                    ->groupBy('tags.id')
                    ->get(['tags.id', 'tags.name', DB::raw('count(tags.id) as tag_count')])
                    //->orderBy('tag_count', 'desc');
                      ->sortByDesc('tag_count')->take(10);
*/


        $topVal = Post::join('post_has_tag', 'post_has_tag.post_id', '=', 'posts.id')
                    ->where('posts.is_empty','0')
		    ->join('tags', 'tags.id','=','post_has_tag.tag_id')
		    ->groupBy('tags.id')		
                    ->get(['tags.id','tags.name',  DB::raw('count(tags.id) as tag_count')])
                    ->sortByDesc('tag_count')->take(10);
	


/*
	$topVal = Post::with(['tags' => function($query){
		$query->groupBy('tags.id');
		$query-> get(['tags.id','tags.name',  DB::raw('count(tags.id) as tag_count')]);  

} ]) -> get() ;
*/
 

/*
	$topVal =Tag::join('post_has_tag', 'post_has_tag.tag_id', '=', 'tags.id')
		->join('posts', 'posts.id', '=', 'post_has_tag.post_id')
		->where('posts.is_empty','0')
               ->groupBy('tags.id')
               ->get(['tags.id', 'tags.name', DB::raw('count(tags.id) as tag_count')])
               ->sortByDesc('tag_count')->take(10);
*/



			


	return $topVal;              
    }







    public function saveImage($photoImage, $position)
    {
        $photo = $this->photos()->create([
            'is_from_instagram'=>0,
            'user_id'=>Auth::user()->id,
            'position'=>$position,
        ]);

        if ($photoId = $photo->saveImageFromRequest($photoImage))
        {
            $this->is_empty = 0;
            $this->save();
            CalculateRatingsService::calculateUserRating($this->user);
            return $photoId;
        }
        else
            return false;
    }

    public function saveDescriptionImage($photoImage)
    {
        $photo = $this->photos()->create([
            'is_from_instagram'=>0,
            'user_id'=>Auth::user()->id,
        ]);

        return $photo->saveImageFromRequest($photoImage);
    }

    /*
    public function favorites_count
    {
        //todo переделать это после выхода из сандбокса
        $sql = 'SELECT favorites FROM `users` WHERE `id` = 1';
        $data = DB::select($sql);

        $count = 0;

        foreach ($data as $user)
        {
            $favorites = json_decode($user->favorites) ? json_decode($user->favorites) : [];

            if(in_array($this->id, (array)$favorites))
                $count++;
        }

        return $count;
    }*/


    public function getTagsStr()
    {
        $tagsCollection = $this->tags()->get();
        $tags = [];
        foreach ($tagsCollection as $tag)
        {
            $tags[] = $tag->name;
        }
        $tagStr = implode(',', $tags);
        return $tagStr;
    }

    public function getCommentsCount()
    {
        return $this->comments()->count();
    }

    public function getInstagramCommentsCount()
    {
        return $this->comments()->count();
    }

    public function getInstagramLikesCount()
    {
        return $this->comments()->count();
    }

    public function getPhotosCount()
    {
        return $this->imagePhotos()->count();
    }


    /**
     * @param null $by
     * @param null $on
     * @param bool $onlyFullPosts
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function getPostsByParams($by = null, $on = null, $onlyFullPosts = false)
    {
        if(!$on) $on = 'all' ;
        if(!$by) $by = 'instagram';
        
        $sql = self::query();

        $sql->select('po.*')
            ->from(DB::raw('posts as po'));

        switch ($on)
        {
            case 'week':
                $sql->where(DB::raw('po.i_created_time'), '>', \Carbon\Carbon::now()->subWeek()->getTimestamp());
                break;
            case 'month':
                $sql->where(DB::raw('po.i_created_time'), '>', \Carbon\Carbon::now()->subMonth()->getTimestamp());
                break;
            case 'all':
            default: ;
        }

        switch ($by)
        {
//            case 'only_posts':
//                $sql
//                    ->select('po.*')
//                    ->distinct()
//                    ->from(DB::raw('posts as po, photos as ph, post_descriptions as pd'))
//                    ->where('po.id', '=', DB::raw('`ph`.`photable_id`'))
//                    ->where('po.id', '=', DB::raw('`pd`.`id`'))
//                    ->where('ph.photable_type', '=', 'App\Models\Post')
//                    ->where(function($q) {
//                        $q->where('ph.is_from_instagram', '=', 0);
//                            $q->orWhere(function ($q2) {
//                                $q2->where('pd.content', '!=', '')
//                                    ->whereNotNull('pd.content');
//                            });
//                    })
//                    ->orderBy('po.i_created_time', 'desc');
//                break;
            case 'only_posts':
            case 'instagram':
                $sql->orderBy('po.i_created_time', 'desc');
                break;
            case 'im_likes':
                $sql->orderBy('po.i_likes_count', 'i_likes_count');
                break;
            case 'rating':
                $sql->orderBy('po.rating', 'desc');
                break;
            case 'favorites':
                $sql->orderBy('po.favorites_count', 'desc');
                break;
            case 'by_comments': //warning: не сочитается с only_posts
                $sql
                    ->leftJoin('comments', 'po.id', '=', 'comments.post_id')
                    ->select(DB::raw('COUNT(comments.id) as cnt, po.*')) // po.id ?
                    ->groupBy('po.id')
                    ->orderBy('cnt', 'desc');
                break;
            default: $sql->orderBy('po.i_created_time', 'desc');
        }

        if ($onlyFullPosts)
        {
            $sql->where('po.is_empty', '0');
        }

 //       dump($sql->toSql());

        return $sql;
    }

    public function getLinkPart()
    {
        $string = $this->i_link;

        $string = parse_url($string)['path'];

        $string = rtrim($string, '/');
        $string = substr($string, 3);

        return $string;
    }

    public static function getLinkFromPart($part)
    {
        return 'https://www.instagram.com/p/'.$part.'/';
    }

    public function isUserLikeFound(User $user = null)
    {
        if (!$user && Auth::check())
        {
            $user = Auth::user();
        }
        else return false;
        $userId = $user->id;
        $likesUsers = is_null($this->likes_users) ? [] : $this->likes_users;

        return (in_array($userId, $likesUsers));
    }




    public function isUserDislikeFound(User $user = null)
    {
        if (!$user && Auth::check())
        {
            $user = Auth::user();
        }
        else return false;
        $userId = $user->id;
        $dislikesUsers = is_null($this->dislikes_users) ? [] : $this->dislikes_users;
	 //$this->is_empty = 1;
	 //$this->save();

	
        return (in_array($userId, $dislikesUsers));
    }



    public function isPostDislikeFound(User $user = null)
    {
        if (!$user && Auth::check())
        {
            $user = Auth::user();
        }
        else return false;
	
        $userId = $user->id;
	$myem = is_null($this->is_empty) ? []:  $this->is_empty;
	//$myem = is_bool($this->is_empty) ? 1 : 0;
        return $myem;
    }



    public function likeOrDislikeAdd($likeOrDislike, User $user = null)
    {
        if (!$user && Auth::check())
        {
            $user = Auth::user();
        }
        else return false;

        if ($likeOrDislike == self::LIKE)
        {
            $likesUsers = is_null($this->likes_users) ? [] : $this->likes_users;

            $likesUsers[] = $user->id;
            $this->likes_users = $likesUsers;
            $this->likes++;
            $this->save();
        }
        elseif ($likeOrDislike == self::DISLIKE)
        {
            $dislikesUsers = is_null($this->dislikes_users) ? [] : $this->dislikes_users;

            $dislikesUsers[] = $user->id;
            $this->dislikes_users = $dislikesUsers;
            $this->dislikes++;
            $this->save();
        }


    }

    public function likeOrDislikeRemove($likeOrDislike, User $user = null)
    {
        if (!$user && Auth::check())
        {
            $user = Auth::user();
        }
        else return false;


        if ($likeOrDislike == self::LIKE)
        {
            $likesUsers = is_null($this->likes_users) ? [] : $this->likes_users;
            unset($likesUsers[array_search($user->id, $likesUsers)]);
            $this->likes_users = $likesUsers;
            $this->likes--;
            $this->save();
        }
        elseif ($likeOrDislike == self::DISLIKE)
        {
            $dislikesUsers = is_null($this->dislikes_users) ? [] : $this->dislikes_users;
            unset($dislikesUsers[array_search($user->id, $dislikesUsers)]);
            $this->dislikes_users = $dislikesUsers;
            $this->dislikes--;
            $this->save();
        }


    }

}

