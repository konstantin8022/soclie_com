<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Comment extends Model
{
    const LIKE = 1;
    const DISLIKE = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'content', 'is_from_instagram', 'likes_users', 'dislikes_users', 'viewed', 'parent_id', 'user_to_id', 'post_id', 'user_id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];


    protected $casts = [
        'likes_users' => 'array',
        'dislikes_users' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function photos()
    {
        return $this->morphMany(Photo::class, 'photable', 'photable_type', 'photable_id');
    }

    public function replies()   // чат ответ 
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    /**
     * @param User $user
     * @return Builder
     */
    public static function getNewCommentsForUser(User $user)
    {
        $comments = self::select(DB::raw('c.*'))
            ->from('comments as c')
            ->where([['c.user_to_id', '=' ,$user->id], ['c.viewed', '=', 0], ['c.user_id', '!=', $user->id]])
            ->orderBy('c.id', 'desc');

//        $comments = self::select(DB::raw('c.*'))
//            ->from('comments as c')
//            ->join('posts as p', DB::raw('p.id'), '=', DB::raw('c.post_id'))
//            ->where([['p.user_id', '=' ,$user->id], ['c.viewed', '=', 0]])
//            ->orderBy('c.id', 'desc');
        /*
        $comments = self::select(DB::raw('*'))
            ->from('comments')
                ->whereIn('comments.id', function ($query) use ($user)
                {
                    $query->select('comments.id')
                        ->from('comments')
                        ->where('parent_id', '!=', 0)
                        //->where('comments.user_id', '=' ,$user->id)
                        ->where('comments.viewed', 0);
                })
            ->where('comments.user_id', '=' ,$user->id)
            /*->orWhere(function ($query) use ($user)
            {
                $query->select('id')
                    ->from('comments as c', 'posts as p')
                    ->where('posts.id', 'comments.post_id')
                    ->where('posts.user_id', $user->id)
                    ->where('comments.user_id', '!=', $user->id)
                    ->where('comments.parent_id', 0)
                    ->where('comments.viewed', 0);
            })*/
            //->orderBy('comments.id', 'desc');

        return $comments;

    }

    public function saveImage($commentImage)
    {
        $photo = $this->photos()->create([
            'is_from_instagram'=>0,
            'user_id'=>Auth::user()->id,
        ]);

        return $photo->saveImageFromRequest($commentImage);
    }

    public function getFormattedTimeAgoString()
    {
        return $this->created_at->diffForHumans();
    }

    //todo 100% дублирование кода (модель Post)
    public function isUserLikeFound(User $user = null)
    {
        if (!$user && Auth::check())
        {
            $user = Auth::user();
        }
        else return false;
        $userId = $user->id;
        $likesUsers = is_null($this->likes_users) ? [] : $this->likes_users;

        return (in_array($userId, $likesUsers));
    }

    public function isUserDislikeFound(User $user = null)
    {
        if (!$user && Auth::check())
        {
            $user = Auth::user();
        }
        else return false;
        $userId = $user->id;
        $dislikesUsers = is_null($this->dislikes_users) ? [] : $this->dislikes_users;

        return (in_array($userId, $dislikesUsers));
    }

    public function likeOrDislikeAdd($likeOrDislike, User $user = null)
    {
        if (!$user && Auth::check())
        {
            $user = Auth::user();
        }
        else return false;

        if ($likeOrDislike == self::LIKE)
        {
            $likesUsers = is_null($this->likes_users) ? [] : $this->likes_users;

            $likesUsers[] = $user->id;
            $this->likes_users = $likesUsers;
            $this->rating++;
            $this->save();
        }
        elseif ($likeOrDislike == self::DISLIKE)
        {
            $dislikesUsers = is_null($this->dislikes_users) ? [] : $this->dislikes_users;

            $dislikesUsers[] = $user->id;
            $this->dislikes_users = $dislikesUsers;
            $this->rating--;
            $this->save();
        }


    }

    public function likeOrDislikeRemove($likeOrDislike, User $user = null)
    {
        if (!$user && Auth::check()) {
            $user = Auth::user();
        } else return false;


        if ($likeOrDislike == self::LIKE) {
            $likesUsers = is_null($this->likes_users) ? [] : $this->likes_users;
            unset($likesUsers[array_search($user->id, $likesUsers)]);
            $this->likes_users = $likesUsers;
            $this->rating--;
            $this->save();
        } elseif ($likeOrDislike == self::DISLIKE) {
            $dislikesUsers = is_null($this->dislikes_users) ? [] : $this->dislikes_users;
            unset($dislikesUsers[array_search($user->id, $dislikesUsers)]);
            $this->dislikes_users = $dislikesUsers;
            $this->rating++;
            $this->save();
        }
    }

    public function getLikesCount()
    {
        return count((array)$this->likes_users);
    }

    public function getDislikesCount()
    {
        return count((array)$this->dislikes_users);
    }

    public function withoutTimestamps()
    {
        $this->timestamps = false;
        return $this;
    }

}
