<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = session('lang');
        if (isset($locale))
        {
            App::setLocale($locale);
            Carbon::setLocale(substr($locale, 0, -1));//todo сделать универсальным
        }

        return $next($request);
    }
}
