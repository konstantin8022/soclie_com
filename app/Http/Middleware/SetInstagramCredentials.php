<?php

namespace App\Http\Middleware;

use Config;
use Closure;
use Illuminate\Support\Facades\App;

class SetInstagramCredentials
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $clientId = session('clientId', '');
        $clientSecret = session('clientSecret', '');

        Config::set('instagram.clientId', $clientId);
        Config::set('instagram.clientSecret', $clientSecret);

        Config::set('services.instagram.clientId', $clientId);
        Config::set('services.instagram.clientSecret', $clientSecret);

        return $next($request);
    }
}
