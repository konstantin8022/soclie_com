<?php

namespace App\Http\Controllers\Cabinet\User;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class Follow extends Controller
{
    public function __invoke(Request $request)
    {
        $user = User::findOrFail($request->id);
        Auth::user()->follow($user);

        return redirect()->back();//->with('messsage', 'Зафоловили =)');
    }
}
