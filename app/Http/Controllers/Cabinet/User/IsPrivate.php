<?php

namespace App\Http\Controllers\Cabinet\User;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Queue;

use App\Jobs\ParseJob;

class IsPrivate extends Controller
{
    public function __invoke(Request $request)
    {
        $user = Auth::user();

        if ($user->i_private) {
            Queue::push(new ParseJob($user));
        }

        return response()->json([
            'is_private' => $user->i_private
        ]);
    }
}
