<?php

namespace App\Http\Controllers\Cabinet;

use App\Models\Post;
use App\Models\PostDescription;
use App\Models\Photo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ShowCatalog extends Controller
{
    public function __invoke(Request $request, $slug1 = null, $slug2 = null)
    {
        $user = Auth::user();

        $defaults = [
            'by' => 'instagram',
            'on' => 'all',
        ];
        $numOfLeftSlugs = 2;

        if ($slug1) {
            $onlyFullPosts = ($slug1 == 'only_posts');
            $posts = Post::getPostsByParams($slug1, $slug2, $onlyFullPosts)->where(['po.user_id'=>$user->id])->paginate(Post::POSTS_PER_PAGE); //todo пагинация
            //$numOfLeftSlugs = 2;
            $defaults['filter1'] = $slug1;
            $defaults['filter2'] = $slug2;

        } else {
            $onlyFullPosts = ($defaults['by'] == 'only_posts');
            $posts = Post::getPostsByParams($defaults['by'], $defaults['on'], $onlyFullPosts)->where(['po.user_id'=>$user->id])->paginate(Post::POSTS_PER_PAGE); //todo пагинация
        }

        /*
        if ($request->by == 'only_posts')
            $posts = Post::getPostsByParams($request->by, $request->on, true)->where(['po.user_id'=>$user->id])->get(); //todo пагинация
        elseif (!$request->has('by'))
            $posts = Post::getPostsByParams('only_posts', $request->on, true)->where(['po.user_id'=>$user->id])->get(); //todo пагинация
        else
            $posts = Post::getPostsByParams('only_posts', $request->on)->where(['po.user_id'=>$user->id])->get(); //todo пагинация
        */

        $lastPosts = Post::where('user_id', '=', $user->id)
            ->with(['photos' => function($q) {
                $q->where('is_from_instagram', '=', true);
                $q->orderBy('updated_at', 'DESC');

            }])
            ->limit(10)
            ->get();

        $data = [
            'user'=>$user,
            'posts'=>$posts,
            'defaults'=>$defaults,
            'numOfLeftSlugs'=>$numOfLeftSlugs,
            'lastPosts' => $lastPosts

        ];

        if ($request->ajax()) {
            $html = view('cabinet.partials.pagination-item', $data)->render();
            $lastPage = empty($html) ? true : ($posts->currentPage() == $posts->lastPage());

            $response = [
                'html' => $html,
                'last_page' => $lastPage,
            ];
            return response()->json($response);
        }
        
        //todo убрать нагромождение лишних запросов
        return view('cabinet.showcatalog', $data);
    }
}
