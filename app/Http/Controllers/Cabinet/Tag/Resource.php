<?php

namespace App\Http\Controllers\Cabinet\Tag;

use App\Http\Requests\TagStoreRequest;
use App\Models\Post;
use App\Models\Tag as TagModel;
use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Resource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // return 'index';
        $tags = TagModel::all();
        return view('main.announce.index', ['tags' => $tags]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagStoreRequest $request)
    {
        $data = $request->except('_token');

        $postId = $data['post_id'];
        $name = $data['name'];

        $post = Post::findOrFail($postId);
        $tag = Tag::where('name', $name)->first();

        if ($tag)
        {
            $post->tags()->attach($tag->id);
        }
        else
        {
            $post->tags()->create([
                'name'=> $name,
            ]);
        }


        return response()->json(['message'=> __('cabinet.tag_added')]);

        //return redirect()->back()->with('message', 'create.succsess');
        //return redirect()->route('cabinet.showcatalog')->with('message', 'tag.success');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Resource $tag)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Resource $tag)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resource $tag)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //todo TagDestroyRequest, только свои теги!
        $post = Post::findOrFail($request->post_id);
        $tag = $post->tags()->where('name', $request->name)->first();
        //$tagId = $tag->id;

        if ($tag)
        {
            $post->tags()->detach($tag->id);
        }

        return response()->json(['message'=> __('cabinet.tag_removed')]);

        //TagModel::findOrFail($tagId)->delete();

        //return response()->json($tagId);
    }
}
