<?php

namespace App\Http\Controllers\Cabinet\Photo;

use App\Http\Requests\PhotoStoreRequest;
use App\Models\Photo as PhotoModel;
use App\Services\CalculateRatingsService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class Resource extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photo = Auth::user()->announces()->orderBy('id', 'desc')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhotoStoreRequest $request)
    {
       /* dump( $request -> all());
        if($request->isMethod('post') && $request->hasFile('photo') && $request->file('photo')->isValid() && $request->has('post_id'))
        {
            $photoImage = $request->photo;

            $post = Auth::user()->posts()->where('id', $request->post_id)->first();

        }
    */
        //todo return redirect()->back()->with('message', 'create.succsess');
        //return redirect()->route('cabinet.index')->with('message', 'photo.success');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function show(Resource $photo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function edit(Resource $photo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Resource $photo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Photo  $photo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resource $photo)
    {
        $post = $photo->photable;

        if ($photo->hasImage())
        {
            $photo->deleteImage();
        }
        $photo->delete();

        if (empty($post->description->content) && !$post->getPhotosCount())
        {
            $post->is_empty = 1;
            $post->save();
            CalculateRatingsService::calculateUserRating($post->user);
        }
    }

}
