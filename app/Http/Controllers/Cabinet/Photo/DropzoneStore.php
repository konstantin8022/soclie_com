<?php

namespace App\Http\Controllers\Cabinet\Photo;

use App\Http\Requests\DropzoneStoreRequest;
use App\Models\Photo;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class DropzoneStore extends Controller
{
    public function __invoke(DropzoneStoreRequest $request)
    {
        //todo перенести проверку в DropzoneStoreRequest
        if($request->hasFile('photo') && $request->file('photo')->isValid() && $request->has('post_id'))
        {
            $post = Auth::user()->posts()->where('id', $request->post_id)->first();

            $message = __('layout.many_images');

            $photoCount = $post->getPhotosCount();

            if ($photoCount >= Post::MAX_PHOTOS)
            {
                return response()->json(['message'=>$message]);
            }


            $photoImage = $request->file('photo');
            $srcs = $ids = [];
            //foreach ($photoImages as $photoImage)
            //{
                if (in_array($photoImage->extension(), Photo::EXTENSIONS))
                {
                    if ($photoCount <= Post::MAX_PHOTOS)
                    {
                        $photoCount++;
                        $photoId = $post->saveImage($photoImage, $photoCount);
                        $photo = Photo::findOrFail($photoId);
                        $srcs[] = $photo->getThumbnailSrc();
                        $ids[] = $photoId;
                    }
                    else
                    {
                        if (!empty($srcs))
                            return response()->json(['ids'=>$ids, 'success'=>$srcs,'message'=>$message]);
                        else
                            return response()->json(['message'=>$message]);
                    }
                }
           // }

            return response()->json(['ids'=>$ids, 'success'=>$srcs]);
        }
    }
}
