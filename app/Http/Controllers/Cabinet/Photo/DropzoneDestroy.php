<?php

namespace App\Http\Controllers\Cabinet\Photo;

use App\Http\Requests\DropzoneDestroyRequest;
use App\Models\Photo;
use App\Services\CalculateRatingsService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DropzoneDestroy extends Controller
{
    public function __invoke(DropzoneDestroyRequest $request)
    {
        $photo = Photo::findBySrc($request->src);

        if ($photo)
        {
            $post = $photo->photable;

            if ($photo->hasImage())
            {
                $photo->deleteImage();
            }
            $photo->delete();

            if (empty($post->description->content) && !$post->getPhotosCount())
            {
                $post->is_empty = 1;
                $post->save();
                CalculateRatingsService::calculateUserRating($post->user);
            }

            $message = 'delete ok';
        }
        else $message = 'error deleting!'; //todo localize

        return response()->json($message);
    }
}