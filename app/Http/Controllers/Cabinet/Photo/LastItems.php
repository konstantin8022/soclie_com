<?php

namespace App\Http\Controllers\Cabinet\Photo;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class LastItems extends Controller
{
    public function __invoke(Request $request)
    {
        $last_ids = $request->input('last_ids', false);

        $user = Auth::user();

        $lastPhotos = [];

        $lastPosts = Post::where('user_id', '=', $user->id)
            ->with(['photos' => function($q) use ($last_ids) {
                $q->where('is_from_instagram', '=', true);
                $q->orderBy('updated_at', 'DESC');

                if ($last_ids) {
                    $q->whereNotIn('id', $last_ids);
                }

            }])
            ->limit(10)->get();
        
      //  dd($lastPosts);

        foreach ($lastPosts as $lastPost) {

            foreach ($lastPost->photos()->get() as $photo) {

                $lastPhotos[] = [
                    'id' => $photo->id,
                    'src' => $photo->getThumbnailSrc(),
                    'url' => route('showpost', $lastPost->getLinkPart()),
                    'updated' => \Carbon\Carbon::parse($photo->updated_at)->diffForHumans(null, false, true)
                ];

            }

        }

        return response()->json(['photos'=> $lastPhotos]);
    }
}
