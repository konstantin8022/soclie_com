<?php

namespace App\Http\Controllers\Cabinet\Photo;

use App\Models\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DropzonePhotoMove extends Controller
{
    public function __invoke(Request $request)
    {
        $photoIds = $request->arr;
        $photoIdsStr = implode(',',$request->arr);

        $photos = Photo::query()->whereIn('id', $photoIds)->orderByRaw('FIND_IN_SET(id,?)',$photoIdsStr)->get();


        /*
        $photos = collect();
        foreach ($photoIds as $photoId)
        {
            $photos->push(Photo::find($photoId));
        }
            */

        $i = 1; //todo сделать это sql-insert-ом
        foreach ($photos as $photo)
        {
            $photo->position = $i;
            $photo->save();
            $i++;
        }

        return $photos; //локализовать
    }
}
