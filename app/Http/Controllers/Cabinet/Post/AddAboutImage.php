<?php

namespace App\Http\Controllers\Cabinet\Post;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\PostDescription;
use App\Models\Photo;
class AddAboutImage extends Controller
{
    public function __invoke(Request $request)
    {
        if($request->hasFile('imgs'))
        {
            $postDescription = PostDescription::findOrFail($request->post_id);
            $images = $request->file('imgs');
            $srcs = [];

            $i = 0;
            foreach ($images as $image)
            {
                if (in_array($image->extension(), Photo::EXTENSIONS) && $image->getClientSize() <= Photo::MAX_FILE_SIZE)
                {
                    $photoId = $postDescription->saveImage($image);
                    $photo = Photo::findOrFail($photoId);
                    $srcs[$i]['thumb'] = $photo->getThumbnailSrc();
                    $srcs[$i]['full'] = $photo->getImageSrc();
                    $i++;
                }
                else return response()->json(['error'=> __('layout.large_image'), 'label'=> __('cabinet.choose_file')]);
            }
            return response()->json(['srcs'=>$srcs, 'label'=> __('cabinet.choose_file')]);
        }
        else return 'error';
    }
}

