<?php

namespace App\Http\Controllers\Cabinet\Post;

use Illuminate\Http\Request;

class ShowPost extends Controller
{
    public function __invoke()
    {
        return view('index');
    }
}
