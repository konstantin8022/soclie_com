<?php

namespace App\Http\Controllers\Cabinet\Post;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
//use App\Events\onPostDislikeSet;
//use App\Events\onPostDislikeUnset;
//use App\Events\onPostLikeSet;
//use App\Events\onPostLikeUnset;
use App\Models\Post;

class PostDislike extends Controller
{
	public function __invoke(Request $request)
    {
	$user = Auth::user();
        $post = Post::findOrFail($request->id);
        $message = '';
	//if ($request->has('is_empty'))
        if(!$post->isPostDislikeFound())
	{
	   $post->is_empty=1;
	   $post->save();			
          //  event(new onPostDislikeUnset($post, $user));
            $message = __('layout.dislikepost.set');
        }
        else
        {
            //event(new onPostDislikeSet($post, $user));
	    $post->is_empty=0;
	    $post->save();			
            $message = __('layout.dislikepost.unset');
        }

        //if ($post->isUserLikeFound())
       // {
         //   event(new onPostLikeUnset($post, $user));
       // }

        return response()->json(['status' => 'success', 'message'=> $message, 'likes'=> $post->likes, 'dislikes'=>$post->dislikes, 'rating'=>$post->rating]);

    }    
}
