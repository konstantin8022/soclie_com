<?php

namespace App\Http\Controllers\Cabinet\Post;

use App\Events\onPostLikeSet;
use App\Events\onPostLikeUnset;
use App\Events\onPostDislikeSet;
use App\Events\onPostDislikeUnset;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PostLikeSet extends Controller
{
    public function __invoke(Request $request)
    {
        $user = Auth::user();
        $post = Post::findOrFail($request->id);
        $message = '';
        if ($post->isUserLikeFound())
        {
            event(new onPostLikeUnset($post, $user));
            $message = __('layout.like.unset');
        }
        else
        {
            $message = __('layout.like.set');
            event(new onPostLikeSet($post, $user));
        }

        if ($post->isUserDislikeFound())
        {
            event(new onPostDislikeUnset($post, $user));
        }

        return response()->json(['status' => 'success', 'message'=> $message, 'likes'=> $post->likes, 'dislikes'=>$post->dislikes, 'rating'=>$post->rating]);
    }
}
