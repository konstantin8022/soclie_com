<?php

namespace App\Http\Controllers\Cabinet\Post;

use App\Events\onAddPostToFavouritesEvent;
use App\Events\onRemovePostFromFavouritesEvent;
use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AddPostToFavorites extends Controller
{
    public function __invoke(Request $request)
    {
        $user = Auth::user();
        $post = Post::findOrFail($request->id);

        $message = '';
        if ($user->isUserFavoritesFound($post))
        {
            event(new onRemovePostFromFavouritesEvent($post, $user));
            $message = __('layout.favorites.unset');
        }
        else
        {
            event(new onAddPostToFavouritesEvent($post, $user));
            $message = __('layout.favorites.set');
        }

        return response()->json(['status' => 'success', 'message'=> $message, 'favorites'=> $post->favorites_count, 'rating'=>$post->rating]);

    }
}
