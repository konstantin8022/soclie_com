<?php

namespace App\Http\Controllers\Cabinet\Post;

use App\Events\onPostDislikeSet;
use App\Events\onPostDislikeUnset;
use App\Events\onPostLikeSet;
use App\Events\onPostLikeUnset;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class PostDislikeSet extends Controller
{
    public function __invoke(Request $request)
    {
        $user = Auth::user();
        $post = Post::findOrFail($request->id);
        $message = '';
        if ($post->isUserDislikeFound())
        {
            event(new onPostDislikeUnset($post, $user));
            $message = __('layout.dislike.unset');
        }
        else
        {
            event(new onPostDislikeSet($post, $user));
            $message = __('layout.dislike.set');
        }

        if ($post->isUserLikeFound())
        {
            event(new onPostLikeUnset($post, $user));
        }

        return response()->json(['status' => 'success', 'message'=> $message, 'likes'=> $post->likes, 'dislikes'=>$post->dislikes, 'rating'=>$post->rating]);
    }
}
