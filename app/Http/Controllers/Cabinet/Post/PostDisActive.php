<?php

namespace App\Http\Controllers\Cabinet\Post;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use App\Events\onPostDislikeSet;
use App\Events\onPostDislikeUnset;
use App\Events\onPostLikeSet;
use App\Events\onPostLikeUnset;
use App\Models\Post;

class PostDisActive extends Controller
{
    public function __invoke(Request $request)
    {


    	$user = Auth::user();
        $post = Post::findOrFail($request->id);
        $message = '';


         if(  $request->has('is_empty') )
        {
            $post->is_empty = 1;
        }
        else
        {
            $post->is_empty = 0;
        }

        $post->save();

        CalculateRatingsService::calculateUserRating($post->user);

        return 'server ok';
    }	
}
