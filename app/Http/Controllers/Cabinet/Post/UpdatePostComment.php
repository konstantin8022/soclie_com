<?php

namespace App\Http\Controllers\Cabinet\Post;

use App\Models\Post;
use App\Services\CalculateRatingsService;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UpdatePostComment extends Controller
{
    public function __invoke(Request $request)
    {
        /*
        $files = File::allFiles(public_path().$path);

        $srcs = [];
        foreach ($files as $file)
        {
            $srcs[] = $path . $file->getFilename();
        }
        return $srcs;
        */

        $content = $request->input('content');
        $postId = $request->post_id;

        $post = Post::findOrFail($postId);

        $description = $post->description;

        $description->content = $content;
        $description->save();

        if (empty($description->content) || $post->getPhotosCount() == 0)
        {
            $post->is_empty = 1;
        }
        else
        {
            $post->is_empty = 0;
        }

        $post->save();

        CalculateRatingsService::calculateUserRating($post->user);

        return 'server ok';
    }
}
