<?php

namespace App\Http\Controllers\Cabinet;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class ShowRating
 * @package App\Http\Controllers\Cabinet
 */
class ShowRating extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        $users = User::orderBy('rating', 'desc')->get();

        return view('cabinet.showrating', ['users'=>$users]);
    }
}