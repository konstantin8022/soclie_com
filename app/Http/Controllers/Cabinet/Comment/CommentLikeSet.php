<?php

namespace App\Http\Controllers\Cabinet\Comment;

use App\Events\onCommentRatingDownUnset;
use App\Events\onCommentRatingUpSet;
use App\Events\onCommentRatingUpUnset;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

/**
 * Class CommentLikeSet
 * @package App\Http\Controllers\Cabinet\Comment
 */
class CommentLikeSet extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(Request $request)
    {
        $user = Auth::user();
        $comment = Comment::findOrFail($request->comment_id);

        $message = '';
        if ($comment->isUserLikeFound())
        {
            event(new onCommentRatingUpUnset($comment, $user));
            $message = __('layout.like.unset');
        }
        else
        {
            event(new onCommentRatingUpSet($comment, $user));
            $message = __('layout.like.set');
        }

        if ($comment->isUserDislikeFound())
        {
            event(new onCommentRatingDownUnset($comment, $user));
        }

      //  dd();
        return response()->json(['status' => 'success', 'message' => $message, 'rating' => $comment->rating]);
    }
}
