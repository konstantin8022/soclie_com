<?php

namespace App\Http\Controllers\Cabinet\Comment;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MarkAsViewed extends Controller
{
    public function __invoke(Request $request)
    {
        $comment = Comment::findOrFail($request->comment_id);
        $comment->viewed = 1;
        $comment->save();

        return redirect()->back();
    }
}
