<?php

namespace App\Http\Controllers\Cabinet\Comment;

use App\Models\Comment;
use App\Models\Photo;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AddCommentImage extends Controller
{
    public function __invoke(Request $request)
    {
        if($request->hasFile('imgs'))
        {
            $comment = Comment::findOrFail($request->comment_id);
            $images = $request->file('imgs');
            $srcs = [];

            $i = 0;
            foreach ($images as $image)
            {
                if (in_array($image->extension(), Photo::EXTENSIONS) && $image->getClientSize() <= Photo::MAX_FILE_SIZE)
                {
                    $photoId = $comment->saveImage($image);
                    $photo = Photo::findOrFail($photoId);
                    $srcs[$i]['thumb'] = $photo->getThumbnailSrc();
                    $srcs[$i]['full'] = $photo->getImageSrc();
                    $i++;
                }
                else return response()->json(['error'=> __('layout.large_image'), 'label'=> __('cabinet.choose_file')]);
            }
            return response()->json(['srcs'=>$srcs, 'label'=> __('cabinet.choose_file')]);
        }
        else return '$request->hasFile(\'imgs\') was return false';
    }
}
