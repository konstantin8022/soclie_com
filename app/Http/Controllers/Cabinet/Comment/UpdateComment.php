<?php

namespace App\Http\Controllers\Cabinet\Comment;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UpdateComment extends Controller
{
    public function __invoke(Request $request)
    {
        /*
        $files = File::allFiles(public_path().$path);

        $srcs = [];
        foreach ($files as $file)
        {
            $srcs[] = $path . $file->getFilename();
        }
        return $srcs;
        */

        $content = $request->input('content');
        $commentId = $request->comment_id;

        $comment = Comment::findOrFail($commentId);

        $comment->content = $content;
        $comment->save();

        return 'server ok';
    }
}
