<?php

namespace App\Http\Controllers\Cabinet\Comment;

use App\Http\Requests\CommentStoreRequest;
use App\Models\Comment as CommentModel;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Photo;

class Resource extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentStoreRequest $request)
    {
        if($request->isMethod('post') && $request->has('post_id') && $request->has('comment'))
        {
            $parent_id = (int)$request->parent_id;

            $post = Post::where('id', $request->post_id)->firstOrFail();

            $parentComment = null;
            if ($parent_id)
            {
                $parentComment = Comment::findOrFail($parent_id);
            }

            $userId = $parentComment ? $parentComment->user_id : $post->user_id;


                $comment = $post->comments()->create([
                    'is_from_instagram'=> 0,
                    'content'=> (string)$request->input('comment'),
                    'parent_id' => $parent_id,
                    'user_to_id' => $userId,
                    'user_id' => Auth::user()->id,
                ]);

                $response = [
                    'html' => view('home.partials.comment', ['comment'=>$comment, 'parent_id'=> $parent_id, 'post'=>$post])->render(),
                    'id' => $comment->id,
                ];
                return response()->json($response);

                /*
                if ($request->hasFile('photo') && $request->file('photo')->isValid())
                {
                    $commentImage = $request->photo;
                    $comment->saveImage($commentImage);
                }*/
        }

        //return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(CommentModel $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(CommentModel $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CommentModel $comment)
    {
        $data = $request->except('_token');

        $comment->content = $data['comment'];
        $comment->save();
		$parent = $comment->parent()->first();
        $parent_id = empty($parent) ? 0 : $parent->id;
        $post = $comment->post;

        $response = [
            'html' => view('home.partials.comment', ['comment'=>$comment, 'parent_id'=> $parent_id, 'post'=>$post])->render(),
            'id' => $comment->id,
        ];
        return response()->json($response);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommentModel $comment)
    {
        if ($comment->delete())
            return 'SERVER delete ok';
        else return 'SERVER ERROR DELETING';
    }
}
