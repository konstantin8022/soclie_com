<?php

namespace App\Http\Controllers\Cabinet;

use DB;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ShowComments extends Controller
{
    public function __invoke()
    {
        $comments = Comment::getNewCommentsForUser(Auth::user())->get();

        $view = view('cabinet.showcomments', ['comments'=>$comments]);

        //$comments = Comment::getNewCommentsForUser(Auth::user())->update(['comments.viewed'=>1]);

        //$sql = 'update `comments` inner join `posts` on `comments`.`post_id` = `posts`.`id` set `comments`.`viewed` = 1 where (`posts`.`user_id` = 1 and `comments`.`viewed` = 0)';
        //DB::update($sql);

        return $view;
    }
}
