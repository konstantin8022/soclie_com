<?php

namespace App\Http\Controllers\Cabinet;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ShowFollows extends Controller
{
    public function __invoke(Request $request, $slug1 = null, $slug2 = null)
    {
        $defaults = [
            'by' => 'instagram',
            'on' => 'all',
            'filter1' => $slug1,
            'filter2' => $slug2,
        ];

        $users = Auth::user()->followedUsers()->get();
        $usersIds = [];
        foreach ($users as $user)
        {
            $usersIds[] = $user->id;
        }

        if (!empty($usersIds))
            $posts = Post::getPostsByParams($slug1, $slug2, true)->whereIn('po.user_id', [$usersIds])->paginate(Post::POSTS_PER_PAGE);
        else
            $posts = [];

        $data = [
            //'user'=>$user,
            'posts'=>$posts,
            'defaults'=>$defaults,
            'numOfLeftSlugs'=>2,
        ];

        if ($request->ajax())
        {
            $html = view('home.partials.pagination-item', $data)->render();
            $lastPage = empty($html) ? true : ($posts->currentPage() == $posts->lastPage());

            $response = [
                'html' => $html,
                'last_page' => $lastPage,
            ];
            return response()->json($response);

        }

        return view('home.index', $data);
    }
}
