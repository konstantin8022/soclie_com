<?php

namespace App\Http\Controllers\Cabinet;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;

class ShowFavorites extends Controller
{
    public function __invoke(Request $request, $slug1 = null, $slug2 = null)
    {
        $user = Auth::user();

        $defaults = [
            'by' => 'instagram',
            'on' => 'all',
            'filter1' => $slug1,
            'filter2' => $slug2,
        ];
        $numOfLeftSlugs = 2;

        $posts = Post::getPostsByParams($slug1, $slug2, true)
            ->whereIn('po.id', (array)$user->favorites)
            ->paginate(Post::POSTS_PER_PAGE);


        $data = [
            'user'=> $user,
            'posts' => $posts,
            'defaults' => $defaults,
            'numOfLeftSlugs' => $numOfLeftSlugs,
        ];

        if ($request->ajax())
        {
            $html = view('home.partials.pagination-item', $data)->render();
            $lastPage = empty($html) ? true : ($posts->currentPage() == $posts->lastPage());

            $response = [
                'html' => $html,
                'last_page' => $lastPage,
            ];
            return response()->json($response);
        }

        return view('home.index', $data);
    }
}
