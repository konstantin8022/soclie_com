<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

class ShowProfile extends Controller
{
    public function __invoke(Request $request, $username)
    {
        dd('Удалить контроллер и роут!');
        $user = User::where('i_login', $username)->firstOrFail();

        $posts = Post::getPostsByParams($request->by, $request->on, true)->where(['po.user_id'=>$user->id])->paginate(Post::POSTS_PER_PAGE);

        $data = [
            'user' => $user,
            'posts' => $posts,
        ];

        if ($request->ajax())
        {
            $html = view('home.partials.pagination-item', $data)->render();
            $lastPage = empty($html) ? true : ($posts->currentPage() == $posts->lastPage());

            $response = [
                'html' => $html,
                'last_page' => $lastPage,
            ];
            return response()->json($response);
        }

        return view('home.showprofile', $data);
    }
}
