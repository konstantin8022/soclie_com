<?php

namespace App\Http\Controllers;


use App\Models\Post;
use App\Models\Tag;
use App\Models\User;

use App\Services\StringService;
use Illuminate\Http\Request;



use App\Services\CalculateRatingsService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
//use Andreyco\Instagram\Support\Laravel\Facade\Instagram;

  //use Dispatchable, InteractsWithQueue,Queueable,
  //use   SerializesModels;

   



class Index extends Controller
{


     protected $user;
   // protected $token;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
       // $this->token = $token;
    }






    public function __invoke(Request $request, $slug1 = null, $slug2 = null, $slug3 = null, $slug4 = null)
    {
        $user = User::where('i_login', $slug1)->first();

        $defaults = [
            'by' => 'instagram',
            'on' => 'all',
        ];

        if (!$user)
        {
            if ($slug1)
            {
                if ($slug1 == 'tag') //2 режим : /tag/value/ | /tag/value/filter1 | /tag/value/filter1/filter2
                {
                    //$posts = Tag::where('name', $slug2)->firstOrFail()->posts()->paginate(Post::POSTS_PER_PAGE); //todo сделать сортировку и для тегов
                    //dd($posts);
                    $posts = Post::getPostsByParams($slug2,$slug3,$slug4)->paginate(Post::POSTS_PER_PAGE);
                    $numOfLeftSlugs = 2;
                    $defaults['filter1'] = $slug3;
                    $defaults['filter2'] = $slug4;
                }
                else // 1 режим : /filter1 | /filter1/filter2
                {
                    $posts = Post::getPostsByParams($slug1, $slug2, true)->paginate(Post::POSTS_PER_PAGE);
                    $numOfLeftSlugs = 0;
                    $defaults['filter1'] = $slug1;
                    $defaults['filter2'] = $slug2;
                }
            }
            else // 1 режим : /
            {
    
    
       // Instagram::setAccessToken($this->token);
/*
        do
        {
            $media = Instagram::getUserMedia('self', 90);

            foreach ($media->data as $instagramPhoto)
            {
                if ($instagramPhoto->type != 'image' && $instagramPhoto->type != 'carousel')
                    //continue;

                var_dump($media->data);
               // $existingPhoto = Photo::where('i_id', $instagramPhoto->id)->first();

                //todo удаление фото при удалении из ИМ?

                //$description = isset($instagramPhoto->caption->text) ? $instagramPhoto->caption->text : '';

                

            }
        }
        while($media = Instagram::pagination($media, 90));
*/


                // Index
                $posts = Post::getPostsByParams($slug1, $slug2, true)->paginate(Post::POSTS_PER_PAGE);
                $numOfLeftSlugs = 0;
                $defaults['filter1'] = $slug1;
                $defaults['filter2'] = $slug2;
            }


            $data = [
                //'user' => $user,
                'posts' => $posts,
                'defaults' => $defaults,
                'numOfLeftSlugs' => $numOfLeftSlugs,
            ];

            if ($request->ajax())
            {
                $response = [
                    'html' => view('home.partials.pagination-item', $data)->render(),
                    'last_page' => $posts->currentPage() == $posts->lastPage(),
                ];
                return response()->json($response);

            }
            return view('home.index', $data);

        }
        else // 3 режим : /username/ | /username/filter1/ | /username/filter1/filter2
        {
            $posts = Post::getPostsByParams($slug2, $slug3, true)->where(['po.user_id'=>$user->id])->paginate(Post::POSTS_PER_PAGE);
            $numOfLeftSlugs = 1;

            $defaults['filter1'] = $slug2;
            $defaults['filter2'] = $slug3;

            $data = [
                'user' => $user,
                'posts' => $posts,
                'defaults' => $defaults,
                'numOfLeftSlugs' => $numOfLeftSlugs,
            ];

            if ($request->ajax())
            {
                $html = view('home.partials.pagination-item', $data)->render();
                $lastPage = empty($html) ? true : ($posts->currentPage() == $posts->lastPage());

                $response = [
                    'html' => $html,
                    'last_page' => $lastPage,
                ];
                return response()->json($response);
            }

            return view('home.showprofile', $data);
        }

    }
}