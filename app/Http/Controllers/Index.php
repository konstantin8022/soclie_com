<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use App\Models\Post;
use App\Models\Tag;
use App\Models\User;
use App\Services\StringService;
use Illuminate\Http\Request;

class Index extends Controller
{
    public function __invoke(Request $request, $slug1 = null, $slug2 = null, $slug3 = null, $slug4 = null)
    {
        $user = User::where('i_login', $slug1)->first();

        $defaults = [
           // 'by' => 'instagram',
	    'by' => 'rating',	
            'on' => 'all',
	//'on' => 'month',	
        ];

        if (!$user)
        {
            if ($slug1)
            {
                if ($slug1 == 'tag') //2 режим : /tag/value/ | /tag/value/filter1 | /tag/value/filter1/filter2
                {
                    $posts = Tag::where('name', $slug2)->firstOrFail()->posts()->where('is_empty','0') -> orderBy('rating', 'desc') ->paginate(Post::POSTS_PER_PAGE); //todo сделать сортировку и для тегов
            //        $posts = Tag::where('name', $slug2)->firstOrFail()->posts()->where('is_empty','0')-> where('i_created_time', '>', \Carbon\Carbon::now()->subMonth()->getTimestamp())  ->orderBy('rating', 'desc')->paginate(Post::POSTS_PER_PAGE); //todo сделать сортировку и для тегов
                #$rez=Tag::count_tags()->get();   
		# dd($rez);
                    //$posts = Tag::getPostsByParams($slug2,$slug3,$slug4)->paginate(Post::POSTS_PER_PAGE);
                    $numOfLeftSlugs = 2;
                    $defaults['filter1'] = $slug3;
                    $defaults['filter2'] = $slug4;
		//top tags	
		    $topTags=Post::toprezTags();	
                }
                else // 1 режим : /filter1 | /filter1/filter2
                {
                    $posts = Post::getPostsByParams($slug1, $slug2, true)->paginate(Post::POSTS_PER_PAGE);
                    $numOfLeftSlugs = 0;
                    $defaults['filter1'] = $slug1;
                    $defaults['filter2'] = $slug2;
		//top tags
		    $topTags=Post::toprezTags();	
                }
            }
            else // 1 режим : /
            {
 


 		$posts = Post::where('is_empty','0')->orderBy('rating', 'desc')->paginate(Post::POSTS_PER_PAGE); 	              
 //		$posts = Post::where('is_empty','0')-> where('i_created_time', '>', \Carbon\Carbon::now()->subMonth()->getTimestamp())->orderBy('rating', 'desc')->paginate(Post::POSTS_PER_PAGE); 	              
//		$posts = Post::getPostsByParams($slug1, $slug2, true)->paginate(Post::POSTS_PER_PAGE);
                $numOfLeftSlugs = 0;
                $defaults['filter1'] = $slug1;
                $defaults['filter2'] = $slug2;

		// top tags from post
		    $topTags=Post::toprezTags();	
                //$topTags=Post::with('tagsCount')->get();
		
	//	dump($topTags);
	}


            $data = [
                //'user' => $user,
                'posts' => $posts,
                'defaults' => $defaults,
                'numOfLeftSlugs' => $numOfLeftSlugs,
		'topTags' => $topTags	
            ];

            if ($request->ajax())
            {
                $response = [
                    'html' => view('home.partials.pagination-item', $data)->render(),
                    'last_page' => $posts->currentPage() == $posts->lastPage(),
                ];
                return response()->json($response);

            }
            return view('home.index', $data);

        }
        else // 3 режим : /username/ | /username/filter1/ | /username/filter1/filter2
        {
            $posts = Post::getPostsByParams($slug2, $slug3, true)->where(['po.user_id'=>$user->id])->paginate(Post::POSTS_PER_PAGE);
            $numOfLeftSlugs = 1;

            $defaults['filter1'] = $slug2;
            $defaults['filter2'] = $slug3;
	
	
//	 $myurl=$user->contentab; 
//	if(str_contains($myurl,'http')){
//		preg_match_all('/^(http?:\/\/)?(www\.)?\w+\.[a-z]{2,6}(\/)?$/i',$myurl, $result);		

	//return   '<a  href=" {{ url('http://ya.ru')}}">http://ya.ru</a>'

//	$res44=preg_replace('/((http:\/\/)[^ ]+)/', '<a href="\1">\1</a>', $myurl);
//	dump($result44);
//}


//	 $rez7=Tag::posts()->where('is_empty',0);
//	dump($rez7);
//	    $topTags=Post::toprezTags();	
            $data = [
                'user' => $user,
                'posts' => $posts,
                'defaults' => $defaults,
                'numOfLeftSlugs' => $numOfLeftSlugs,
//		'res44' => $res44
            ];

            if ($request->ajax())
            {
                $html = view('home.partials.pagination-item', $data)->render();
                $lastPage = empty($html) ? true : ($posts->currentPage() == $posts->lastPage());

                $response = [
                    'html' => $html,
                    'last_page' => $lastPage,
                ];
                return response()->json($response);
            }

           return view('home.showprofile', $data);
        }

    }
}

