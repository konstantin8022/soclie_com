<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use App\Models\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/cabinet';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null)
    {
        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email_or_login' => $request->email_or_password]
        );
    }

    protected function rules()
    {
        return [
            'token' => 'required',
            'email_or_login' => 'required|string|max:255',
            'password' => 'required|min:6',
        ];
    }

    protected function credentials(Request $request)
    {
        $email = $request->email_or_login;

        if (!filter_var($request->email_or_login, FILTER_VALIDATE_EMAIL)) {
            $user = User::where('i_login', $request->email_or_login)->first();

            if ($user) {
                $email = $user->email;
            }
        }

        return [
            'email' => $email,
            'password' => $request->password,
            'password_confirmation' => $request->password,
            'token' => $request->token
        ];
    }

    protected function sendResetFailedResponse(Request $request, $response)
    {
        return redirect()->back()
                    ->withInput($request->only('email_or_login'))
                    ->withErrors(['email_or_login' => trans($response)]);
    }
}
