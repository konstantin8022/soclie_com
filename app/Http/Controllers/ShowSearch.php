<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Http\Request;

/**
 * Class ShowSearch
 * @package App\Http\Controllers
 */
class ShowSearch extends Controller
{
    /**
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(SearchRequest $request)
    {
        $searchString = $request->phrase;

        $usersCollection = User::where('i_login', 'like', "%$searchString%")
            ->orWhere('i_real_name', 'like', "%$searchString%")->get();

        $tagsCollection = Tag::where('name', 'like', "%$searchString%")->get();

        $result = [];

        foreach ($usersCollection as $user)
        {
            $result[] = ['name' => $user->i_login, 'website-link' => $user->generateSearchLink()];
        }

        foreach ($tagsCollection as $tag)
        {
            $result[] = ['name' => '#' . $tag->name, 'website-link' => $tag->generateSearchLink()];
        }


      //dd( $result);
        return response()->json($result);
    }
}
