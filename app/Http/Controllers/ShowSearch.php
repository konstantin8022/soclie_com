<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Models\Tag;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;

/**
 * Class ShowSearch
 * @package App\Http\Controllers
 */
class ShowSearch extends Controller
{
    /**
     * @param SearchRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(SearchRequest $request)
    {
        $searchString = $request->phrase;

        $usersCollection = User::where('i_login', 'like', "%$searchString%")
            ->orWhere('i_real_name', 'like', "%$searchString%")->get();

        //$tagsCollection = Tag::where('name', 'like', "%$searchString%")->get();
	$tagsCollection = Tag::join('post_has_tag', 'post_has_tag.tag_id', '=', 'tags.id')
			->where('tags.name', 'like', "%$searchString%")
                	->join('posts', 'posts.id', '=', 'post_has_tag.post_id')
                	->where('posts.is_empty','0')
              // ->orderBy('tags.id')->distinct()
		->get();	 
        
	//dump( $tagsCollection);
	$result = [];

        foreach ($usersCollection as $user)
        {
            $result[] = ['name' => $user->i_login, 'website-link' => $user->generateSearchLink()];
        }
	
	$result1 = []; 	
        foreach ($tagsCollection as $tag)
        {
            //	$post1=$tag1->posts()->where('is_empty','0')->get();
		$result1[] = ['name' => '#' . $tag->name, 'website-link' => $tag->generateSearchLink()];
        }

	$rez=array_unique($result1, SORT_REGULAR);

	$rezz = array_merge($result,$rez);

/*
$summ4 = [];
        foreach( $rez77 as $myun ){

                 foreach( $myun as $val ){
                        $summ4[] = $val;
                }
        }
$result = array_unique($summ4);
//dump($ttt);
*/
         return response()->json($rezz);
    }
}
