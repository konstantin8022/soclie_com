<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use App\Services\StringService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShowPost extends Controller
{
    public function __invoke($id, $segment = null)
    {
        //dd($segment);

        if ($segment && ($comment = Comment::find($segment)))
        {
            $comment->update(['viewed'=>1]);
        }

//        $comment = Comment::findOrFail($request->comment_id);
//        $comment->viewed = 1;
//        $comment->save();

        $post = Post::where('i_link', Post::getLinkFromPart($id))->firstOrFail();

//        $comments = $post->comments()->where([
//            ['viewed', '=', 0],
//        ])->update(['viewed'=>1]);

        $comments = $post->comments()->where('parent_id', 0)->get();

        return view('home.showpost', ['post'=>$post, 'comments'=>$comments]);
    }
}
