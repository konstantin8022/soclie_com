<?php
/**
 * Created by PhpStorm.
 * User: newexe
 * Date: 11.12.17
 * Time: 17:53
 */
namespace App\Services;

use App\Models\Post;
use App\Models\User;

class StringService
{
    public static function getLinkPart($fullLink)
    {
        $string = parse_url($fullLink)['path'];

        $string = rtrim($string, '/');
        $string = substr($string, 3);

        return $string;
    }

    public static function getLinkFromPart($part)
    {
        return 'https://www.instagram.com/p/'. $part .'/';
    }

    /**
     * @param $numOfLeftSlugs
     * @param $filters
     * @param $defaults
     * @return string
     */
    public static function generateFiltersUrl($numOfLeftSlugs, $filters, $defaults)
    {
//
        $string = request()->getSchemeAndHttpHost() . '/'; // http://instagram.app/

        $allSlugs = request()->segments(); // ['slug1','slug2','slug3','slug4']

        $filtersSlugs = array_slice($allSlugs, $numOfLeftSlugs); // [] | ['slug3'] | ['slug3', 'slug4']

        $filtersSlugsCount = count($filtersSlugs); // 0 | 1 | 2
        if ($filtersSlugsCount > 2) abort(404);

        $slugs = array_diff($allSlugs, $filtersSlugs); // ['slug1', 'slug2']


        if (isset($filters['by']))
        {

            $filtersSlugs[0] = $filters['by'];

            //if (!isset($filtersSlugs[1]))
                //$filtersSlugs[1] = $defaults['on'];
        }
        elseif (isset($filters['on']))
        {
            if (!isset($filtersSlugs[0]))
                $filtersSlugs[0] = $defaults['by'];
            $filtersSlugs[1] = $filters['on'];
        }

        $resultSlugs = array_merge($slugs, $filtersSlugs);

        $string .= implode('/', $resultSlugs);
        if ($q = request()->getQueryString() != '')
            $string .= '?' . $q;

    return $string;
   //  http://127.0.0.1:8000/rating/all  , http://127.0.0.1:8000/rating/month  , //http://127.0.0.1:8000/by_comments/all  , http://127.0.0.1:8000/instagram/all
    }

}