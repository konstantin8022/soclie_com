<?php
/**
 * Created by PhpStorm.
 * User: newexe
 * Date: 11.12.17
 * Time: 17:53
 */
namespace App\Services;

use App\Models\Post;
use App\Models\User;

class CalculateRatingsService
{
    const INCREMENT = 1;
    const DECREMENT = 2;

    /**
     * @param User $user
     * @return void
     */
    public static function calculateUserRating(User $user)
    {
        /*
           +Количество лайков по всем постам
           - количество дизлайков всех постов
           + количество лайков к каждому его комментарию
           - количество дилайков к каждому его комментарию
           +количество добавлений в избранное по всем его постам.
           +количество фотоальбомов
           +количество комментариев в каждом фотоальбоме
           + количество подписчиков на нашем сайте
         */

        $rating = $user->getPostsLikesCount()
            - $user->getPostsDislikesCount()
            + $user->getCommentsLikesCount()
            - $user->getCommentsDislikesCount()
            + $user->getPostsFavoritesCount()
            + $user->getAllOnlyPosts()->count()
            + $user->getCommentsCount()
            + $user->followedUsers()->count();

        $user->rating = $rating;
        $user->save();
    }

    /**
     * @param Post $post
     * @return void
     */
    public static function calculatePostRating(Post $post)
    {
        $rating = $post->likes - $post->dislikes + $post->favorites_count + $post->getCommentsCount();
        $post->rating = $rating;
        $post->save();
    }

    public static function changeFavoritesCount(Post $post, $action = self::INCREMENT)
    {
        switch ($action)
        {
            case self::INCREMENT:
                $post->favorites_count++;
                break;
            case self::DECREMENT:
                $post->favorites_count--;
                break;
            default: return false;
        }
        $post->save();
    }
}