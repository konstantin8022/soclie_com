<?php

namespace App\Listeners;

use App\Events\onAddPostToFavouritesEvent;
use App\Services\CalculateRatingsService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddPostToFavouritesListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onAddPostToFavouritesEvent  $event
     * @return void
     */
    public function handle(onAddPostToFavouritesEvent $event)
    {
        if (!$event->user->isUserFavoritesFound($event->post))
        {
            $event->user->AddToFavorites($event->post);
            CalculateRatingsService::changeFavoritesCount($event->post, CalculateRatingsService::INCREMENT);
            CalculateRatingsService::calculatePostRating($event->post);
            CalculateRatingsService::calculateUserRating($event->post->user);
        }

    }
}
