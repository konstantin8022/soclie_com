<?php

namespace App\Listeners;

use App\Events\onPostLikeSet;
use App\Services\CalculateRatingsService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostLikeSetListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onPostLikeSet  $event
     * @return void
     */
    public function handle(onPostLikeSet $event)
    {
        if (!$event->post->isUserLikeFound())
        {
            $event->post->likeOrDislikeAdd($event->post::LIKE);
            CalculateRatingsService::calculatePostRating($event->post);
            CalculateRatingsService::calculateUserRating($event->post->user);
        }

    }
}
