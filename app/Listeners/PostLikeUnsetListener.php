<?php

namespace App\Listeners;

use App\Events\onPostLikeUnset;
use App\Services\CalculateRatingsService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostLikeUnsetListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onPostLikeUnset  $event
     * @return void
     */
    public function handle(onPostLikeUnset $event)
    {
        if ($event->post->isUserLikeFound())
        {
            $event->post->likeOrDislikeRemove($event->post::LIKE);
            CalculateRatingsService::calculatePostRating($event->post);
            CalculateRatingsService::calculateUserRating($event->post->user);
        }

    }
}
