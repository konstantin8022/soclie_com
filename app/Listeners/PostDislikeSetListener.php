<?php

namespace App\Listeners;

use App\Events\onPostDislikeSet;
use App\Services\CalculateRatingsService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostDislikeSetListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onPostDislikeSet  $event
     * @return void
     */
    public function handle(onPostDislikeSet $event)
    {
        if (!$event->post->isUserDislikeFound())
        {
            $event->post->likeOrDislikeAdd($event->post::DISLIKE);
            CalculateRatingsService::calculatePostRating($event->post);
            CalculateRatingsService::calculateUserRating($event->post->user);
        }
    }
}
