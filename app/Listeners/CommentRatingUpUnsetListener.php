<?php

namespace App\Listeners;

use App\Events\onCommentRatingUpUnset;
use App\Services\CalculateRatingsService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentRatingUpUnsetListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onCommentRatingUpUnset  $event
     * @return void
     */
    public function handle(onCommentRatingUpUnset $event)
    {
        if ($event->comment->isUserLikeFound())
        {
            $event->comment->likeOrDislikeRemove($event->comment::LIKE);
            CalculateRatingsService::calculatePostRating($event->comment->post);
            CalculateRatingsService::calculateUserRating($event->comment->user);
        }
    }
}
