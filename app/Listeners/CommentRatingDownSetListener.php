<?php

namespace App\Listeners;

use App\Events\onCommentRatingDownSet;
use App\Services\CalculateRatingsService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentRatingDownSetListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onCommentRatingDownSet  $event
     * @return void
     */
    public function handle(onCommentRatingDownSet $event)
    {
        if (!$event->comment->isUserDislikeFound())
        {
            $event->comment->likeOrDislikeAdd($event->comment::DISLIKE);
            CalculateRatingsService::calculatePostRating($event->comment->post);
            CalculateRatingsService::calculateUserRating($event->comment->user);
        }
    }
}
