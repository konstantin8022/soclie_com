<?php

namespace App\Listeners;

use App\Events\onPostDislikeUnset;
use App\Services\CalculateRatingsService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostDislikeUnsetListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onPostDislikeUnset  $event
     * @return void
     */
    public function handle(onPostDislikeUnset $event)
    {
        if ($event->post->isUserDislikeFound())
        {
            $event->post->likeOrDislikeRemove($event->post::DISLIKE);
            CalculateRatingsService::calculatePostRating($event->post);
            CalculateRatingsService::calculateUserRating($event->post->user);
        }
    }
}
