<?php

namespace App\Listeners;

use App\Events\onCommentRatingDownUnset;
use App\Services\CalculateRatingsService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentRatingDownUnsetListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onCommentRatingDownUnset  $event
     * @return void
     */
    public function handle(onCommentRatingDownUnset $event)
    {
        if ($event->comment->isUserDislikeFound())
        {
            $event->comment->likeOrDislikeRemove($event->comment::DISLIKE);
            CalculateRatingsService::calculatePostRating($event->comment->post);
            CalculateRatingsService::calculateUserRating($event->comment->user);
        }
    }
}
