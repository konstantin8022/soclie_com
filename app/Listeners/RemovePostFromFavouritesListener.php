<?php

namespace App\Listeners;

use App\Events\onRemovePostFromFavouritesEvent;
use App\Services\CalculateRatingsService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class RemovePostFromFavouritesListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onRemovePostFromFavouritesEvent  $event
     * @return void
     */
    public function handle(onRemovePostFromFavouritesEvent $event)
    {
        if ($event->user->isUserFavoritesFound($event->post))
        {
            $event->user->RemoveFromFavorites($event->post);
            CalculateRatingsService::changeFavoritesCount($event->post, CalculateRatingsService::DECREMENT);
            CalculateRatingsService::calculatePostRating($event->post);
            CalculateRatingsService::calculateUserRating($event->post->user);
        }
    }
}
