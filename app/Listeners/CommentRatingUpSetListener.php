<?php

namespace App\Listeners;

use App\Events\onCommentRatingUpSet;
use App\Models\Post;
use App\Services\CalculateRatingsService;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CommentRatingUpSetListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  onCommentRatingUpSet  $event
     * @return void
     */
    public function handle(onCommentRatingUpSet $event)
    {
        if (!$event->comment->isUserLikeFound())
        {
            $event->comment->likeOrDislikeAdd($event->comment::LIKE);
            CalculateRatingsService::calculatePostRating($event->comment->post);
            CalculateRatingsService::calculateUserRating($event->comment->user);
        }
    }
}
