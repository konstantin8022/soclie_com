<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            'SocialiteProviders\Instagram\InstagramExtendSocialite@handle',
        ],

        'App\Events\onAddPostToFavouritesEvent' => [
            'App\Listeners\AddPostToFavouritesListener',
        ],

        'App\Events\onRemovePostFromFavouritesEvent' => [
            'App\Listeners\RemovePostFromFavouritesListener',
        ],

        'App\Events\onPostLikeSet' => [
            'App\Listeners\PostLikeSetListener',
        ],
        'App\Events\onPostLikeUnset' => [
            'App\Listeners\PostLikeUnsetListener',
        ],
        'App\Events\onPostDislikeSet' => [
            'App\Listeners\PostDislikeSetListener',
        ],
        'App\Events\onPostDislikeUnset' => [
            'App\Listeners\PostDislikeUnsetListener',
        ],

        'App\Events\onCommentRatingUpSet' => [
            'App\Listeners\CommentRatingUpSetListener',
        ],
        'App\Events\onCommentRatingDownSet' => [
            'App\Listeners\CommentRatingDownSetListener',
        ],
        'App\Events\onCommentRatingUpUnset' => [
            'App\Listeners\CommentRatingUpUnsetListener',
        ],
        'App\Events\onCommentRatingDownUnset' => [
            'App\Listeners\CommentRatingDownUnsetListener',
        ],

    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
