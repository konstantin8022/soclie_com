<?php

namespace App\Providers\Social;

use App\Models\User;

use Andreyco\Instagram\Support\Laravel\Facade\Instagram;

use Laravel\Socialite\Facades\Socialite;

use App\Jobs\DownloadInstagramUserMedia;

class InstagramServiceProvider extends AbstractServiceProvider
{

    public function __construct()
    {
        $this->provider = Socialite::driver(
            str_replace(
                'serviceprovider', '', strtolower((new \ReflectionClass($this))->getShortName())
            )
        )->setScopes(config('instagram.scope'));
    }

    /**
     *  Handle Instagram response
     * 
     *  @return Illuminate\Http\Response
     */
    public function handle()
    {
        $instagramData = $this->provider->user();
        
        $token = (isset($instagramData->token) && !empty($instagramData->token)) ? $instagramData->token : $instagramData->accessTokenResponseBody->access_token;
        session(['access_token'=>$token]);

        //todo Проверка на пригодность данных к обновлению в базе!!!

        $i_login = $instagramData->user['username'];
        $i_real_name = $instagramData->user['full_name'];
        $i_id = $instagramData->user['id'];
        $i_count_subscribers = $instagramData->user['counts']['followed_by'];
        $i_count_subscribes = $instagramData->user['counts']['follows'];
        $i_publications = $instagramData->user['counts']['media'];

        $existingUser = User::where('i_id', $instagramData->user['id'])->first();

        if ($existingUser) {

            //dd($existingUser);
            
            //todo: если что-то обновилось, обновить базу:
            //if (!isset($settings['instagram_id'])) {
            $existingUser->i_login = $i_login;
            $existingUser->i_real_name = $i_real_name;
            $existingUser->i_id = $i_id;
            $existingUser->i_count_subscribers = $i_count_subscribers;
            $existingUser->i_count_subscribes = $i_count_subscribes;
            $existingUser->i_publications = $i_publications;
            $existingUser->save();
            //}

            $existingUser->setProfilePhoto($instagramData->avatar);

            DownloadInstagramUserMedia::dispatch($existingUser, $token);

            return $this->login($existingUser)->with('message', __('cabinet.message_sync'));
        }

        $newUser = $this->register([
            'i_login' => $i_login,
            'i_real_name' => $i_real_name,
            'i_id' => $i_id,
            'i_count_subscribers' => $i_count_subscribers,
            'i_count_subscribes' => $i_count_subscribes,
            'i_publications' => $i_publications,
            'rating' => 0,
        ]);

        $newUser->setProfilePhoto($instagramData->avatar);

        DownloadInstagramUserMedia::dispatch($newUser, $token);

        return $this->login($newUser)->with('message', __('cabinet.message_welcome'));
    }       
}