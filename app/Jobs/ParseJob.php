<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;

use InstagramAPI\Instagram;
use Carbon\Carbon;

use App\Models\User;
use App\Models\Photo;

use App\Services\CalculateRatingsService;

class ParseJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    protected $max_id;

    protected $ig_login;

    protected $ig_password;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
        $this->ig_login = env('IG_LOGIN');
        $this->ig_password = env('IG_PASSWORD');

        $this->max_id = Redis::get('parsejob:user:profile:' . $this->user->id);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $ig = new Instagram(false, false);

        try {
            $ig->login($this->ig_login, $this->ig_password);

        } catch (\Exception $e) {
            Log::error($e->getMessage());

            $this->user->sync_error = $e->getMessage();
            $this->user->save();
        }

        $this->user->sync_error = null;
        $this->user->save();

        try {

	//    dump($ig->people);
            $user_id = $ig->people->getUserIdForName($this->user->i_login);
            $is_private = $ig->people->getInfoById($user_id)->getUser()->getIsPrivate();
            $is_friendship = $ig->people->getFriendship($user_id)->getFollowing();

//	    $array_followers=$ig->people->getFollowers($user_id);
		//foreach($array_followers as $direct){
		
		//	$ig->direct->sendText($user_id, "hello world");
	//	}	
			//$recipient=array(['users' "ivalive",]);
//			$ig->direct->sendText(array('users' => array($is_friendship) ), "hello world");
//	dump($array_followers);	
            if ($is_private && !$is_friendship) {
                $result = $ig->people->follow($user_id);
                $this->user->i_private = true;
                $this->user->save();
                return;
            }

            $this->user->i_private = false;

            $followers_count = $ig->people->getInfoById($user_id)->getUser()->getFollowerCount();
            $following_count = $ig->people->getInfoById($user_id)->getUser()->getFollowingCount();
            $media_count = $ig->people->getInfoById($user_id)->getUser()->getMediaCount();
            $profile_pic = $ig->people->getInfoById($user_id)->getUser()->getProfilePicUrl();
        
            $this->user->setProfilePhoto($profile_pic);
            $this->user->i_count_subscribes = $following_count;
            $this->user->i_count_subscribers = $followers_count;
            $this->user->i_publications = $media_count;
            $this->user->save();

            do {
                $response = $ig->timeline->getUserFeed($user_id, $this->max_id);

                foreach ($response->getItems() as $item) {

                    $type = '';

                    if ($item->getMediaType() == \InstagramAPI\Response\Model\Item::PHOTO) {
                        $type = 'image';

                    } elseif ($item->getMediaType() == \InstagramAPI\Response\Model\Item::ALBUM) { // Альбом
                        $type = 'carousel';

                    } else {
                        continue;
                    }

                    $photo = Photo::where('i_id', $item->getPk())->first();

                    if ($type == 'image') { // Фото
                        $photo_url = $item->getImageVersions2()->getCandidates()[0]->getUrl();
                        $photo_url = substr($photo_url, 0, strpos($photo_url, '?'));

                    } elseif ($type == 'carousel') { // Альбом
                        $photo_urls = [];
                        $subitems = $item->getCarouselMedia();
                        
                        foreach ($subitems as $subitem) {
                            $photo_url = $subitem->getImageVersions2()->getCandidates()[0]->getUrl();
                            $photo_url = substr($photo_url, 0, strpos($photo_url, '?'));
                            $photo_urls[] = $photo_url;
                        }
                    }
                    

                    if ($photo) {

                        if (!$photo->hasImage()) {
                            if ($type == 'image') {
                                $photo->savePhotoToFilesystem($photo_url);
                            }

                            if ($type == 'carousel') {
                                $photo->savePhotoToFilesystem($photo_urls, true);
                            }
                        }

                        if ($item->getCaption()) {
                            $photo->i_description = $item->getCaption()->getText();
                        }

                        $photo->save();

                    } else {

                        $description = '';
                        $created_at = Carbon::createFromTimestamp($item->getTakenAt())->toDateTimeString();

                        if ($item->getCaption()) {

                            $description = $item->getCaption()->getText();
                            $created_at = $item->getCaption()->getCreatedAt();

                            if (!empty($created_at)) {
                                $created_at = Carbon::createFromTimestamp($created_at)->toDateTimeString();

                            } else {
                                $created_at = Carbon::createFromTimestamp($item->getTakenAt())->toDateTimeString();
                            }

                        }

                        $duplicate = $this->user->posts()
                            ->where('i_link', '=', 'https://www.instagram.com/p/' . $item->getCode() . '/')
                            ->count();

                        if (!$duplicate) {
                            $post = $this->user->posts()->create([
                                'i_likes_count' => $item->getLikeCount(),
                                'i_comments_count' => $item->getCommentCount(),
                                'i_created_time' => Carbon::parse($created_at)->getTimestamp(),
                                'i_link' => 'https://www.instagram.com/p/' . $item->getCode() . '/'
                            ]);

                            $post->description()->create();

                            $photo = $post->photos()->create([
                                'i_id' => $item->getPk(),
                                'i_type' => $type,
                                'is_from_instagram' => true,
                                'i_description' => $description,
                                'i_created_time' => Carbon::parse($created_at)->getTimestamp(),
                                'user_id' => $this->user->id
                            ]);

                            if ($type == 'image') {
                                $photo->savePhotoToFilesystem($photo_url);
                            }

                            if ($type == 'carousel') {
                                $photo->savePhotoToFilesystem($photo_urls, true);
                            }
                        }
                        
                    }

                }

                $this->max_id = $response->getNextMaxId();
                Redis::set('parsejob:user:profile:' . $this->user->id, $this->max_id);


            } while ($this->max_id !== null);

            $this->user->synced_at = Carbon::now()->toDateTimeString();
            $this->user->save();

            Redis::command('del', ['parsejob:user:profile:' . $this->user->id]);

            CalculateRatingsService::calculateUserRating($this->user);

        } catch (\Exception $e) {
            Log::error($e->getMessage());

            $this->user->sync_error = $e->getMessage();
            $this->user->save();
        }
    }
}
