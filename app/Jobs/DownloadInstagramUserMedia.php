<?php

namespace App\Jobs;

use App\Models\Photo;

use App\Services\CalculateRatingsService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Andreyco\Instagram\Support\Laravel\Facade\Instagram;

use App\Models\User;

class DownloadInstagramUserMedia implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;
    protected $token;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Instagram::setAccessToken($this->token);

        do
        {
            $media = Instagram::getUserMedia('self', 90);

            foreach ($media->data as $instagramPhoto)
            {
                if ($instagramPhoto->type != 'image' && $instagramPhoto->type != 'carousel')
                    continue;

                //var_dump($media->data);
                $existingPhoto = Photo::where('i_id', $instagramPhoto->id)->first();

                //todo удаление фото при удалении из ИМ?

                $description = isset($instagramPhoto->caption->text) ? $instagramPhoto->caption->text : '';

                if ($existingPhoto)
                {
                    //                dump('if ($existingPhoto)');
                    if (!$existingPhoto->hasImage())
                    {
                        $existingPhoto->savePhotoOrCollectionToFilesystem($instagramPhoto);
                    }
                    $existingPhoto->i_description = $description;
                    $existingPhoto->save();
                }
                else
                {
                    //dump('NOT if ($existingPhoto)');
                    $post = $this->user->posts()->create([
                        'i_likes_count' => $instagramPhoto->likes->count,
                        'i_comments_count' => $instagramPhoto->comments->count,
                        'i_created_time' => $instagramPhoto->created_time,
                        'i_link' => $instagramPhoto->link,
                    ]);
                    $post->description()->create();

                    $photo = $post->photos()->create([
                        'i_id' => $instagramPhoto->id,
                        'i_type' => $instagramPhoto->type,
                        'is_from_instagram' => 1,
                        'i_description'=> $description,
                        'i_created_time' => $instagramPhoto->created_time,
                        'user_id' => $this->user->id,
                    ]);
                    $photo->savePhotoOrCollectionToFilesystem($instagramPhoto);
                }

            }
        }
        while($media = Instagram::pagination($media, 90));

        CalculateRatingsService::calculateUserRating($this->user);

//        return redirect()->refresh();
    }
}
