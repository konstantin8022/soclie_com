<?php

return [
    'by' => ['only_posts', 'instagram', 'im_likes', 'rating', 'favorites', 'comments'],
    'on' => ['week', 'month', 'all'],
];