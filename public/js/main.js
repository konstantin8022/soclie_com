$(function(){
    //GLOBAL VARIABLES
    var token = $("meta[name='csrf-token']").attr("content");

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // FULL POST SLIDERS
    $( '#my-slider' ).sliderPro({
        
        width: '100%',
        height: 500,
        arrows: false,
        thumbnailArrows: true,
        fade: true,
        fadeThumbnailArrows: false,
        buttons: false,
        keyboard: false,
        autoplay: false,
        slideAnimationDuration: 50,
        breakpoints: {
            1024: {
                height: 300
            }
        }
    });

    $( '#my-slider2' ).sliderPro({
        width: '100%',
        height: 500,
        arrows: false,
        thumbnailArrows: true,
        fadeThumbnailArrows: false,  // Указывает, будут ли уменьшены стрелки миниатюр
        buttons: false,
        keyboard: false,
        // waitForLayers: true,
        // imageScaleMode: 'contain',
        // autoHeight: true,
        autoplay: false,
        // fullScreen: true,
        fadeFullScreen: false,  //Указывает, будет ли кнопка появляться только при наведении курсора.
        slideAnimationDuration: 50,
        // autoScaleLayers: true,
        breakpoints: {  // изменять внешний вид и поведение слайдера при изменении размера страницы.
            1024: {
                height: 300
            }
        }
    });

    $('.last-synced-items').find('.posts').owlCarousel({
        loop: true,  //Бесконечное зацикливание слайдера
        nav: false, //  Отображение кнопок вперед\назад
        merge: true, //Слияние предметов. Ищем данные-слияние = '{число}' внутри элемента.
        margin: 10,
        smartSpeed: 500,
        autoplay: true, //Автоматическое пролистывание
        responsive:{  //Адаптивность. Кол-во выводимых элементов при определенной ширине.
            0:{
                items:3
            },
            400:{
                items:5
            },
            735:{
                items:7
            },
            1020:{
                items:10
            }
        }
    });

    // var posts_carousel = $('.post-carousel');
    //
    // posts_carousel.owlCarousel({
    //     loop: true,
    //     items:	1,
    //     mouseDrag: false,
    //     touchDrag: true,
    //     nav: false,
    //     dots: false,
    //     smartSpeed: 0.01,
    // });

    // $(posts_carousel).trigger('add.owl.carousel', ['<div class="item"><img src="http://placehold.it/140x100" alt=""></div>'])
    //     .trigger('refresh.owl.carousel');

    setTimeout(function(){
        $('.post_slider').addClass('loaded');
        $('.full_post .slider-pro img.sp-image').css({"width": "auto", "height": "auto"});
    }, 50);

    var last_items = [];

    setInterval(function(){
            //alert("get-last-items-synced-items");
        $.get('/cabinet/get-last-items', { last_ids: last_items })
            .done(function(data) {
                  //  alert(data);
                 console.log(data.photos);

                if($('.last-synced-items').hasClass('in-progress')) {
                    $('.last-synced-items').removeClass('in-progress');
                }

                $(data.photos).each(function(i,e){
                    $('.last-synced-items')
                        .find('.posts')
                        .trigger('add.owl.carousel', '<div class="item"><a href="' + e.url + '"><img src="' + e.src + '" alt="inst_img"><span class="date">' + e.updated + '</span></a></div>', 0)
                        .trigger('refresh.owl.carousel');

                    last_items.push(e.id);
                });

                // var all_items = $('.last-synced-items').find('.posts .item').length;
                // console.log(all_items);
                //
                // $('.last-synced-items').find('.posts').trigger('to.owl.carousel', [all_items, 20]);
            });

    }, 5000);
    setInterval(function(){
        $.get('/cabinet/user/is-private')
            .done(function(data) {
         //   alert(JSON.stringify(data));
                if (data.is_private == false) {
                    $('.private-profile-info').hide();

                    $('.sync_status')
                        .empty()
                        .addClass('is-progress')
                        .html('user/is-privateSynchronizing posts from instagram in progress, please wait...');


                } else {
                    $('.private-profile-info').show();

                    $('.sync_status')
                        .empty()
                        .addClass('is-private')
                        .html('Account is private, please open your account or add our account to friends');
                }
            });
    }, 15000);
    
    
    $('.check_again_btn').on('click', function(e){
        // e.preventDefault();

        $('.private-profile-info').hide();

        $.get('/cabinet/user/is-private')
            .done(function(data) {
                if (data.is_private == false) {
                    $('.private-profile-info').hide();

                } else {
                    $('.private-profile-info').show();
                }
            });
    });
    

    $('.next-btn').on('click', function(){
        alert("next-btn_slidepro");
        $(this).closest('.slider-pro').sliderPro( 'nextSlide' );
    });

    $('.prew-btn').on('click', function(){
        $(this).closest('.slider-pro').sliderPro( 'previousSlide' );
    });

    
     
    // MODAL SLIDER INIT
    $('.modal_slider').owlCarousel({
        loop: true,
        items:	1,
        mouseDrag: false,
        touchDrag: false,
        nav: true,
        smartSpeed: 0.01,
        navText: [
            '<p class="slider-nav-btn prew-btn"><i class="fa fa-angle-left" aria-hidden="true">kostya1</i></p>',
            '<p class="slider-nav-btn next-btn"><i class="fa fa-angle-right" aria-hidden="true">kostya2</i></p>'
        ]
    });

    $(document).on('click', '.slide_img', function () {
        //home/showpost/  -- прокрутка сладера 
            alert("slide_img_carusel");
        $('.modal_slider').trigger('next.owl.carousel');
    });
    
    // CLOSE & OPEN MODAL BTN
    $(document).on('click', '.open_modal', function() {
        
        var id = $(this).closest('.modal_block').attr('id');
        //var index = $('#'+ id + ' .open_modal').index( this );
        
        alert(id);
        var customIndex = $(this).attr('data-index');
        alert("data-index-open_modal " +customIndex);
        // console.log("customIndex = " + customIndex);
        // console.log(id);

        // console.log($(window).width());
        if ($(document).width() < 768) {
            
            alert($(window).width());
        } else {
            if ($(this).attr('thumb') == 'true') {
                $('#'+ id + '.modal_block').find('.owl-carousel').eq(1).trigger('to.owl.carousel', [customIndex, 0]);  // [position, speed]
            } else {
                $('#'+ id + '.modal_block').find('.owl-carousel').eq(1).trigger('to.owl.carousel', [customIndex - 1, 0]); // [position, speed]
            }

            $('#'+ id + '.modal_block').find('.modal').removeClass('hidden');
        }

    });
 
//duble two    
    $(document).on('click', '.photo_wrap', function() {
        alert("photo_wrap_modal33");
        var id = $(this).closest('.modal_block').attr('id');
        //var index = $('#'+ id + ' .open_modal').index( this );
        var customIndex = $(this).attr('data-index');

        // console.log("customIndex = " + customIndex);
        // console.log(id);

        // console.log($(window).width());

        if ($(document).width() < 768) {
            // console.log(this);
            $(this).find('.instagram_slider').trigger('next.owl.carousel');

        } else {
            if ($(this).attr('thumb') == 'true') {
                $('#'+ id + '.modal_block').find('.owl-carousel').eq(1).trigger('to.owl.carousel', [customIndex, 0]);  //[position, speed]
            } else {
                $('#'+ id + '.modal_block').find('.owl-carousel').eq(1).trigger('to.owl.carousel', [customIndex - 1, 0]);  //[position, speed]
            }

            $('#'+ id + '.modal_block').find('.modal').removeClass('hidden');
        }

    });

    
    
    
    
    // FULL POST COMMON MODAL
    $(document).on('click', '.open_modal_2', function() {
        alert("to.owl.carousel11");
        var customIndex = $(this).attr('data-index');
        $('.modal').removeClass('hidden');
        $('.full_common_modal .owl-carousel').trigger('to.owl.carousel', [customIndex - 1, 0]);  //[position, speed]
    });

    $(document).on('click', '.modal_back', function(){
        $('.modal').addClass('hidden');
    });

    $(document).keyup(function(e) {
       // alert("esc");
        if (e.keyCode === 27) $('.modal').addClass('hidden');  // esc
    });

    var instagramSliderParams = {
        loop: true,
        items:	1,
        nav: false,
        // animateOut: 'fadeOut',
        smartSpeed: 300,
        lazyLoad: true,  // Lazy-Load картинок. data-src и data-src-retina указываются для высоких ////разрешений. Так же загружает изображение инлайново в свойство background 
        navText: [
            '<p class="slider-nav-btn prew-btn"><i class="fa fa-angle-left" aria-hidden="true">rrrr</i></p>',
            '<p class="slider-nav-btn next-btn"><i class="fa fa-angle-right" aria-hidden="true">jjjjj</i></p>'
        ],
        responsive: {
            769 : {
                nav : true,
                loop: true
            },
        }
    };

    // MAIN PAGE SLIDER
    var instaSlider = $('.instagram_slider');
    instaSlider.owlCarousel(instagramSliderParams);

    instaSlider.on('loaded.owl.lazy', function(e) {
        $(this).parent('.photo_wrap').find('.loader').addClass('hidden');
         console.log("loaded.owl.lazy" + this);
    });
    
    // CUSTOM SELECT
    $('.select_title').on('click', function(){  // Language
          alert("select_title2");
        $('.select_wrap').removeClass('active');
        $(this).closest('.select_wrap').addClass('active');
    });

    $('.select_wrap li').on('click', function(){
                  alert("select_title" + $(this).text());
        $(this).closest('.select_wrap').find('.select_title').text($(this).text());
        $(this).closest('.select_wrap').removeClass('active');
    });

    $(document).on('click', function(e) {
        if (!$(e.target).closest('.select_wrap').length) {
            $('.select_wrap').removeClass('active');
        }
        e.stopPropagation();
    });
  
    // MOBILE MENU
        //alert("mobile menu2")   Entry Point 
    $('.menu_mob_btn').on('click', function(){
        alert("mobile menu");
        $('.menu').toggleClass('active');
    });

    $(document).on('click', function(e) {
        
        if (!$(e.target).closest('.menu').length) {
            $('.menu').removeClass('active');
        }
        e.stopPropagation();
    });
 
    
    
    
    
    
    
    
    // AUTOCOMPLITE SEARCH
    var options = {
        url: function(phrase) {
            return "/showsearch";
        },

        getValue: function(element) {
            return element.name;
        },

        ajaxSettings: {
            dataType: "json",
            method: "GET",
            data: {	dataType: "json" }
        },

        preparePostData: function(data) {
            data.phrase = $("#search").val();
            return data;
        },

        template: {
            type: "links",
            fields: {
                link: "website-link"
            }
        },
        // requestDelay: 400,
        list: {
            onShowListEvent: function() {
                $('.search').addClass('active');
            },

            onHideListEvent: function() {
                $('.search').removeClass('active');
            }
        }
    };

    $("#search").easyAutocomplete(options);

    
    
    
    
    
    
    // LIKE/DISLIKE COMMENTS COUNT CHANGE
    $(document).on('click', '.like_btn, .dislike_btn', function(){
        alert("COMMENTS COUNT");
        var btn = $(this);
        var comment = btn.closest('.post_comment');
        var comment_id = comment.find('input[name=comment_id]').val();
        var url;

        if(btn.hasClass('like_btn')){
            url = '/cabinet/comment/commentlikeset';

        }else{
            url = '/cabinet/comment/commentdislikeset';
        }

        $.ajax({
            type: "POST",
            url: url,
            data: { comment_id: comment_id, _token: token }

        }).done(function(data) {
            alert( JSON.stringify(data));
            comment.find('.post_comment_rate:first').html(data.rating);
            comment.find('.post_comment_rate:first').removeClass('negative').removeClass('positive');

            if(data.rating > 0){
                comment.find('.post_comment_rate:first').addClass('positive');

            } else if (data.rating != 0){
                comment.find('.post_comment_rate:first').addClass('negative');
            }

            btn.toggleClass('active');

            if(btn.hasClass('like_btn')){
                comment.find('.dislike_btn:first').removeClass('active');

            } else {
                comment.find('.like_btn:first').removeClass('active');
            }

            Snakbar(data.message);

        }).fail(function(data){
            Snakbar('error');
        });
    });

    
    
    // LIKE/DISLIKE POST COUNT CHANGE
    $(document).on('click', '.icon_js', function(){
        var icons = $(this).closest('.icons');
        alert( JSON.stringify(icons));
        var id = icons.find('.post_id').val();
        alert(id);
        var url;

        var msg = $('body').find('.sm_need_auth').val();
       // alert($(this).data('value'));
        alert($('body').find('.need_auth_js').length);
        if ($('body').find('.need_auth_js').length == 0){
            $(this).toggleClass('active');

            switch($(this).data('value')) {
                case 'like':
                    url = "/cabinet/post/postlikeset";
                    icons.find('.dislike > i').removeClass('active');
                    break;

                case 'dislike':
                    url = "/cabinet/post/postdislikeset";
                    icons.find('.like > i').removeClass('active');
                    break;

                case 'favor':
                    url = "/cabinet/post/addposttofavorites";
                    break;
                default:
                // code block
            }

            $.ajax({
                type: 'POST',
                url: url,
                data: { id: id, _token: token }

            }).done(function(data) {
                alert(JSON.stringify(data));
                icons.find('.like > span').html(data.likes);
                icons.find('.dislike > span').html(data.dislikes);
                icons.find('.favor > span').html(data.favorites);
                icons.find('.rating > span').html(data.rating);

                Snakbar(data.message);

            }).fail(function(data){
                Snakbar('error888');
            });
        } else {
            Snakbar(msg);
        }
    }); 
    
    // SMILES DOCK
    $('.smiles_dock_btn').on('click', function() {
        alert("smiles_dock_btn");
        
        $(this).closest('.comment_action_block')
            .find('.comment_action_icons')
            .toggleClass('hidden');
    });
    
    
    
    
    // COMMENTS REPLY BLOCK SHOW
    $(document).on('click','.reply_btn', function(){
        alert("replaybtn");
        $('.post_comment .reply_block').addClass('hidden');
        $('.post_comment .reply_btn').removeClass('hidden');

        $(this).closest('.post_comment')
            .find('.reply_block:first')
            .removeClass('hidden');

        $(this).closest('.post_comment')
            .find('.reply_btn:first')
            .addClass('hidden');
    });
 
    $(document).on('click','.cansel_btn', function() {
        
        var item = $(this);

        var comment_id = $(this).closest('.comment_block')
            .find('form input[name=comment_id]')
            .val();
        alert(comment_id + "canselbtn");
        if(comment_id != '') {
            $.ajax({
                type: 'DELETE',
                url: '/comments/' + comment_id

            }).done(function(data) {

                alert(data);
                Loader(item.closest('.comment_block'));  // Loader from end this file 

            }).fail(function(data) {
                Snakbar('error');
            });
        }

        item.closest('.post_comment').find('.reply_block:first').addClass('hidden');
        item.closest('.post_comment').find('.reply_btn:first').removeClass('hidden');
        item.closest('.comment_block').find('.comment_text_field').html('');
    });

    
    // UPLOAD PHOTO INPUT ACTION
    $(document).on('change','.upload_photo_input_js', function() {
        var nnnn;
            // eto nugno 
      /*  if ($(this).val().lastIndexOf('\\')) {
            nnnn = $(this).val().lastIndexOf('\\') + 1;

        }else{
            nnnn = $(this).val().lastIndexOf('/') + 1;
        }
*/
        alert(nnnn);
        r34566=$(this).closest('.comment_modal').find("label").html($(this).val().slice(nnnn));

        alert(JSON.stringify(r34566));
        if($(this).val() == '') {
            $(this).closest('.comment_modal').find("label").html('chose file');
        }
    });

    // UPLOAD PHOTO MODAL OPEN/CLOSE
    $(document).on('click','.upload_photo_btn', function() {
        
       // alert("upload_photo_btn999999");
        
        $(this).closest('.comment_action_block')
            .find('.comment_modal')
            .toggleClass('hidden');
    });

    $(document).on('click', function(e) {
      //   alert("upload_photo_btn7777777");
        if (!$(e.target).closest('.comment_action_block').find('.comment_modal').length) {  //DOM-элемент, являющийся источником события.
            $('.comment_modal').addClass('hidden');
        }
        e.stopPropagation();
    });
    
    // UPLOAD YOUTUBE VIDEO
    $(document).on('click','.upload_video_js', function() {
        var input_youtube = $(this).closest('.comment_modal ').find('input[name=youtube_link]').val();
        //alert(input_youtube);
        var n = input_youtube.lastIndexOf('/') + 1;
        var link = input_youtube.slice(n);
        //alert(link);
        link = link.split('&')[0];

        n = link.lastIndexOf('=') + 1;
        link = link.slice(n);

        var iframe = '<br /><iframe width="560" height="315" src="https://www.youtube.com/embed/'+ link +'" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>';

        $(this).closest('.comment_modal ').find('input[name=youtube_link]').val('');
      
        if(link != '') {
            $(this).closest('.comment_block').find('.text_container').append(iframe + '<br /><br />');
            $(this).closest('.comment_modal ').addClass('hidden');

            if($('body').hasClass('my_catalog')) {
                
                SendPostDescription($(this));
            }
        }
    });

    
  //////////////////////////////////////////////  
    
    // UPLOAD IMAGE TO COMMENT
    $(document).on('submit','.ajax_form', function(e) {
        
        
        e.preventDefault();

        var this_item = $(this);
        var url = $(this).attr('action');
        alert(url);
        var text_field = this_item.closest('.comment_block').find('.text_container');
        var comment_id = this_item.find('input[name=comment_id]').val();
        alert( comment_id+"ajax_form_button");
        var modal =  this_item.closest('.comment_block').find('.comment_modal');
        var form = $(this);
         alert(JSON.stringify( this_item));
        if(comment_id == ''){
            alert("SendPostComment send");
            SendPostComment(this_item);

        }else{
                alert("not SendPostComment send");
            Loader(this_item.closest('.comment_block'), 'add');  // Loader from end this file 
/////////////???????????????????????????????????????
            $.ajax({
                url: url,
                data: new FormData(this),
                cache: false,
                processData: false,
                contentType: false,
                type: 'POST'

            }).done(function(data) {
                
                
                Loader(this_item.closest('.comment_block'));  // Loader from end this file 

                if (data.error == undefined) {
                    data.srcs.forEach(function(item) {
                        //text_field.append('<img src="'+ item +'" alt="image"<br>');
                        text_field.append('<a data-fancybox="images" href="' + item.full + '"><img src="' + item.thumb + '"></a>');
                    });

                    modal.addClass('hidden');

                    if ($('body').hasClass('full_post')) {
                        SendPostComment(this_item);

                    } else {
                        SendPostDescription(this_item);
                    }

                } else {
                    Snakbar(data.error);
                }

                form.find('.label_upload').html(data.label);

            }).fail(function(data) {
                Snakbar('error_enddd');
                form.find('.label_upload').html(data.label);
            });
        }
    }); 
    
    // SEND POST DESCRIPTION
    $(document).on('click', '.send_comment_btn', function() {
        // if($(this).closest('.comment_block').find('.text_container').html() != ''){
            alert("send_comment_btn_last");
            SendPostDescription($(this));
        // }
    });

    // ADD COMMENT TO POST
    $(document).on('click', '.send_btn_js', function() {
        SendPostComment($(this));
    });

    function SendPostComment(elem){
        var comment_block = elem.closest('.comment_block');
        var text = comment_block.find('.comment_text_field').html();
        var post_id = comment_block.find('input[name=post_id]').val();
        var parent_id = comment_block.find('input[name=comment_id]').val();
        var comment_id = comment_block.find('form input[name=comment_id]').val();

        if (elem.hasClass('send_btn_js')) {
            if (text != '' && comment_id == ''){
                Loader(elem.closest('.comment_block'), 'add');

                $.ajax({
                    type: 'POST',
                    url: '/comments',
                    data: { parent_id: parent_id, post_id: post_id, comment: text }

                }).done(function(data) {

                    Loader(elem.closest('.comment_block'));

                    if (comment_block.closest('.post_comment').hasClass('new_comment')) {
                        $('.post_comments').append(data.html);

                    } else {
                        comment_block.closest('.post_comment').append(data.html);
                        comment_block.closest('.post_comment').find('.reply_block').addClass('hidden');
                        comment_block.closest('.post_comment').find('.reply_btn').removeClass('hidden');
                    }

                    comment_block.find('.comment_text_field').html('*************');

                }).fail(function(data) {
                    Snakbar('error');
                });

            } else {
                if (text != '') {
                    Loader(elem.closest('.comment_block'), 'add');

                    $.ajax({
                        type: 'PUT',
                        url: '/comments/' + comment_id,
                        data: { comment: text }

                    }).done(function(data) {

                        Loader(elem.closest('.comment_block'));

                        if (comment_block.closest('.post_comment').hasClass('new_comment')) {
                            $('.post_comments').append(data.html);

                        } else {
                            comment_block.closest('.post_comment').append(data.html);
                            comment_block.closest('.post_comment').find('.reply_block').addClass('hidden');
                            comment_block.closest('.post_comment').find('.reply_btn').removeClass('hidden');
                        }
                        comment_block.find('.comment_text_field').html('');

                    }).fail(function(data) {
                        Snakbar('error');
                    });
                }
            }
        } else {
            if (comment_id == '') {
                $.ajax({
                    type: 'POST',
                    url: '/comments',
                    data: { parent_id: parent_id, post_id: post_id, comment: text }

                }).done(function(data) {

                    Loader(elem.closest('.comment_block'));

                    comment_block.find('form input[name=comment_id]').val(data.id);
                    comment_block.find('form').submit();

                }).fail(function(data) {
                    Snakbar('error');
                });

            } else {
                $.ajax({
                    type: 'PUT',
                    url: '/comments/' + comment_id,
                    data: { comment: text }

                }).done(function(data) {

                    Loader(elem.closest('.comment_block'));

                }).fail(function(data) {
                    Snakbar('error');
                });
            }
        }
    }

    function SendPostDescription(elem) {
        var content = elem.closest('.comment_block').find('.text_container').html();
        var post_id = elem.closest('.post').find('input[name=post_id]').val();
        var images = [];

        elem.closest('.comment_block').find('.text_container img').each(function() {
            images.push($(this).attr('src'));
        });

        Loader(elem.closest('.comment_block'), 'add');

        $.ajax({
            type: "POST",
            url: "/cabinet/post/updatepostcomment",
            data: { post_id: post_id, content: content, images: images }

        }).done(function(data) {
            Loader(elem.closest('.comment_block'));

        }).fail(function(data) {
            Snakbar('error8888888');
        });
    }

    
    

    // TAGS: ADD/REMOVE   https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/
    $('.tags_input_js').on('beforeItemAdd', function(event) {
        var tag = event.item;
        var id = $(this).closest('.post').find('.post_id').val();
        alert(id);
        var input = $(this);

        //if (!event.options || !event.options.preventPost) {
            $.ajax({
                type: "POST",
                url: '/tags',
                data: { post_id: id, name: tag, _token: token }

            }).done(function(data) {
                Snakbar(data.message);

            }).fail(function(data) {
                Snakbar('error tag post');
                input.tagsinput('remove', tag, {preventPost: true});
            });
       // }
    });
    
    $('.tags_input_js').on('beforeItemRemove', function(event) {
        var tag = event.item;
        var id = $(this).closest('.post').find('.post_id').val();
        var input = $(this);

        if (!event.options || !event.options.preventPost) {
            $.ajax({
                type: "DELETE",
                url: '/tags',
                data: { post_id: id, name: tag, _token: token, _method: 'DELETE' }

            }).done(function(data) {
                Snakbar(data.message);

            }).fail(function(data) {
                Snakbar('error');
                input.tagsinput('add', tag, { preventPost: true });
            });
        }
    });


    // DRAG & DROP MAIN PHOTO
    $('.dragbox').each(function() {
       
        $(this).click(function() {
             alert("DRAG & DROP");
           $(this).siblings('.dragbox-content').toggle();
        }).end();
    });

    $('.drop_zone').sortable({
        connectWith: '.photos_block', 
        handle: '.dragbox-content', 
        placeholder: 'add_img_wrap',
        forcePlaceholderSize: true, //Если true, заставляет заполнитель иметь размер.
        opacity: 0.4,

        start: function() {
            // some code when start drag
        },

        stop: function(event, ui) {
            var arr = [];

            $(this).find('img').each(function() {
                arr.push($(this).data("id"));
            });

            $.ajax({
                type: "POST",
                url: "/cabinet/showcatalog/photomove",
                data: { arr: arr, _token: token }

            }).done(function(data) {
            }).fail(function(data){
                Snakbar('error');
            });


        }
    }).disableSelection();  // Отключить выделение текстового содержимого в наборе совпадающих элементов.

    // DROPZONE
    Dropzone.autoDiscover = false;

    // var deferred = $.Deferred();

    // deferred.done(function() {

    var dropzoneOptions = {
        method: 'POST',
        paramName: 'photo',
        url: "/cabinet/showcatalog/store",
        clickable: true,
        // addRemoveLinks: true,
        // dictRemoveFile: true,
        chunking: false,
        // chunkSize: 8000000,
        // maxFilesize: 8000000,
        // parallelUploads: 9,
        // uploadMultiple: true,
        thumbnailWidth: 250,
        thumbnailHeight: 250,
        // resizeMethod: contain,
        maxThumbnailFilesize: 8000000,
        // maxFilesize: 8,
        maxFiles: 9,
        timeout: 0,
        // maxFilesize: 100,
        previewTemplate: document.querySelector('#preview-template').innerHTML,

        init: function() {   //Вызывается при инициализации dropzone. Здесь вы можете добавить //////   п //рослушиватели событий.
            this.on("addedfile", function(file) {
                $(this)[0].element.classList.remove("empty"); // $(this).get(0)
                $(this)[0].element.classList.remove("active");
                var id = $(this)[0].element.getAttribute('id');
                alert(id);
                LoaderDropZone(id, 'add');  // func from end this file
            });

            this.on("success", function(file, response) {  //  Gets the server response as second argument. //(This event was called finished previously)
                var src = response.success;
                alert(src);
                var id = response.ids;
                alert(id);
                var form_id = $(this)[0].element.getAttribute('id');
                alert(form_id);
                file.previewElement.children[0].setAttribute("src", response.success);  //Доступ к предварительному просмотру html файла
                file.previewElement.children[0].setAttribute("data-id", id);
                if(src == undefined){
                    DeleteExcessImages(file.previewElement);
                }
            });

            this.on("processing", function(file) {
                var id = $(this)[0].element.getAttribute('id');
                LoaderDropZone(id, 'add'); // func from end this file
            });

            this.on("error", function(file, errorMessage) {
                var id = $(this)[0].element.getAttribute('id');
                MrrorMessage(id, errorMessage);
            });

            this.on("queuecomplete", function() {
                var id = $(this)[0].element.getAttribute('id');
                LoaderDropZone(id); // func from end this file
            });

            this.on("dragover", function(file) {
                $(this)[0].element.className += " active";
            });

            this.on("dragleave", function(file) {  ///Пользователь перетащил файл из Dropzone
                $(this)[0].element.classList.remove("active");
            });
        }
    }

    var uploaders = $(document).find('.drop_zone');

    $(uploaders).each(function(i, el){
        $(el).addClass('dropzone_init_done').dropzone(dropzoneOptions);
    });

        // var myDropzone = $(document).find('.drop_zone').dropzone({
        //
        // });
    // });

    // deferred.resolve();

    

             

    
    
   
    // $(".drop_zone").dropzone({
    //     method: 'POST',
    //     paramName: 'photo',
    //     url: "/cabinet/showcatalog/store",
    //     clickable: true,
    //     // addRemoveLinks: true,
    //     // dictRemoveFile: true,
    //     chunking: true,
    //     chunkSize: 8000000,
    //     // maxFilesize: 8000000,
    //     // parallelUploads: 9,
    //     // uploadMultiple: true,
    //     thumbnailWidth: 250,
    //     thumbnailHeight: 250,
    //     maxThumbnailFilesize: 8000000,
    //     maxFilesize: 8,
    //     // maxFiles: 9,
    //     previewTemplate: document.querySelector('#preview-template').innerHTML,
    //     init: function() {
    //         this.on("addedfile", function(file) {
    //             $(this)[0].element.classList.remove("empty");
    //             $(this)[0].element.classList.remove("active");
    //             var id = $(this)[0].element.getAttribute('id');
    //             LoaderDropZone(id, 'add');
    //         });
    //         this.on("success", function(file, response) {
    //             var src = response.success;
    //             var id = response.ids;
    //             var form_id = $(this)[0].element.getAttribute('id');
    //             file.previewElement.children[0].setAttribute("src", response.success);
    //             file.previewElement.children[0].setAttribute("data-id", id);
    //             if(src == undefined){
    //                 DeleteExcessImages(file.previewElement);
    //             }
    //         });
    //         this.on("processing", function(file) {
    //             var id = $(this)[0].element.getAttribute('id');
    //             LoaderDropZone(id, 'add');
    //         });
    //         this.on("error", function(file, errorMessage) {
    //             var id = $(this)[0].element.getAttribute('id');
    //             MrrorMessage(id, errorMessage);
    //         });
    //         this.on("queuecomplete", function() {
    //             var id = $(this)[0].element.getAttribute('id');
    //             LoaderDropZone(id);
    //         });
    //         this.on("dragover", function(file) {
    //             $(this)[0].element.className += " active";
    //         });
    //         this.on("dragleave", function(file) {
    //             $(this)[0].element.classList.remove("active");
    //         });
    //     }
    // });
    // SHOW UPLOAD ERROR
    function MrrorMessage(id, message) {
        if (message.slice(0, 15) == 'File is too big'){
            $('#'+ id).closest('.add_photos').find('.error').removeClass('hidden');

        } else {
            Snakbar(message);
        }
    }

    
    // REPLACE SRC FOR DELETING & REMOVE EXCESS PHOTOS
    function DeleteExcessImages(file) {
        file.remove();
    }
    /*function ReplaceSrc(elem, id, src){
        var i = 0;
        $('#'+elem+'').find('.dz-image-preview img').each(function(){
            if(src[i] == undefined) {
                $(this).closest('.drop_zone').find('.dz-image-preview').each(function(){
                    $(this).remove();
                });
            }else{
                $(this).attr('src', src[i]);
                $(this).attr('data-id', id[i]);
                $(this).closest('.add_img_wrap').removeClass('dz-image-preview');
            }
            ++i;
        });
    }*/

    // REMOVE UPLOADED PHOTOS
    $(document).on('click', '.del_photos', function() {
        var element = $(this);
       
        var form = element.closest('.drop_zone');
        var src = element.closest('.add_img_wrap').find('img').attr('src');
        
        alert(src);
        if(element.closest('.add_img_wrap').hasClass('dz-error') && form.find('.dz-error').length == 1){
            element.closest('.add_photos').find('.error').addClass('hidden');
        }

        LoaderDropZone(element.closest('.drop_zone').attr('id'), 'add');

        $.ajax({
            type: "POST",
            url: "/cabinet/showcatalog/destroy",
            data: { src: src, _token: token }

        }).done(function(data) {
            LoaderDropZone(element.closest('.drop_zone').attr('id'));

            if(element.closest('.drop_zone').find('.add_img_wrap').length == 2){
                element.closest('.drop_zone').addClass('empty');
            }

            element.closest('.add_img_wrap').remove();

        }).fail(function(data){
            Snakbar('error');
        });
    });

    
    
    
    
    
    
   
    // SHOW MORE
    $('.show_more_btn').on('click', function() {
        var page = parseInt($('#current_page').val()) + 1;
        var get_params = $('body').find('.get_params').val();
        
       
        if (get_params == '') {
            url = '?page=' + page;

        } else {
            url = '?' + get_params + '&page=' + page;
        }
        alert(url);
        Loader($('body'), 'add');
        $('.loader').css('position', 'fixed');

        $.ajax({
            url: url,  // ShowCatalog
            type: 'GET',
            datatype: "html",

        }).done(function(data){
            alert(data.html);
            $('section .container').append(data.html);
            //$(this.find('.container')).append(data.html);
                // myFunction();

            $('.tags_input_js').tagsinput('refresh');
                $('.tags_input_js').on('beforeItemAdd', function(event) {
        var tag = event.item;//   https://bootstrap-tagsinput.github.io/bootstrap-tagsinput/examples/
        alert(tag);
        var id = $(this).closest('.post').find('.post_id').val();
        var input = $(this);
        alert(input);
                    
        if (!event.options || !event.options.preventPost) {
            $.ajax({
                type: "POST",
                url: '/tags',
                data: { post_id: id, name: tag, _token: token }

            }).done(function(data) {
                Snakbar(data.message);

            }).fail(function(data) {
                Snakbar('error');
                input.tagsinput('remove', tag, {preventPost: true});
            });
        }
                 });


            $('#current_page').val(page);
            // inizialized slider
            $('.instagram_slider').owlCarousel({
                loop: true,
                items:	1,
                nav: true,
                smartSpeed: 500,
                navText: [
                    '<p class="slider-nav-btn prew-btn"><i class="fa fa-angle-left" aria-hidden="true"></i></p>',
                    '<p class="slider-nav-btn next-btn"><i class="fa fa-angle-right" aria-hidden="true"></i></p>'
                ]
            });

            // inizialized modal slider
            $('.modal_slider').owlCarousel({
                loop: true,
                items:	1,  // Кол-во отображаемых элементов в слайдере
                mouseDrag: false, //Перелистывание элементов зажатой мышкой
                touchDrag: false, //Перелистывание касанием 
                nav: true,
                smartSpeed: 0, //Время движения слайда
                navText: [  //Текст на кнопках навигации. HTML разрешен.
                    '<p class="slider-nav-btn prew-btn"><i class="fa fa-angle-left" aria-hidden="true"></i></p>',
                    '<p class="slider-nav-btn next-btn"><i class="fa fa-angle-right" aria-hidden="true"></i></p>'
                ]
            });

            Loader($('body'));

            $('.instagram_slider').parent('.photo_wrap').append('<div class="loader"><div class="spiner"></div></div>');
            $('.instagram_slider').trigger('destroy.owl.carousel');

            instaSlider = $('.instagram_slider');
            instaSlider.owlCarousel(instagramSliderParams);

            instaSlider.on('loaded.owl.lazy', function(e) {
                $(this).parent('.photo_wrap').find('.loader').addClass('hidden');
            });

            

            var uploaders = $(document).find('.drop_zone');

            $(uploaders).each(function(i, el) {
                if ($(el).hasClass('dropzone_init_done') == false) {
                    $(el).dropzone(dropzoneOptions).addClass('dropzone_init_done');
                    $(el).append('<div class="loader hidden"><div class="spiner"></div></div>');
                }
            });

            if (data.last_page == true) {
                $('.show_more_btn').remove();
            }

        }).fail(function() {
            Loader($('body'));
            Snakbar('error');
        });
    });
  
    
    
    
    
    
    // SNAKBAR FUNCTION
    function Snakbar(text) {
        $('.snackbar').html('<p><b>okeySnakbar</b>'+text+'</p>');
        $(".snackbar").slideToggle('fast');

        setTimeout(function() {
            $(".snackbar").slideToggle('fast');
        }, 3000);

        // $('.snackbar').html('<p>'+text+'</p>').toggleClass('active');
        // setTimeout(function(){
        // 	$('.snackbar').removeClass('active').html('');
        // }, 3000);
    }

    // LOADER FUNCTION
    function Loader(elem, action) {
        if (action == 'add') {
            elem.append('<div class="loader"><div class="spiner"></div></div>');

        } else {
            elem.find('.loader').remove();
        }
    }

    function LoaderDropZone(id, action){
        if(action == 'add'){
            $('#'+ id).find('.loader').removeClass('hidden');
        }else{
            $('#'+ id).find('.loader').addClass('hidden');
        }
    }

});
