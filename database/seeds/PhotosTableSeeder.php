<?php

use Illuminate\Database\Seeder;

class PhotosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('photos')->insert([
            'id' => 100,
            'is_from_instagram'=> 1,
            'i_type' => 'image',
            'user_id'=> 100,
            'photable_id'=> 100,
            'photable_type'=> 'App\Models\Post',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('photos')->insert([
            'id' => 101,
            'is_from_instagram'=> 1,
            'i_type' => 'image',
            'user_id'=> 100,
            'photable_id'=> 101,
            'photable_type'=> 'App\Models\Post',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('photos')->insert([
            'id' => 102,
            'is_from_instagram'=> 1,
            'i_type' => 'image',
            'user_id'=> 100,
            'photable_id'=> 102,
            'photable_type'=> 'App\Models\Post',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
