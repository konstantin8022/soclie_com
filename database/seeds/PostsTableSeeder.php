<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'id' => 100,
            'i_likes_count'=>4,
            'i_comments_count'=>8,
            'user_id' => 100,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('posts')->insert([
            'id' => 101,
            'i_likes_count'=>4,
            'i_comments_count'=>8,
            'user_id' => 100,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('posts')->insert([
            'id' => 102,
            'i_likes_count'=>4,
            'i_comments_count'=>8,
            'user_id' => 100,
            'created_at' => now(),
            'updated_at' => now(),
        ]);


        DB::table('post_descriptions')->insert([
            'id' => 100,
            'content' => 'Я - бездушно сгенерированное описание поста 100',
            'post_id'=>100,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('post_descriptions')->insert([
            'id' => 101,
            'content' => 'Я - бездушно сгенерированное описание поста 101',
            'post_id'=>101,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        DB::table('post_descriptions')->insert([
            'id' => 102,
            'content' => 'Я - бездушно сгенерированное описание поста 102',
            'post_id'=>102,
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
