<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 100,
            'i_login' => str_random(10) . '_i_login',
            'i_real_name' => str_random(10) . '_i_real_name',
            'i_id' => rand(10000, 99999),
            'i_count_subscribes' => rand(1, 100),
            'i_count_subscribers' => rand(1, 100),
            'i_publications' => rand(1, 100),
            'email'=> 'kostya@pzi.ru',
            'created_at' => now(),
            'updated_at' => now(),
            'password' => bcrypt('secret'),
        ]);
    }
}
