<?php

use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'id' => 100,
            'content' => 'С виду я - самый обычный комментарий с id = 100, однако я написан пользователем с id = 100 - исскуственно сгенерированным пользователем, поэтому я не имею понятия, что изображено на фото, к посту которого я оставлен (( я знаю только то, что его id = 1!',
            'is_from_instagram'=>0,
            'user_id'=>100,
            'post_id'=>100,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

       
    }
}
