<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
           DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        App\Models\User::truncate();
        App\Models\Post::truncate();
        App\Models\Comment::truncate();
        App\Models\Photo::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $this->call(UsersTableSeeder::class);
        $this->call(PostsTableSeeder::class);
        $this->call(CommentsTableSeeder::class);
        //$this->call(PostDescriptionTableSeeder::class);
        $this->call(PhotosTableSeeder::class);
    }
}
