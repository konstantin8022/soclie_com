<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultValuesToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('i_real_name', 255)->nullable()->change();
            $table->string('i_id', 16)->nullable()->change();
            $table->integer('i_count_subscribes')->default(0)->change();
            $table->integer('i_count_subscribers')->default(0)->change();
            $table->integer('i_publications')->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
