<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('photos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedTinyInteger('position')->nullable();
            $table->string('i_id', 64)->nullable();
            $table->text('i_description')->nullable();
            $table->string('i_created_time', 64)->nullable();
            $table->text('description')->nullable();
            $table->string('i_type', 64)->nullable();
            $table->tinyInteger('is_from_instagram');
            $table->timestamps();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('photable_id')->unsigned();
            $table->text('photable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('photos');
    }
}
