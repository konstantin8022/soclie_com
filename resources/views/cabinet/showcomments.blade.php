@extends('layouts.app')

@section('var_title', 'Комментарии')
@section('var_bodyClass', 'comments')

@section('content')
        <h3>{{ __('cabinet.your_answers') }}</h3>
        @forelse($comments as $comment)
            <div class="post_comment">
                <!-- <h3>{!! $comment->post->description->content !!}</h3> -->
                <p>{!! $comment->post->description->getSmallDescription(50) !!}</p>

                <div class="post_comment_hader">
                    <span class="post_comment_rate {{ $comment->rating > 0  ? 'positive' : ($comment->rating != 0 ? 'negative' : '') }}">{{ $comment->rating }}</span>
                    <span>
                            <i class="fa fa-arrow-down dislike_btn {{ $comment->isUserDislikeFound() ? 'active' : '' }}" aria-hidden="true"></i>
                            <i class="fa fa-arrow-up like_btn {{ $comment->isUserLikeFound() ? 'active' : '' }}" aria-hidden="true"></i>
                        </span>
                    <img src="{{ $comment->user->getAvatarSrc() }}" alt="user_pick">
                    <span class="post_comment_autor"><a href="{{ route('index', $comment->user->i_login) }}">{{ $comment->user->i_login }}</a></span>
                    <span>{{ $comment->user->rating }}</span>
                    <span>{{ $comment->getFormattedTimeAgoString() }}</span>
                </div>

                <p>{!! $comment->content !!}</p>

                <a class="comment_btn reply_btn" href="{{ route('showpost', [$comment->post->getLinkPart(), $comment->id]) }}#comment{{ $comment->id }}">{{ __('cabinet.go_to_post') }}</a>
                <a class="comment_btn read_btn snack_js" href="{{ route('cabinet.markAsViewed') }}"
                   onclick="event.preventDefault();
                    document.getElementById('_markAsViewed{{ $comment->id }}').submit();">
                    {{ __('cabinet.mark_as_viewed') }}
                </a>
                <form id="_markAsViewed{{ $comment->id }}" action="{{ route('cabinet.markAsViewed') }}" method="POST" style="display: none;">
                    <input type="hidden" name="comment_id" value="{{ $comment->id }}">
                    {{ csrf_field() }}
                </form>
            </div>
        @empty
            {{ __('cabinet.empty_answers') }}
        @endforelse
@endsection

@section('additionalDivs')
@endsection