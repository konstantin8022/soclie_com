	
	</br></br>	
	<div class=" comment44  ">
                        <h2>{{ __('cabinet.about') }} </h2>
		<div class="comment_block">
                            {{ csrf_field() }}
                            <input type="hidden" name="post_id" value="{{ $user->id }} ">
                            <div class="text_container" contenteditable="true">
                            		{!! $user->contentab !!}
			</div>     
			<div class="comment_action_block">
                              
				 <div class="comment_action_icons hidden">
                                    <i class="flaticon-piggy-bank" aria-hidden="true"></i>
                                    <i class="flaticon-musical-note" aria-hidden="true"></i>
                                    <i class="flaticon-chat" aria-hidden="true"></i>
                                    <i class="flaticon-headphones" aria-hidden="true"></i>
                                </div>
                                <div class="comment_action_btn_block">
                                    <i class="send_about">отправить55</i>
                                    <i class="flaticon-smile smiles_dock_btn" aria-hidden="true"></i>
                                    <i class="flaticon-photo-camera upload_photo_btn" aria-hidden="true"></i>
                                </div>
                                <div class="comment_modal hidden">
                                    <span>{{ __('cabinet.youtube_link') }}</span>
                                    <input type="text" name="youtube_link" placeholder="{{ __('cabinet.paste_link') }}">
                                    <button class="upload_btn upload_video_js">{{ __('cabinet.upload_video') }}</button>
                                    
				<form method="post" class="ajax_form1" action="{{ route('cabinet.addAboutImage') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="post_id" value="{{$user->id}}" />
                                        <span>{{ __('cabinet.upload_image') }}</span>
					<label class="label_upload" for="upload{{ ($user->id == 1) ? '' : $user->id }}">{{ __('cabinet.choose_file') }}</label>
                                        <input id="upload{{ ($user->id == 1) ? '' : $user->id }}" class="upload_photo_input_js" type="file" accept="image/*" name="imgs[]" multiple hidden>                  

                                        <input class="upload_btn" type="submit" value="{{ __('cabinet.upload') }}" />
                                 </form>
                                 </div>
                            
			      </div>


		</div>
	</div>
</div>
