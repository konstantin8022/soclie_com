    <div class="profile_menu">

        <div>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="flaticon-logout" aria-hidden="true"></i>{{ __('cabinet.exit') }}
            </a>
        </div>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>

        <div>
            <a href="{{ route('cabinet.showFavorites') }}"><i class="flaticon-heart" aria-hidden="true"></i>{{ __('cabinet.favorites') }}
            </a>
        </div>
        <div>
            <a style="position: relative" href="{{ route('cabinet.showFollows') }}"><i class="flaticon-commerce" aria-hidden="true"></i>{{ __('cabinet.follows') }}
                @if($count = Auth::user()->followedUsers()->count())
                    <div class="be_bage">{{ $count }}</div>
                @endif
            </a>
        </div>
        {{--({{ Auth::user()->followedUsers()->count() }})--}}
    </div>
