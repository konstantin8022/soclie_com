<div class="last-synced-items {{ count($lastPosts) == 0 ? 'in-progress' : '' }}">
    <h2>lAst-synced-ITEM {{ __('cabinet.latest_widget_header') }}</h2>

    <div class="posts owl-carousel owl-theme">
        @if(count($lastPosts))
            @foreach ($lastPosts as $lastPost)
                @foreach ($lastPost->photos() as $lastPhoto)
                    <a href="{{ route('showpost', $lastPost->getLinkPart()) }}">
                        <div class="item">
                        <img src="{{ $lastPhoto->getThumbnailSrc() }}" alt="inst_img">
                        <span class="date">{{ \Carbon\Carbon::parse($lastPhoto->updated_at)->diffForHumans(null, false, true) }}</span>
                        </div>
                    </a>
                @endforeach
            @endforeach
        @endif
    </div>
</div>
