@foreach($posts as $post)
<div class="post">
    <div class="post_item_block instagram_photo">
        <h2 class="post_title_item_mob">{{ __('cabinet.instagram') }}</h2>
        @include('layouts.partials.icons', ['post'=>$post])
        <div class="photo">
            <div class="photo_wrap">
                @foreach($post->instagramPhotos()->get() as $photo)
                    @if($photo->isInstagramImage())
                        {{--                                    <img {{ $side['isEqual'] ? '' : $side['biggestSide'] . '=' . '' . $side['number'] . '' }} src="{{ $photo->getImageSrc() }}" alt="inst_img">--}}
                        <img src="{{ $photo->getImageSrc() }}" alt="inst_img">
                    @endif
                    @if($photo->isInstagramCarousel())
                        <div class="owl-carousel owl-theme instagram_slider">
                            @foreach($photo->getInstagramCarouselSrcs() as $src)
                                <div class="slider-item">
                                    {{--<img {{ $side['isEqual'] ? '' : $side['biggestSide'] . '=' . '' . $side['number'] . '' }} src="{{ $src }}" alt="inst_img">--}}
                                    <img class="owl-lazy" data-src="{{ $src }}" src="{{ $src }}" alt="inst_img">
                                </div>
                            @endforeach
                        </div>
                        <div class="instagram_album_ico">
                            <i class="fa fa-clone" aria-hidden="true"></i>
                        </div>
                    @endif
                @endforeach
            </div>
            @include('layouts.partials.photo-info', ['post'=>$post])
        </div>
    </div>
    <div class="post_item_block add_photos">
        <h2 class="post_title_item_mob">{{ __('home.additional_photo') }} <span>{{ __('cabinet.max_photo', ['count'=>\App\Models\Post::MAX_PHOTOS]) }}</span></h2>
        <form id="dropzone{{ $post->id }}" action="{{ route('cabinet.dropzoneStore') }}" method="post" class="drop_zone {{ $post->imagePhotos()->count() ? '' : 'empty' }}" enctype="multipart/form-data">
            <input type="hidden" name="post_id" value="{{ $post->id }}">
            {{ csrf_field() }}
            @forelse($post->imagePhotos()->get() as $photo)
                <div class="add_img_wrap dragbox">
                    <img class="dragbox-content" src="{{ $photo->getThumbnailSrc() }}" alt="inst_img" data-id="{{ $photo->id }}">                               <span class="del_photos"><i class="fa fa-times" aria-hidden="true"></i></span>
                </div>
            @empty
            @endforelse
            <div class="dz-default dz-message drop_zone_message add_img_wrap">
                <div class="drop_zone_ico">+</div>
                <span class="drop_desk_text">{{ __('cabinet.drag_and_drop') }}</span>
                <span class="drop_mob_text">{{ __('cabinet.add_photo') }}</span>
            </div>
            <div class="loader hidden"><div class="spiner"></div></div>
        </form>
        <div class="error hidden">
            <span></span> {{ __('cabinet.big_file_text') }}
        </div>
    </div>
    <div class="post_item_block comment">
        <h2 class="post_title_item_mob">{{ __('cabinet.comment') }} <span>{{ __('cabinet.write_comment') }}</span></h2>
        <div class="comment_block">
            {{ csrf_field() }}
            <input type="hidden" name="post_id" value="{{ $post->id }}">
            <div class="text_container" contenteditable="true">
                {!! $post->description->content !!}
            </div>
            <div class="comment_action_block">
                <div class="comment_action_icons hidden">
                    <i class="flaticon-piggy-bank" aria-hidden="true"></i>
                    <i class="flaticon-musical-note" aria-hidden="true"></i>
                    <i class="flaticon-chat" aria-hidden="true"></i>
                    <i class="flaticon-headphones" aria-hidden="true"></i>
                </div>
                <div class="comment_action_btn_block">
                    <span class="send_comment_btn">отправить</span>
                    <i class="flaticon-smile smiles_dock_btn" aria-hidden="true"></i>
                    <i class="flaticon-photo-camera upload_photo_btn" aria-hidden="true"></i>
                </div>
                <div class="comment_modal hidden">
                    <span>{{ __('cabinet.youtube_link') }}</span>
                    <input type="text" name="youtube_link" placeholder="{{ __('cabinet.paste_link') }}">
                    <button class="upload_btn upload_video_js">{{ __('cabinet.upload_video') }}</button>
                    <form method="post" class="ajax_form" action="{{ route('cabinet.addPostCommentImage') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="post_id" value="{{ $post->id }}" />
                        <span>{{ __('cabinet.upload_image') }}</span>
                        <label class="label_upload" for="upload{{ ($post->id == 1) ? '' : $post->id }}">{{ __('cabinet.choose_file') }}</label>
                        <input id="upload{{ ($post->id == 1) ? '' : $post->id }}" class="upload_photo_input_js" type="file" accept="image/*" name="imgs[]" multiple hidden>
                        <input class="upload_btn" type="submit" value="{{ __('cabinet.upload') }}" />
                    </form>
                </div>
            </div>
        </div>

        <input class="tags_input_js1" type="text" value="{{ $post->getTagsStr() }}" data-role="tagsinput" placeholder="{{ __('cabinet.add_tag') }}" />





<div class="icons">
        <input class="post_id" type="text" hidden value="{{ $post->id }}">
<div class="icon_wrap is_empty1"><i class="fas fa-eye  icon_js {{ $post->isPostDislikeFound() ? '' : 'active' }}" data-value="is_empty" aria-hidden="true"> </i><span></span></div>
</div>



    </div>
</div>
@endforeach
