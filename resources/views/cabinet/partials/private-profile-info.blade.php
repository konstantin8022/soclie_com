<div class="private-profile-info">
    <h2>{{ __('cabinet.private_profile_header') }}</h2>

    <div class="content">
        <img style="max-width: 100%; height: auto;" src="/img/private-info.png"/>

        <div class="check_again">
            <button class="check_again_btn">{{ __('cabinet.private_profile_check') }}</button>
        </div>
    </div>
</div>

