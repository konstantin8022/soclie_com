@extends('layouts.app')

@section('var_title', 'Мои подписки')
@section('var_bodyClass', 'rating')

@section('content')
        @forelse($users as $user)
        <a class="user" href="{{ route('index', $user->i_login) }}">
            <div class="user_order_number">{{ $loop->iteration }}</div>
            <div class="user_pick">
                <img src="{{ $user->getAvatarSrc() }}" alt="user_pick">
            </div>
            <div class="user_info_item user_name">
                <h2><span>{{ $user->i_login }}</span><span>{{ $user->i_real_name }}</span></h2>
            </div>
            <div class="user_info info_descr">
                <div class="user_info_item">
                    <div class="user_info_descr">
                        <div><span><i class="fa fa-star-o" aria-hidden="true"></i>{{ $user->rating }}</span> {{ __('cabinet.rating') }}</div>
                        <div><span>{{ count($user->posts()) }}</span> {{ __('cabinet.posts') }}</div>
                        <div><span>{{ count($user->followedUsers()->get()) }}</span> {{ __('cabinet.followers_count') }}</div>
                        <div><span>{{ $user->getPostsFavoritesCount() }}</span> {{ __('cabinet.favorites_count') }}</div>
                    </div>
                </div>
                <div class="user_info_item">
                    <div class="user_info_descr inst_item">
                        <div><span>{{ $user->i_publications }}</span> {{ __('cabinet.instagram_media') }}</div>
                        <div><span>{{ $user->i_count_subscribers }}</span> {{ __('cabinet.followers_count') }}</div>
                        <div><span>{{ $user->i_count_subscribes }}</span> {{ __('cabinet.follows_count') }}</div>
                        <div>instagram</div>
                    </div>
                </div>
            </div>
        </a>
        @empty
            {{ __('cabinet.follows_empty') }}
        @endforelse
@endsection

@section('additionalDivs')
@endsection