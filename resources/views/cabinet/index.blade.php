@extends('layouts.app')

@section('var_title', 'Мой профиль')
@section('var_bodyClass', 'profile')

@section('content')
    @include('cabinet.partials.profilemenu')
    @include('layouts.partials.user')
@endsection

@section('additionalDivs')
@endsection