
    <div class="icons">
        <input class="post_id" type="text" hidden value="{{ $post->id }}">
        <div class="icon_wrap like"><i class="flaticon-like icon_js {{ $post->isUserLikeFound() ? 'active' : '' }}" data-value="like" aria-hidden="true"></i><span>{{ $post->likes }}</span></div>
        <div class="icon_wrap dislike"><i class="flaticon-dislike icon_js {{ $post->isUserDislikeFound() ? 'active' : '' }}" data-value="dislike" aria-hidden="true"></i><span>{{ $post->dislikes }}</span></div>

        <div class="icon_wrap favor"><i class="flaticon-heart icon_js @if(Auth::check()) {{ Auth::user()->isUserFavoritesFound($post) ? 'active' : '' }} @endif" data-value="favor" aria-hidden="true"></i><span>{{ $post->favorites_count }}</span></div>


<!-- <div class="icon_wrap is_empty1"><i class="flaticon-piggy-bank icon_js {{ $post->isPostDislikeFound() ?  'active' : ''}}   " data-value="is_empty" aria-hidden="true"></i><span>{{ $post->is_empty }}</span></div> 
-->   
     <div class="icon_wrap"><a href="{{ route('showpost', $post->getLinkPart()) }}"><i class="flaticon-edit" aria-hidden="true"></i></a><span>{{ $post->getCommentsCount() }}</span></div>
        <div class="icon_wrap rating"><i class="flaticon-star" data-value="rating" aria-hidden="true"></i><span>{{ $post->rating }}</span></div>
 

   </div>
