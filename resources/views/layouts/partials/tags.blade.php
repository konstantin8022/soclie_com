@if(count($tags))
    <div class="tags">
        @forelse($tags as $tag)
            <a href="{{ route('index', ['tag', $tag->name])  }}">{{ $tag->name }}</a>
        @empty
            {{--{{ __('home.no_tags') }}--}}
        @endforelse
    </div>
@endif