
    <div class="modal hidden">
        
        <div class="modal_back"><i class="fa fa-times close_modal" aria-hidden="true"></i></div>
        <div class="modal_content">
            <div class="owl-carousel owl-theme modal_slider">
                    @if(!empty($carouselSrcs))
                    @foreach($carouselSrcs as $src)
                    <div class="slider-item">
                        <div class="slide_img">
                            <img src="{{ $src }}" alt="slide"/>
                        </div>
                        <div class="slide_text_wrap">
                            <span class="photo_title">{{ __('home.modal_instagram_photo') }}</span>
                            <div class="slide_text_header">
                                <div class="slide_text_img">
                                     <p>carouselSrcs_modal_for_thumb</p>
                                    <a href="{{ route('index', $post->user->i_login) }}"><img src="{{ $post->user->getAvatarSrc() }}" alt="user_pick"></a>
                                </div>
                                <div class="slide_header_text">
                                    <a href="{{ route('index', $post->user->i_login) }}"><p class="slide_username">{{ $post->user->i_login }}</p></a>
                                    <p></p>
                                </div>
                            </div>
                            @include('layouts.partials.modal-right', ['post'=>$post])
                        </div>
                    </div>
                    @endforeach
                    @endif
               
                    @foreach($imagePhotos as $photo)
                        @if($loop->first && is_object($photo) && $photo->isInstagramCarousel()) @continue @endif
                        <div class="slider-item">
                            <div class="slide_img">
                                <img src="{{ isset($mode) && $mode == 'carousel' ? $photo : $photo->getImageSrc() }}" alt="slide"/>
                            </div>
                            <div class="slide_text_wrap">
                                 <p>carouselSrcs_modal_for_thumb</p>
                                <span class="photo_title">{{ is_object($photo) && $photo->isSiteImage() ? __('home.modal_additional_photo') : __('home.modal_instagram_photo') }}</span>
                                <div class="slide_text_header">
                                    <div class="slide_text_img">
                                        <a href="{{ route('index', $post->user->i_login) }}"><img src="{{ $post->user->getAvatarSrc() }}" alt="user_pick"></a>
                                    </div>
                                    <div class="slide_header_text">
                                        <a href="{{ route('index', $post->user->i_login) }}"><p class="slide_username">{{ $post->user->i_login }}</p></a>
                                        <p></p>
                                    </div>
                                </div>
                                @include('layouts.partials.modal-right', ['post'=>$post])
                            </div>
                        </div>
                    @endforeach
            </div>
        </div>
    </div>