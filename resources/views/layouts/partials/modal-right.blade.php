<div class="slide_text">
    <div class="slide_comment">
        @if(!empty($post->description->content))
        <a href="{{ route('index', $post->user->i_login) }}"><span class="slide_username">{{ $post->user->i_login }}</span></a>
        {!! $post->description->content !!}
        @else
        <p>{{ __('layout.post_description_empty') }}</p>
        @endif
        @foreach($post->tags as $tag)
        <a href="{{ route('index', ['tag', $tag->name]) }}"><span class="hashtag">{{ $tag->name }}</span></a>
        @endforeach
        <br /><br />
        @forelse($post->comments as $comment)
        <a href="{{ route('index', $comment->user->i_user) }}"><span class="slide_username">{{ $comment->user->i_login }}</span></a>
        <p>{!! $comment->content !!}</p>
        @empty
        <p>{{ __('layout.post_comments_empty') }}</p>
        @endforelse


        <i class="fa fa-times delete_comment hidden" aria-hidden="true"></i>
    </div>
</div>
<div class="slide_text_footer">
    <span>{{ $post->user->i_login }}</span>
    <i class="fa fa-heart" aria-hidden="true"></i><span>{{ $post->i_likes_count }}</span>
    <i class="fa fa-comment" aria-hidden="true"></i><span>{{ $post->i_comments_count }}</span>
</div>
