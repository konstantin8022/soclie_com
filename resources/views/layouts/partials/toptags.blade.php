
<div class="filters">
<div class="select_wrap ">
<div class="tags">
	<string> # </string>
	
	    <a class="{{ url('/') == request()->url() ? 'be_active' : '' }}" href="{{ url('/') }}" ><span class="iii"> all</span> </a>

	 @foreach($topTags as $tags)
                <a class="{{ route('index', ['tag', $tags->name]) == request()->url() ? 'be_active' : '' }}" href="{{ route('index', ['tag', $tags->name]) }}"  > <span class="iii"> {{ $tags->name }}</span></a>
         @endforeach 
</div>
</div>
</div>

