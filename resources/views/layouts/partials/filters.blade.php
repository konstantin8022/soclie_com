
    <div class="filters">
        <div class="select_wrap ">
            {{--<span class="filter_title select_title">{{ __('home.sort_by.'.request('by', (isset($withOnlyPosts) && $withOnlyPosts) ? 'only_posts' : 'instagram')) }}</span><i class="fa fa-angle-down" aria-hidden="true"></i>--}}
            <span class="filter_title select_title">{{ __('home.sort_by.' . (!empty($defaults['filter1']) ? $defaults['filter1'] : $defaults['by'])) }}</span><i class="fa fa-angle-down" aria-hidden="true"></i>
            <div class="select">
                <ul>
                    <li><a href="{{ \App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['by'=>'instagram'], $defaults) }}">{{ __('home.sort_by.instagram') }}</a></li>
                    @if(isset($withOnlyPosts) && $withOnlyPosts)
                        {{--                        <li><a href="{{ request()->fullUrlWithQuery(['by'=>'only_posts']) }}">{{ __('home.sort_by.only_posts') }}</a></li>--}}
                        <li><a href="{{ \App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['by'=>'only_posts'], $defaults) }}">{{ __('home.sort_by.only_posts') }}</a></li>
                    @endif
                    <li><a href="{{ \App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['by'=>'im_likes'], $defaults) }}">{{ __('home.sort_by.im_likes') }}</a></li>
                    <li><a href="{{ \App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['by'=>'rating'], $defaults) }}">{{ __('home.sort_by.rating') }}</a></li>
                    <li><a href="{{ \App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['by'=>'favorites'], $defaults) }}">{{ __('home.sort_by.favorites') }}</a></li>
                    <li><a href="{{ \App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['by'=>'by_comments'], $defaults) }}">{{ __('home.sort_by.by_comments') }}</a></li>
                </ul>
            </div>
        </div>
        <div class="select_wrap ">
            <span class="filter_title select_title">{{ __('home.time.' . (!empty($defaults['filter2']) ? $defaults['filter2'] : $defaults['on'])) }}</span><i class="fa fa-angle-down" aria-hidden="true"></i>
            <div class="select">
                <ul>
                    <li><a href="{{ \App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['on'=>'all'], $defaults) }}">{{ __('home.time.all') }}</a></li>
                    <li><a href="{{ \App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['on'=>'week'], $defaults) }}">{{ __('home.time.week') }}</a></li>
                    <li><a href="{{ \App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['on'=>'month'], $defaults) }}">{{ __('home.time.month') }}</a></li>
                </ul>
            </div>
        </div>
    </div>

