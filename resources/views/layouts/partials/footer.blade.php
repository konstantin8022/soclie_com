	{{--@if(session('message', ''))--}}
		{{--<script type="text/javascript">--}}
            {{--setTimeout(function(){--}}
                {{--//Что-то из этого срабатывает--}}
                {{--window.location.reload(1);--}}
                {{--location.reload;--}}
                {{--window.location.reload();--}}
                {{--window.location=window.location;--}}
                {{--window.location.reload(true); // use this if you want to remove cache--}}
            {{--}, 5000);--}}
		{{--</script>--}}
		{{--<div class="snackbar active">{{ session('message', '') }}</div>--}}
	{{--@else--}}
		{{--<div class="snackbar"></div>--}}
	{{--@endif--}}

	<div class="snackbar"></div>

	<input type="hidden" class="sm_need_auth" value="{{ __('layout.need_auth_for_action') }}" />
	<input type="hidden" class="get_params" value="{{ request()->getQueryString() }}" />

	<div id="preview-template" class="hidden">
		<div class="add_img_wrap dragbox dz-preview dz-file-preview">
			<img class="dragbox-content" src="{{ asset('uploads/no-image.png') }}" data-dz-thumbnail alt="thumb" />
			<span class="del_photos"><i class="fa fa-times" aria-hidden="true"></i></span>
		</div>
	</div>
	<input id="current_page" type="text" value="1" hidden>
	<footer>
		<div class="footer container">
			<p>{{ __('layout.rights') }}</p>
			<a href="#">{{ __('layout.write_to_us') }}</a>
		</div>
	</footer>
