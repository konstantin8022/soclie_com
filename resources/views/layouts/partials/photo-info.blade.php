    <div class="photo_info">
        <div>
            <span><a href="{{ route('index', $post->user->i_login) }}">{{ $post->user->i_login }}</a></span>
            <span><i class="fa fa-heart" aria-hidden="true"></i>{{ $post->i_likes_count }}</span>
            <span><i class="fa fa-comment" aria-hidden="true"></i>{{ $post->i_comments_count }}</span>
        </div>
        <div><i class="fa fa-star-o" aria-hidden="true"></i>{{ $post->user->rating }}</div>
    </div>