@section('content')
    @parent
 	
    @include('layouts.partials.filters')
 
   @if(count($posts) > 0)
        <div class="post_title">
            <h1 class="post_title_item instagram_photo">{{ __('home.instagram') }}</h1>
            <h2 class="post_title_item add_photos">{{ __('home.additional_photo') }}</h2>
            <h2 class="post_title_item comment">{{ __('home.comments') }}</h2>
        </div>
        @include('home.partials.pagination-item', $posts)
    @else
        <div style="padding-left: 110px; padding-top: 40px">{{ __('home.posts_not_found') }}</div>
    @endif
@endsection

@section('additionalDivs')
    @parent
    @if(!empty($posts) && $posts->currentPage() != $posts->lastPage())
        <button class="btn show_more_btn">{{ __('home.show_more') }}</button>
    @endif
@endsection
