
    <div class="user">
        <div class="user_pick">
            <img src="{{ $user->getAvatarSrc() }}" alt="user_pick">
        </div>
        <div class="user_info">
            <h1 class="user_info_item user_name"><span>{{ $user->i_login }}</span><span>{{ $user->i_real_name }}</span></h1>
            <div class="user_info_item">
                <div class="user_info_descr_wrap">
                    <div class="user_info_title">
                        <span>{{ __('cabinet.instagram') }}</span>
                    </div>
                    <div class="user_info_descr">
                        <div><span>{{ $user->i_publications }}</span> {{ __('cabinet.instagram_media') }}</div>
                        <div><span>{{ $user->i_count_subscribers }}</span> {{ __('cabinet.followers_count') }}</div>
                        <div><span>{{ $user->i_count_subscribes }}</span> {{ __('cabinet.follows_count') }}</div>
                    </div>
                </div>
                @if(Auth::check() && Auth::user()->id != $user->id)
                <button class="btn snack_js">{{ __('cabinet.follow_on_instagram') }}</button>
                @endif
            </div>
            <div class="user_info_item">
                <div class="user_info_descr_wrap">
                    <div class="user_info_title">
                        <span>{{ __('cabinet.our_instagram') }}</span>
                    </div>
                    <div class="user_info_descr">
                        <div><span><i class="fa fa-star-o" aria-hidden="true"></i> {{ $user->rating }}</span> {{ __('cabinet.rating') }}</div>
                        <div><span>{{ $user->getAllOnlyPosts()->count() }}</span> {{ __('cabinet.posts') }}</div>
                        <div><span>{{ $user->followingUsers()->count() }}</span> {{ __('cabinet.followers_count') }}</div>
                        <div><span>{{ $user->getUserFavoritesCount() }}</span> {{ __('cabinet.favorites_count') }}</div>
                    </div>
                </div>
                @if(Auth::check() && Auth::user()->id != $user->id)
                    @if(!Auth::user()->isFollower($user))
                        <a href="{{ route('cabinet.follow', ['id'=>$user->id]) }}" class="btn snack_js">{{ __('cabinet.follow_on_site') }}</a>
                    @else
                        <a href="{{ route('cabinet.unfollow', ['id'=>$user->id]) }}" class="btn snack_js">{{ __('cabinet.unfollow_on_site') }}</a>
                    @endif
                @endif
            </div>
        </div>
    </div>

