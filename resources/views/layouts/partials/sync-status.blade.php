<section class="sync_status {{ Auth::user()->getSyncStatus() }}">
    @switch(Auth::user()->getSyncStatus())
        @case('is-private')
            {{ __('layout.sync_status_is-private') }}
        @break;

        @case('in-progress')
            {{ __('layout.sync_status_in-progress') }}
        @break;

        @case('done')
            {{ __('layout.sync_status_done', ['date' => Auth::user()->getSyncedAt()]) }}
        @break;

        @case('error')
            {{ __('layout.sync_status_error') }}
        @break;
    @endswitch    
</section>    
