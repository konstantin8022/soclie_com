
    <header>
        <div class="header container">
            <a class="logo" href="{{ url('/') }}"><img src="{{ asset('img/logo.png') }}" alt="logo"></a>

            @include('layouts.partials.search')

            <div class="menu">
                <i class="fa fa-bars menu_mob_btn" aria-hidden="true"></i>
                <div class="nav_menu">
                    <div class="select_wrap lang">
                        <span class="select_title">GG{{ app()->getLocale() }}</span>
                        <div class="select">
                            <ul>
                                <li><a href="{{ url('/setlocale/eng') }}">Eng</a></li>
                                <li><a href="{{ url('/setlocale/rus') }}">Rus</a></li>
                            </ul>
                        </div>
                    </div>

                    @if(Auth::check())
                    <div class="menu_icons">
                        <a class="{{ route('cabinet.showrating') == request()->url() ? 'be_active' : '' }}" href="{{ route('cabinet.showrating') }}">
                            <i class="flaticon-star" aria-hidden="true"></i>
                            <div class="rate_count">{{ Auth::user()->rating  }}</div>
                        </a>
                        <a class="{{ url('/') == request()->url() ? 'be_active' : '' }}" href="{{ url('/') }}"><i class="flaticon-house" aria-hidden="true"></i></a>
                        <a class="{{ route('cabinet.showFavorites') == request()->url() ? 'be_active' : '' }}" href="{{ route('cabinet.showFavorites') }}"><i class="flaticon-heart" aria-hidden="true"></i></a>
                        <a class="{{ route('cabinet.showcomments') == request()->url() ? 'be_active' : '' }}" href="{{ route('cabinet.showcomments') }}">
                            <i class="flaticon-edit" aria-hidden="true"></i>
                            @if($commentsCount = \App\Models\Comment::getNewCommentsForUser(Auth::user())->count())
                                <div class="bage">{{ $commentsCount }}</div>
                            @endif
                        </a>
                        <a class="{{ route('cabinet.showcatalog') == request()->url() ? 'be_active' : '' }}" href="{{ route('cabinet.showcatalog') }}"><i class="flaticon-picture-with-frame" aria-hidden="true"></i></a>
                        <a class="{{ route('cabinet.index') == request()->url() ? 'be_active' : '' }}" href="{{ route('cabinet.index') }}"><i class="flaticon-avatar" aria-hidden="true"></i></a>
                    </div>
                    @else
                        <a  href="{{ route('register') }}">
                            <button href="{{ route('register') }}" class="btn_for_auth">{{ __('auth.register_btn') }}</button>
                        </a>
                        <a  href="{{ route('login') }}">
                            <button href="{{ route('login') }}" class="btn_for_auth">{{ __('auth.login_btn') }}</button>
                        </a>
                    @endif

                </div>
            </div>
        </div>
    </header>