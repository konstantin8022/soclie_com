<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0, width=device-width, shrink-to-fit=no">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>@yield('var_title')</title>

        <link rel="stylesheet" href="{{ asset('fonts/font-awesome/css/font-awesome.min.css')  }}">
        <link rel="stylesheet" href="{{ asset('fonts/ubuntu/font.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/icons/flaticon.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/nautiluspompilius/font.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
        <link rel="stylesheet" href="{{ asset('css/slider-pro.min.css') }}"/>
        <link rel="stylesheet" href="{{ asset('css/bootstrap-tagsinput.css') }}"/>
        <link rel="stylesheet" href="{{ asset('css/easy-autocomplete.min.css') }}"/>
        <link rel="stylesheet" href="{{ asset('css/dropzone.css') }}"/>
        <link rel="stylesheet" href="{{ asset('css/style.css?4') }}">
        
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"
                integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
                crossorigin="anonymous">
        </script>
        <script src="{{ asset('js/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('js/jquery.sliderPro.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap-tagsinput.min.js') }}"></script>
        <script src="{{ asset('js/jquery.easy-autocomplete.min.js') }}"></script>
        <script src="{{ asset('js/dropzone.js') }}"></script>
        <script src="{{ asset('js/main.js?7') }}"></script>

        <link  href="{{ asset('js/fancybox/dist/jquery.fancybox.css') }}" rel="stylesheet">
        <script src="{{ asset('js/fancybox/dist/jquery.fancybox.js') }}"></script>

        <!-- FAVICON -->
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-touch-icon.png') }}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon-32x32.png') }}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon-16x16.png') }}">
        <link rel="manifest" href="{{ asset('manifest.json') }}">
        <link rel="mask-icon" href="{{ asset('safari-pinned-tab.svg') }}" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">




<!-- VIDEO -->
         <link href="https://vjs.zencdn.net/4.2/video-js.css" rel="stylesheet">
    <link href="assets/style.css" rel="stylesheet">
    <script src="https://vjs.zencdn.net/4.2/video.js"></script>




    </head>
    <body class="@yield('var_bodyClass')">

        @include('layouts.partials.header')

        @if(Auth::check())
            @include('layouts.partials.sync-status')
        @endif

        <section>
            <div class="container">
                @yield('content')
            </div>
        </section>
        @section('additionalDivs')
        @show

        @include('layouts.partials.footer')

    </body>
</html>
