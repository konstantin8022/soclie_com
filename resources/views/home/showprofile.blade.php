@extends('layouts.app')

@section('var_title', 'Профиль пользователя ' . $user->i_login)
@section('var_bodyClass', 'profile user_profile')

@include('layouts.partials.posts-list')

@section('content')
    @include('layouts.partials.user')
@endsection





