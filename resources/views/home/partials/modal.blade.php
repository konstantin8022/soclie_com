
    <div class="modal hidden">
        <div class="modal_back"><i class="fa fa-times close_modal" aria-hidden="true"></i></div>
        <div class="modal_content">
            <div class="owl-carousel owl-theme modal_slider">

                    @if(!empty($carouselSrcs))
                    @php $c = 0; @endphp
                    @foreach($carouselSrcs as $src)
                    @php $c++; @endphp
                    <div class="slider-item">
                        <div class="slide_img">
                            <img src="{{ $src }}" data-index="{{ $c }}" alt="slide"/>
                        </div>
                        <div class="slide_text_wrap">
                            <span class="photo_title">{{ __('home.modal_instagram_photo') }}</span>
                            <div class="slide_text_header">
                                <div class="slide_text_img">
                                    <a href="{{ route('index', $post->user->i_login) }}"><img src="{{ $post->user->getAvatarSrc() }}" alt="user_pick"></a>
                                </div>
                                <div class="slide_header_text">
                                    <a href="{{ route('index', $post->user->i_login) }}"><p class="slide_username">{{ $post->user->i_login }}</p></a>
                                </div>
                                <span class="slide_header_date">
                                    {{ date('d.m.Y H:i:s', $post->created_at->timestamp) }}
                                </span>
                            </div>
                            @include('layouts.partials.modal-right', ['post'=>$post])
                        </div>
                    </div>
                    @endforeach
                    @endif

                    @php $c = 0; @endphp
                    @foreach($imagePhotos as $photo)
                        @php $c++; @endphp
                        @if($loop->first && is_object($photo) && $photo->isInstagramCarousel()) @continue @endif
                        <div class="slider-item">
                            <div class="slide_img">
                                <img src="{{ isset($mode) && $mode == 'carousel' ? $photo : $photo->getImageSrc() }}" data-index="{{ $c }}" alt="slide"/>
                            </div>
                            <div class="slide_text_wrap">
                                <span class="photo_title">{{ is_object($photo) && $photo->isSiteImage() ? __('home.modal_additional_photo') : __('home.modal_instagram_photo') }}</span>
                                <div class="slide_text_header">
                                    <div class="slide_text_img">
                                        <a href="{{ route('index', $post->user->i_login) }}"><img src="{{ $post->user->getAvatarSrc() }}" alt="user_pick"></a>
                                    </div>
                                    <div class="slide_header_text">
                                        <a href="{{ route('index', $post->user->i_login) }}"><p class="slide_username">{{ $post->user->i_login }}</p></a>
                                    </div>
                                    <span class="slide_header_date">
                                        @if(is_object($photo) && $photo->isSiteImage())
                                            {{ date('d.m.Y H:i:s', $photo->created_at->timestamp)  }}
                                        @elseif(is_object($photo) && !$photo->isSiteImage())
                                            {{ date('d.m.Y H:i:s', $photo->i_created_time) }}
                                        @else
                                            {{ date('d.m.Y H:i:s', $post->created_at->timestamp) }}
                                        @endif
                                        {{--{{ is_object($photo) && $photo->isSiteImage() ? date('d.m.Y H:i:s', $photo->created_at->timestamp) : date('d.m.Y H:i:s', $photo->i_created_time) }}--}}
                                    </span>
                                </div>
                                @include('layouts.partials.modal-right', ['post'=>$post])
                            </div>
                        </div>
                    @endforeach
            </div>
        </div>
    </div>