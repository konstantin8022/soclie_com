<div class="slider-item">
    <div class="slide_img">
        <img src="{{ $src }}" alt="slide" data-index="{{ $y }}" />
    </div>
    <div class="slide_text_wrap">
        <span class="photo_title">{{ __('home.modal_instagram_photo') }}</span>
        <div class="slide_text_header">
            <div class="slide_text_img">
                <a href="{{ route('index', $post->user->i_login) }}"><img src="{{ $post->user->getAvatarSrc() }}" alt="{{ $post->user->i_login }}"></a>
            </div>
            <div class="slide_header_text">
                <a href="{{ route('index', $post->user->i_login) }}"><p class="slide_username">{{ $post->user->i_login }}</p></a>
            </div>
            <span class="slide_header_date">
                {{ date('d.m.Y H:i:s', $post->created_at->timestamp) }}
            </span>
        </div>
        @include('layouts.partials.modal-right', ['post'=>$post])
    </div>
</div>