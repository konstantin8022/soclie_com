@foreach($replies as $reply)
    <div class="post_comment comment_answer"><span id="comment{{ $reply->id }}"></span>
        <div class="post_comment_hader">
            <span class="post_comment_rate {{ $reply->rating > 0  ? 'positive' : ($reply->rating != 0 ? 'negative' : '') }}">{{ $reply->rating }}</span>

            <span>
                <i class="fa fa-arrow-down dislike_btn {{ $reply->isUserDislikeFound() ? 'active' : '' }}" aria-hidden="true"></i>
                <i class="fa fa-arrow-up like_btn {{ $reply->isUserLikeFound() ? 'active' : '' }}" aria-hidden="true"></i>
			</span>

            <img src="{{ $reply->user->getAvatarSrc() }}" alt="user_pick">

            <span class="post_comment_autor"><a href="{{ route('index', $reply->user->i_login) }}">{{ $reply->user->i_login }}</a></span>
            <span>{{ $reply->user->rating }}</span>
            <span>{{ $reply->getFormattedTimeAgoString() }}</span>
        </div>

        <p>{!! $reply->content !!}</p>

        <div class="comment reply_block hidden">
            <div class="comment_block">
                <input type="hidden" name="post_id" value="{{ $post->id }}" />
                <input type="hidden" name="comment_id" value="{{ $reply->id }}" />
                <input type="hidden" name="parent_id" value="{{ $reply->parent->id }}" />

                <div class="comment_text_field text_container" contenteditable="true"></div>

                <div class="comment_action_block">
                    <div class="comment_action_icons hidden">
                        <i class="flaticon-piggy-bank" aria-hidden="true"></i>
                        <i class="flaticon-musical-note" aria-hidden="true"></i>
                        <i class="flaticon-chat" aria-hidden="true"></i>
                        <i class="flaticon-headphones" aria-hidden="true"></i>
                    </div>
                    <div class="comment_action_btn_block">
                        <i class="flaticon-smile smiles_dock_btn" aria-hidden="true"></i>
                        <i class="flaticon-photo-camera upload_photo_btn" aria-hidden="true"></i>
                    </div>
                    <div class="comment_modal hidden">
                        <span>{{ __('cabinet.youtube_link') }}</span>
                        <input type="text" name="youtube_link" placeholder="{{ __('cabinet.paste_link') }}">
                        <button class="upload_btn upload_video_js">{{ __('cabinet.upload_video') }}</button>
                        <form method="post" class="ajax_form" action="{{ route('cabinet.addCommentImage') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="comment_id" value="" />
                            <span>{{ __('cabinet.upload_image') }}</span>
                            <label class="label_upload" for="upload{{ $reply->id }}">{{ __('cabinet.choose_file') }}</label>
                            <input id="upload{{ $reply->id }}" class="upload_photo_input_js" type="file" accept="image/*" name="imgs[]" multiple hidden>
                            <input class="upload_btn" type="submit" value="{{ __('cabinet.upload') }}" />
                        </form>
                    </div>
                </div>

                @if($reply->parent->id == 0)
                    <div class="comment_btn send_btn send_btn_js">{{ __('home.comment_send') }}</div>
                    <div class="comment_btn cansel_btn ">{{ __('home.comment_cancel') }}</div>
                @endif
            </div>
        </div>

        @if($reply->parent->id == 0)
            <div class="comment_btn reply_btn">{{ __('home.comment_answer') }}</div>
        @endif
    @if($reply->replies->count())
        @include('home.partials.replies', ['replies' => $reply->replies])
    @endif
    </div>
@endforeach
