<div class="post_comment {{ $parent_id != 0 ? 'comment_answer' : '' }}"><span id="comment{{ $comment->id }}"></span>
   <div class="post_comment_hader">
        <span class="post_comment_rate">0</span>

        <span>
			<i class="fa fa-arrow-down dislike_btn" aria-hidden="true"></i>
            <i class="fa fa-arrow-up like_btn" aria-hidden="true"></i>
		</span>

	<p>comment from response controller to JSON</> 
        <img src="{{ $comment->user->getAvatarSrc() }}" alt="user_pick">

        <span class="post_comment_autor"><a href="{{ route('index', $comment->user->i_login) }}">{{ $comment->user->i_login }}</a></span>
        <span>{{ $comment->user->rating }}</span>
        <span>{{ $comment->getFormattedTimeAgoString() }}</span>
    </div>

    <p>{!! $comment->content !!}</p>

    <div class="comment reply_block hidden">
        <div class="comment_block">
            <input type="hidden" name="post_id" value="{{ $post->id }}" />
            <input type="hidden" name="comment_id" value="{{ $comment->id }}" />
            <input type="hidden" name="parent_id" value="{{ $parent_id }}" />
            <div class="comment_text_field text_container" contenteditable="true"></div>
            <div class="comment_action_block">
                <div class="comment_action_icons hidden">
                    <i class="flaticon-piggy-bank" aria-hidden="true"></i>
                    <i class="flaticon-musical-note" aria-hidden="true"></i>
                    <i class="flaticon-chat" aria-hidden="true"></i>
                    <i class="flaticon-headphones" aria-hidden="true"></i>
                </div>

                <div class="comment_action_btn_block">
                    <i class="flaticon-smile smiles_dock_btn" aria-hidden="true"></i>
                    <i class="flaticon-photo-camera upload_photo_btn" aria-hidden="true"></i>
                </div>

                <div class="comment_modal hidden">
                    <span>{{ __('cabinet.youtube_link') }}</span>
                    <input type="text" name="youtube_link" placeholder="{{ __('cabinet.paste_link') }}">
                    <button class="upload_btn upload_video_js">{{ __('cabinet.upload_video') }}</button>
                    <form method="post" class="ajax_form" action="{{ route('cabinet.addCommentImage') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="comment_id" value="" />
                        <span>{{ __('cabinet.upload_image') }}</span>
                        <label class="label_upload" for="upload{{ $comment->id }}">{{ __('cabinet.choose_file') }}</label>
                        <input id="upload{{ $comment->id }}" class="upload_photo_input_js" type="file" accept="image/*" name="imgs[]" multiple hidden>
                        <input class="upload_btn" type="submit" value="{{ __('cabinet.upload') }}" />
                    </form>
                </div>
            </div>

            @if($parent_id == 0)
                <div class="comment_btn send_btn send_btn_js">{{ __('home.comment_send') }}</div>
                <div class="comment_btn cansel_btn cancel_btn_js">{{ __('home.comment_cancel') }}</div>
            @endif
        </div>
    </div>
    @if($parent_id == 0)
        <div class="comment_btn reply_btn">{{ __('home.comment_answer') }}</div>
    @endif
</div>
