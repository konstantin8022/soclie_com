@forelse($posts as $post)
    <div id="post{{ $post->id }}" class="post modal_block">
        @php $x=0; @endphp
        <div class="post_item_block instagram_photo">
            {{--<h2 class="post_title_item_mob">{{ __('home.instagram') }}</h2>--}}
            <h2 class="post_title_item_mob">{{ $post->user->i_login }}</h2>
            @include('layouts.partials.icons', ['post'=>$post])
            <div class="photo">
                <div class="photo_wrap">
                    <div class="loader">
                        <div class="spiner"></div>
                    </div>
                    @php $carouselSrcs = []; @endphp

                    @foreach($post->instagramPhotos()->get() as $photo)
                        <div class="owl-carousel owl-theme instagram_slider">
                        @if($photo->isInstagramImage())
                            @php $x++; @endphp
                            <img class="open_modal owl-lazy" src="{{ $photo->getThumbnailSrc() }}" data-src="{{ $photo->getThumbnailSrc() }}"  data-index="{{ $x }}" alt="inst_img">
                        @endif

                        @if($photo->isInstagramCarousel())
                                @foreach($photo->getInstagramCarouselSrcs(true) as $src)
                                    @php $x++; @endphp
                                    @php $carouselSrcs[] = $src; @endphp
                                    <div class="slider-item">
                                        {{-- <img class="open_modal" {{ $side['isEqual'] ? '' : $side['biggestSide'] . '=' . '' . $side['number'] . '' }} src="{{ $src }}" data-index="{{ $x }}" alt="inst_img">--}}
                                        <img class="open_modal owl-lazy" src="{{ $src }}"  data-src="{{ $src }}" data-index="{{ $x }}" alt="inst_img">
                                    </div>
                                @endforeach
                            <div class="instagram_album_ico">
                                <i class="fa fa-clone" aria-hidden="true"></i>
                            </div>
                        @endif

                        @forelse($post->imagePhotos()->get() as $photo)
                            @php $x++; @endphp

                            <img  class="open_modal owl-lazy" src="{{ $photo->getImageSrc() }}" data-src="{{ $photo->getImageSrc() }}" data-index="{{ $x }}" alt="add_img">
                        @empty
                            {{--{{ __('home.no_add') }}--}}
                        @endforelse

                        </div>
                    @endforeach
                </div>
                @include('layouts.partials.photo-info', ['post'=>$post])
            </div>
        </div>
        {{--@if($post->imagePhotos()->count())--}}
        <div class="post_item_block add_photos">
            @if($post->imagePhotos()->count())
                <h2 class="post_title_item_mob">{{ __('home.additional_photo') }}</h2>
            @endif
            <div class="photos_block">
                @forelse($post->imagePhotos()->get() as $photo)
                    @php $x++; @endphp
                    <div class="add_img_wrap">
                        <img class="open_modal" src="{{ $photo->getThumbnailSrc() }}" data-index="{{ $x }}" alt="add_img" thumb="true">
                    </div>
                @empty
                    {{--{{ __('home.no_add') }}--}}
                @endforelse
            </div>
            @if(!empty($carouselSrcs))
                @include('home.partials.modal', ['carouselSrcs'=>$carouselSrcs, 'imagePhotos' => $post->imagePhotos, 'post'=>$post])
            @else
                @include('home.partials.modal', ['imagePhotos' => $post->instagramAndImagePhotos, 'post'=>$post])
            @endif
        </div>
        {{--@endif--}}

{{--        @if(($content = $post->description->content) || ($post->tags()->count()))--}}

        <div class="post_item_block comment">
            @if(($content = $post->description->content))
                <h2 class="post_title_item_mob">{{ __('home.comments') }}</h2>
            @endif
            <div class="text">
                {{--<div class="{{ $post->description->getDescriptionLength() > 500 ? 'text_wrap' : '' }}">{!! $post->description->getSmallDescription(500) !!}</div>--}}
                <div class="text_wrap">{!! $post->description->content !!}</div>
                <a href="{{ route('showpost', $post->getLinkPart()) }}">{{ __('home.read_more') }}</a>
            </div>
            @include('layouts.partials.tags', ['tags'=>$post->tags])
        </div>

        {{--@endif--}}

    </div>
@empty
@endforelse