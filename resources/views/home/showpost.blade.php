@extends('layouts.app')

@section('var_title', 'Пост №' . $post->id)
@section('var_bodyClass', 'full_post')

@section('content')
    <div class="photo_info">
        <div>
            <span><a href="{{ route('index', $post->user->i_login) }}">{{ $post->user->i_login }}</a></span>
            <span><i class="fa fa-heart" aria-hidden="true"></i>{{ $post->i_likes_count }}</span>
            <span><i class="fa fa-comment" aria-hidden="true"></i>{{ $post->i_comments_count }}</span>
            <span><i class="fa fa-star-o" aria-hidden="true"></i>{{ $post->user->rating }}</span>
        </div>
    </div>
    @include('layouts.partials.icons', ['post'=>$post])

    <div class="post_slider slider_inst">
        <div class="slider-pro modal_block" id="my-slider">

            @php $x=0; @endphp
            @php $y=0; @endphp

            @foreach($post->instagramPhotos()->get() as $photo)

                @if($photo->isInstagramImage())

                    @php $x++; @endphp
                    @php $y++; @endphp
                    <div class="sp-slides">
                        <div class="sp-slide">
                            {{--<a data-options='{"clickContent" : false, "touch" : true, "keyboard" : true, "wheel" : true, "arrows" : true}' data-fancybox="group" data-caption="{{ __('home.modal_instagram_photo') }}" href="{{ $photo->getImageSrc() }}">--}}
                                <img class="open_modal_2 sp-image" src="{{ $photo->getImageSrc() }}" data-index="1" alt="slide" />
                                @push('modal-item')
                                    @include('home.partials.modal-items', ['src'=>$photo->getImageSrc(), 'post'=>$post])
                                @endpush
                            {{--</a>--}}
                            <img class="sp-thumbnail" src="{{ $photo->getImageSrc() }}" alt="slide_thumb"/>
                        </div>
                    </div>

                    @include('home.partials.modal', ['imagePhotos' => $post->instagramPhotos, 'post'=>$post])

                @elseif($photo->isInstagramCarousel())
                    <div class="sp-slides">
                        @foreach($photo->getInstagramCarouselSrcs() as $src)
                            @php $x++; @endphp
                            @php $y++; @endphp
                            <div class="sp-slide">
                                {{--<a data-options='{"clickContent" : false, "touch" : true, "keyboard" : true, "wheel" : true, "arrows" : true}' data-fancybox="group" data-caption="{{ __('home.modal_instagram_photo') }}" href="{{ $src }}">--}}
                                <img class="open_modal_2 sp-image" src="{{ $src }}" data-index="{{ $x }}" alt="slide"/>
                                @push('modal-item')
                                    @include('home.partials.modal-items', ['src'=>$src, 'post'=>$post])
                                @endpush
                                {{--</a>--}}
                                <img class="sp-thumbnail" src="{{ $src }}" alt="slide_thumb"/>
                            </div>
                        @endforeach
                    </div>
                    <div class="slider-nav-btn prew-btn"><img src="{{ asset('img/arr_left.png') }}" alt="arr_left"></div>
                    <div class="slider-nav-btn next-btn"><img src="{{ asset('img/arr_right.png') }}" alt="arr_right"></div>

                    {{--@include('home.partials.modal', ['imagePhotos' => $photo->getInstagramCarouselSrcs(), 'post'=>$post, 'mode'=>'carousel'])--}}

                @endif
            @endforeach

            @stack('modal')
        </div>
        <div class="photo_info_mob">
            <div>
                <span><a href="{{ route('index', $post->user->i_login) }}">{{ $post->user->i_user }}</a></span>
                <span><i class="fa fa-heart" aria-hidden="true"></i>{{ $post->i_likes_count }}</span>
                <span><i class="fa fa-comment" aria-hidden="true"></i>{{ $post->i_comments_count }}</span>
                <span><i class="fa fa-star-o" aria-hidden="true"></i>{{ $post->rating }}</span>
            </div>
        </div>
        <!--div class="tags hidden">
            <div>Солнце</div>
            <div>Пляж</div>
        </div-->
    </div>


    <!-- SLIDER-2 -->
    <div class="post_slider slider_new_inst">
        <div class="slider-pro modal_block" id="my-slider2">
            <div class="sp-slides">
                @forelse($post->imagePhotos()->get() as $photo)
                    @php $x++; @endphp
                    @php $y++; @endphp
                    <div class="sp-slide">
                        {{--<a data-options='{"clickContent" : false, "touch" : true, "keyboard" : true, "wheel" : true, "arrows" : true}' data-fancybox="group" data-caption="{{ __('home.modal_additional_photo') }}" href="{{ $photo->getImageSrc() }}">--}}
                        <img class="open_modal_2 sp-image" src="{{ $photo->getImageSrc() }}" alt="slide" data-index="{{ $x }}" />
                        @push('modal-item')
                            @include('home.partials.modal-items', ['src'=>$photo->getImageSrc(), 'post'=>$post])
                        @endpush
                        {{--</a>--}}
                        <img class="sp-thumbnail" src="{{ $photo->getImageSrc() }}" alt="slide_thumb"/>
                    </div>
                @empty
                    @php $x++; @endphp
                    @php $y++; @endphp
                    <div class="sp-slide">
                        {{--<a data-options='{"clickContent" : false, "touch" : true, "keyboard" : true, "wheel" : true, "arrows" : true}' data-fancybox="group" data-caption="{{ __('home.modal_additional_photo') }}" href="{{ $photo->getImageSrc() }}">--}}
                        <img class="open_modal_2 sp-image" src="{{ asset('uploads/no-image.png') }}" data-index="{{ $x }}" alt="slide"/>
                        @push('modal-item')
                            @include('home.partials.modal-items', ['src'=>asset('uploads/no-image.png'), 'post'=>$post])
                        @endpush
                        {{--</a>--}}
                    </div>
                @endforelse
            </div>
            <!-- <div class="slider-resize-btn"><img src="img/full_size.png" alt="arr_left"></div> -->
            <div class="slider-nav-btn prew-btn"><img src="{{ asset('img/arr_left.png') }}" alt="arr_left"></div>
            <div class="slider-nav-btn next-btn"><img src="{{ asset('img/arr_right.png') }}" alt="arr_right"></div>


                {{--@include('home.partials.modal', ['imagePhotos' => $post->imagePhotos])--}}





        </div>
        @include('layouts.partials.tags', ['tags' => $post->tags])
    </div>

    <div class="full_common_modal">

        @include('home.partials.modal-with-stack-items')
    </div>





    <div class="post_descr">
        <h2>{{ __('home.post_description') }}</h2>
        <p>{!! $post->description->content !!}</p>
    </div>
    <div class="post_comments">
        @forelse($comments as $comment)
            <div class="post_comment"><span id="comment{{ $comment->id }}"></span>
                <div class="post_comment_hader">
                    <span class="post_comment_rate {{ $comment->rating > 0  ? 'positive' : ($comment->rating != 0 ? 'negative' : '') }}">{{ $comment->rating }}</span>
                    <span>
							<i class="fa fa-arrow-down {{ Auth::check() ? 'dislike_btn' : '' }} {{ $comment->isUserDislikeFound() ? 'active' : '' }}" aria-hidden="true"></i>
							<i class="fa fa-arrow-up {{ Auth::check() ? 'like_btn' : '' }} {{ $comment->isUserLikeFound() ? 'active' : '' }}" aria-hidden="true"></i>
						</span>
                    <img src="{{ $comment->user->getAvatarSrc() }}" alt="user_pick">
                    <span class="post_comment_autor"><a href="{{ route('index', $comment->user->i_login) }}">{{ $comment->user->i_login }}</a></span>
                    <span>{{ $comment->user->rating }}</span>
                    <span>{{ $comment->getFormattedTimeAgoString() }}</span>
                </div>
                <p>{!! $comment->content !!}</p>
                <div class="comment reply_block hidden">
                    <div class="comment_block">
                        <input type="hidden" name="post_id" value="{{ $post->id }}" />
                        <input type="hidden" name="comment_id" value="{{ $comment->id }}" />
                        <input type="hidden" name="parent_id" value="0" />
                        <div class="comment_text_field text_container" contenteditable="true"></div>
                        <div class="comment_action_block">
                            <div class="comment_action_icons">
                                <i class="flaticon-piggy-bank" aria-hidden="true"></i>
                                <i class="flaticon-musical-note" aria-hidden="true"></i>
                                <i class="flaticon-chat" aria-hidden="true"></i>
                                <i class="flaticon-headphones" aria-hidden="true"></i>
                            </div>
                            <div class="comment_action_btn_block">
                                <i class="flaticon-smile smiles_dock_btn" aria-hidden="true"></i>
                                <i class="flaticon-photo-camera upload_photo_btn" aria-hidden="true"></i>
                            </div>
                            <div class="comment_modal hidden">
                                <span>{{ __('cabinet.youtube_link') }}</span>
                                <input type="text" name="youtube_link" placeholder="{{ __('cabinet.paste_link') }}">
                                <button class="upload_btn upload_video_js">{{ __('cabinet.upload_video') }}</button>
                                <form method="post" class="ajax_form" action="{{ route('cabinet.addCommentImage') }}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="comment_id" value="" />
                                    <span>{{ __('cabinet.upload_image') }}</span>
                                    <label class="label_upload" for="upload{{ $comment->id }}">{{ __('cabinet.choose_file') }}</label>
                                    <input id="upload{{ $comment->id }}" class="upload_photo_input_js" type="file" accept="image/*" name="imgs[]" multiple hidden>
                                    <input class="upload_btn" type="submit" value="{{ __('cabinet.upload') }}" />
                                </form>
                            </div>
                        </div>
                        <div class="comment_btn send_btn send_btn_js">{{ __('home.comment_send') }}</div>
                        <div class="comment_btn cansel_btn">{{ __('home.comment_cancel') }}</div>
                    </div>
                </div>
                @if(Auth::check())
                    <div class="comment_btn reply_btn">{{ __('home.comment_answer') }}</div>
                @endif
                @if($comment->replies->count())
                    @include('home.partials.replies', ['replies' => $comment->replies])
                @endif
            </div>
        @empty
            <div class="post_comment">{{ __('home.comments_empty') }}</div>
        @endforelse
    </div>
    <div class="comment reply_block post_comment new_comment">
        <div class="comment_block">
            @if(Auth::check())
                <h2>{{ __('home.comment_new_comment') }}</h2>
            <!-- <form action="{{ route('comments.store') }}" method="post"> -->
            <!-- {{ csrf_field() }} -->
                <input type="hidden" name="post_id" value="{{ $post->id }}" />
                <input type="hidden" name="parent_id" value="0" />
            <!-- <textarea class="comment_text_field" name="comment" cols="30" rows="10" placeholder="{{ __('home.comment_textarea_placeholder') }}"></textarea> -->
                <div class="comment_text_field text_container" contenteditable="true"></div>
                <div class="comment_action_block">
                    <div class="comment_action_icons hidden">
                        <i class="flaticon-piggy-bank" aria-hidden="true"></i>
                        <i class="flaticon-musical-note" aria-hidden="true"></i>
                        <i class="flaticon-chat" aria-hidden="true"></i>
                        <i class="flaticon-headphones" aria-hidden="true"></i>
                    </div>
                    <div class="comment_action_btn_block">
                        <i class="flaticon-smile smiles_dock_btn" aria-hidden="true"></i>
                        <i class="flaticon-photo-camera upload_photo_btn" aria-hidden="true"></i>
                    </div>
                    <div class="comment_modal hidden">
                        <span>{{ __('cabinet.youtube_link') }}</span>
                        <input type="text" name="youtube_link" placeholder="{{ __('cabinet.paste_link') }}">
                        <button class="upload_btn upload_video_js">{{ __('cabinet.upload_video') }}</button>
                        <form method="post" class="ajax_form" action="{{ route('cabinet.addCommentImage') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="comment_id" value="" />
                            <span>{{ __('cabinet.upload_image') }}</span>
                            <label class="label_upload" for="upload">{{ __('cabinet.choose_file') }}</label>
                            <input id="upload" class="upload_photo_input_js" type="file" accept="image/*" name="imgs[]" multiple hidden>
                            <input class="upload_btn" type="submit" value="{{ __('cabinet.upload') }}" />
                        </form>
                    </div>
                </div>
                <div class="comment_btn send_btn send_btn_js">{{ __('home.comment_send') }}</div>
            <!-- div class="comment_btn cansel_btn">{{ __('home.comment_cancel') }}</div -->
                <!-- </form> -->
            @else
                <h2>{{ __('home.comment_need_authorize') }}</h2>
            @endif
        </div>
    </div>
@endsection

@section('additionalDivs')
@endsection