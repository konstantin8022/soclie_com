@extends('layouts.app')

@section('content')
    <div class="auth_user container">
        <h1>{{ __('auth.register_title') }}</h1>

        <form method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}

            <div class="form-item">
                <label for="login">{{ __('auth.login_label') }}</label>
                <input id="login" type="text" name="i_login" value="{{ old('i_login') }}" required autofocus>
                <span class="help-block">{{ __('auth.login_info') }}</span>
                @if ($errors->has('i_login'))
                    <span class="help-block has-error">
                        {{ $errors->first('i_login') }}
                    </span>
                @endif
            </div>

            <div class="form-item">
                <label for="email">{{ __('auth.email_label') }}</label>
                <input id="email" type="email" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="help-block has-error">
                        {{ $errors->first('email') }}
                    </span>
                @endif
            </div>

            <div class="form-item">
                <label for="password">{{ __('auth.password_label') }}</label>
                <input id="password" type="password" name="password" required>
                <span class="help-block">{{ __('auth.password_info') }}</span>
                @if ($errors->has('password'))
                    <span class="help-block has-error">
                        {{ $errors->first('password') }}
                    </span>
                @endif
            </div>

            <div class="form-item">
                <button type="submit" class="btn">{{ __('auth.register_btn') }}</button>
            </div>    
        </form>    
    </div>    
@endsection