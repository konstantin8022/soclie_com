@extends('layouts.app')

@section('content')
    <div class="auth_user container">
        <h1>{{ __('auth.login_title') }}</h1>

        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-item">
                <label for="login">{{ __('auth.login_label') }}</label>
                <input id="login" type="text" name="i_login" value="{{ old('i_login') }}" required autofocus>
                <span class="help-block">{{ __('auth.login_info') }}</span>
                @if ($errors->has('i_login'))
                    <span class="help-block has-error">
                        {{ $errors->first('i_login') }}
                    </span>
                @endif
            </div>

            <div class="form-item">
                <label for="password">{{ __('auth.password_label') }}</label>
                <input id="password" type="password" name="password" required>
                <span class="help-block">{{ __('auth.password_info') }}</span>
                @if ($errors->has('password'))
                    <span class="help-block has-error">
                        {{ $errors->first('password') }}
                    </span>
                @endif
            </div>

            <div class="form-item">
                <label class="remember">
                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('auth.remember_me') }}
                </label>    
            </div>    
    
            <div class="form-item">
                <button type="submit" class="btn">{{ __('auth.login_btn') }}</button>
            </div>
        </form>

        <div class="auth_user restore-link">
            <a href="{{ route('password.request') }}">
                {{ __('auth.password_restore_link') }}
            </a>
        </div>
    </div>    
@endsection