@extends('layouts.app')

@section('content')
    <div class="auth_user container">
        <h1>{{ __('auth.resetpassword_title') }}</h1>

        @if (session('status'))
            <div class="info-block success">
                {{ session('status') }}
            </div>    
        @endif

        <form method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}

            <div class="form-item">
                <label for="email_or_login">{{ __('auth.resetpassword_label') }}</label>
                <input id="email_or_login" type="text" name="email_or_login" value="{{ old('email_or_login') }}" required>
                @if ($errors->has('email_or_login'))
                    <span class="help-block has-error">
                        {{ $errors->first('email_or_login') }}
                    </span>
                @endif
            </div>

            <div class="form-item">
                <button type="submit" class="btn">{{ __('auth.sendresetlink_btn') }}</button>
            </div>    
        </form>    
    </div>    
@endsection