@extends('layouts.app')

@section('content')
    <div class="auth_user container">
        <h1>{{ __('auth.resetpassword_title') }}</h1>

        <form method="POST" action="{{ route('password.request') }}">
            {{ csrf_field() }}

            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-item">
                <label for="email_or_login">{{ __('auth.resetpassword_label') }}</label>
                <input id="email_or_login" type="text" name="email_or_login" value="{{ old('email_or_login') }}" required autofocus>
                @if ($errors->has('email_or_login'))
                    <span class="help-block has-error">
                        {{ $errors->first('email_or_login') }}
                    </span>
                @endif
            </div>

            <div class="form-item">
                <label for="password">{{ __('auth.password_label') }}</label>
                <input id="password" type="password" name="password" required>
                <span class="help-block">{{ __('auth.password_info') }}</span>
                @if ($errors->has('password'))
                    <span class="help-block has-error">
                        {{ $errors->first('password') }}
                    </span>
                @endif
            </div>

            <div class="form-item">
                <button type="submit" class="btn">{{ __('auth.resetpassword_btn') }}</button>
            </div>    
        </form>    
    </div>    
@endsection