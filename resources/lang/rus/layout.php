<?php

return [

    'instagram_authorize' => 'Авторизоваться через Instagram',
    'search_placeholder' => 'Поиск по имени или тегу',
    'rights' => 'Все права защищены',
    'write_to_us' => 'Написать нам',

    'like.set' => 'Лайк поставлен',
    'like.unset' => 'Лайк убран',

    'dislike.set' => 'Дизлайк поставлен',
    'dislike.unset' => 'Дизлайк убран',

    'dislikepost.set' => 'Пост отключен',
    'dislikepost.unset' => 'Пост включен',
    'need_auth_for_action' => 'Вы должны быть авторизованы для совершения действия',

    'favorites.set' => 'Пост добавлен из избранные',
    'favorites.unset' => 'Пост убран из избранных',

    'many_images' => 'Выбрано много изображений',
    'large_image' => 'Слишком большой файл',

    'post_description_empty' => 'Пользователь не добавил описание',
    'post_comments_empty' => 'Комментариев нет',

    'sync_status_is-private' => 'Ваш профиль скрыт, пожалуйста откройте его или добавьте нас в друзья',
    'sync_status_in-progress' => 'Выполняется синхронизация с инстаграм, пожалуйста подождите...',
    'sync_status_done' => 'Прошло :date успешной синхронизации',
    'sync_status_error' => 'Произошла ошибка при выполнении синхронизации, свяжитесь по эл. почте help@soclie.com',
    
];
