<?php

return [

    'instagram' => 'Instagram',
    'additional_photo' => 'Правда',
    'modal_additional_photo' => 'Наше фото',
    'modal_instagram_photo' => 'Instagram фото',
    'comments' => 'Почему это ложь?',
    'read_more' => 'Читать далее',
    'show_more' => 'Показать еще ' . \App\Models\Post::POSTS_PER_PAGE,

    'comments' => 'Почему это ложь?',

    'sort_by' => [
        'only_posts' => 'Показать только изменённые',
        'instagram' => 'По дате в Инстаграме',
        'im_likes' => 'По количеству лайков в Инстаграме',
        'rating' => 'По рейтингу',
        'favorites' => 'По добавлениям в избранное',
        'by_comments' => 'По количеству комментариев',
    ],
    'time' => [
        'week' => 'Неделя',
        'month' => 'Месяц',
        'all' => 'Все время',
    ],

    'posts_not_found' => 'Постов пока нет. Попробуйте изменить фильтрацию',
    'no_favorites' => 'Любимых остов пока нет. Добавьте пост, нажав на сердечко!',

    'post_description' => 'Описание',

    'comments_empty' => 'Комментариев пока нет',
    'comment_textarea_placeholder' => 'Комментарий...',
    'comment_answer' => 'ОТВЕТИТЬ',
    'comment_new_comment' => 'Новый комментарий',
    'comment_need_authorize' => 'Для написания комментариев нужно авторизоваться',
    'comment_send' => 'ОТПРАВИТЬ',
    'comment_cancel' => 'ОТМЕНИТЬ',
    'comment_hours_ago' => 'часа назад',

    'no_add' => 'Нет дополнительных фото',
    'no_comments' => 'Нет комментариев',
    'no_tags' => 'Теги не добавлены',

];
