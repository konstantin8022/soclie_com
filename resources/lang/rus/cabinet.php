<?php

return [

    'message_welcome' => 'Добро пожаловать! Синхронизируем данные из Instagram...',
    'message_sync' => 'С возвращением! Обновляем данные...',

    'exit' => 'Выйти',
    'favorites' => 'Мои избранные',
    'follows' => 'Мои подписки',

    'follows_empty' => 'Подписок нет',
    
    'instagram' => 'Instagram',
    'our_instagram' => 'Instagram наш',

    'comments' => 'Почему это ложь?',
    'write_comment_top' => 'У вас есть подробности чтобы развенчать миф или история снимка',

    'instagram_media' => 'публикаций',
    'followers_count' => 'подписчиков',
    'follows_count' => 'подписки',

    'rating' => 'рейтинг',
    'posts' => 'фотоальбома',
    'favorites_count' => 'в избранном',

    'follow_on_instagram' => 'подписаться в instagram',
    'follow_on_site' => 'подписаться у нас',
    'unfollow_on_site' => 'отписаться у нас',

    'max_photo' => 'Прикрепить до :count фото', //todo сделаьть нормально __('cabinet.max_photo', Photo::MAX_PHOTO)
    'drag_and_drop' => 'Перетащите фото сюда',
    'add_photo' => 'Добавить фото',

    'about' => 'О себе',		
    'comment' => 'Комментарий',
    'write_comment' => 'Напишите ваш комментарий',
    'comment_placeholder' => 'текст комментария',

    'add_tag' => 'добавить новый тег',

    'tag_added' => 'Тег добавлен',
    'tag_removed' => 'Тег удален',

    'your_answers' => 'Ответ вам',
    'empty_answers' => 'Новых ответов нет',

    'go_to_post' => 'перейти на страницу поста к флагу отзыв',
    'mark_as_viewed' => 'Отметить как прочитан',

    'youtube_link' => 'YouTube-ссылка',
    'paste_link' => 'вставьте ссылку',
    'upload_video' => 'ЗАГРУЗИТЬ ВИДЕО',
    'upload_image' => 'загрузить картинку',
    'choose_file' => 'выбрать файл',
    'upload' => 'ЗАГРУЗИТЬ',

    'send' => 'ОТПРАВИТЬ',

    'big_file_text' => '- большой размер файла. максимум - 8 Мб',

    'latest_widget_header' => 'Последние обновленные фото с инстаграм',

    'private_profile_header' => 'Как принять предложение',
    'private_profile_check' => '3. Проверить еще раз',

];
