<?php

return [

    'message_welcome' => 'Welcome! Synchronize data from Instagram...',
    'message_sync' => 'Updating data...',

    'exit' => 'Logout',
    'favorites' => 'My favorites',
    'follows' => 'Follows',

    'follows_empty' => 'Follows empty',

    'instagram' => 'Instagram',
    'our_instagram' => 'Our Instagram',

    'comments' => 'Comment',
    'write_comment_top' => 'Write your comment',

    'instagram_media' => 'publications',
    'followers_count' => 'followers',
    'follows_count' => 'follows',

    'rating' => 'rating',
    'posts' => 'posts',
    'favorites_count' => 'favorites',

    'follow_on_instagram' => 'follow user on instagram',
    'follow_on_site' => 'follow user on site',
    'unfollow_on_site' => 'unfollow user on site',

    'max_photo' => 'Maxx :count photo', //todo сделаьть нормально __('cabinet.max_photo', Photo::MAX_PHOTO)
    'drag_and_drop' => 'Drag and Drop',
    'add_photo' => 'Add photo',

    'comment' => 'Comment',
    'write_comment' => 'Write your comment',
    'comment_placeholder' => 'comment text',

    'add_tag' => 'add tag',

    'tag_added' => 'Tag added',
    'tag_removed' => 'Tag removed',

    'your_answers' => 'Your\'s answer',
    'empty_answers' => 'No new answers',

    'go_to_post' => 'Go to post, on the comment',
    'mark_as_viewed' => 'Mark as viewed',

    'youtube_link' => 'YouTube link',
    'paste_link' => 'past the link',
    'upload_video' => 'UPLOAD VIDEO',
    'upload_image' => 'upload image',
    'choose_file' => 'choose file',
    'upload' => 'UPLOAD',

    'send' => 'SEND',

    'big_file_text' => '- big file size. max - 8 Мб',

    'latest_widget_header' => 'Latest updated photos from instagram',

    'private_profile_header' => 'How to approve follow request',
    'private_profile_check' => '3. Check again',

];
