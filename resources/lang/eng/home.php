<?php

return [

    'instagram' => 'Instagram',
    'additional_photo' => 'Additional photos',
    'modal_additional_photo' => 'Truth',
    'modal_instagram_photo' => 'Instagram photo',
    'comments' => 'Comment',
    'read_more' => 'Read more',
    'show_more' => 'Show more ' . \App\Models\Post::POSTS_PER_PAGE,

    'comments' => 'Why is it lie?',

    'sort_by' => [
        'only_posts' => 'Show only changed',
        'instagram' => 'By Instagram date',
        'im_likes' => 'By Instagram likes',
        'rating' => 'By rating',
        'favorites' => 'Add to favorites',
        'by_comments' => 'By comments',
    ],
    'time' => [
        'week' => 'Week',
        'month' => 'Month',
        'all' => 'All time',
    ],

    'posts_not_found' => 'Posts not add yet. Try to change filter',
    'no_favorites' => 'Favorites posts not add yet',

    'post_description' => 'Description',

    'comments_empty' => 'No comments yet',
    'comment_textarea_placeholder' => 'Comment...',
    'comment_answer' => 'ANSWER',
    'comment_new_comment' => 'New comment',
    'comment_need_authorize' => 'To write a comments you need to login',
    'comment_send' => 'SEND',
    'comment_cancel' => 'CANCEL',
    'comment_hours_ago' => 'hours ago',

    'no_add' => 'No additional photos',
    'no_comments' => 'No comments yet',
    'no_tags' => 'No tags added',
];
