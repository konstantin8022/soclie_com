<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',

    'register_btn' => 'Register',
    'login_btn' => 'Login',
    'sendresetlink_btn' => 'Send password reset link',
    'resetpassword_btn' => 'Reset password',
    'remember_me' => 'Remember me',

    'register_title' => 'Create account',
    'login_title' => 'Login to your account',
    'resetpassword_title' => 'Reset password',
    'resetpassword_label' => 'Email or login',
    'login_label' => 'Login',
    'login_info' => 'Use same name as you login to instagram',
    'email_label' => 'Email',
    'password_label' => 'Password',
    'password_info' => 'Do not use your password from instagram',
    'password_restore_link' => 'Restore your password',

];
