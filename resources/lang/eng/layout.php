<?php

return [

    'instagram_authorize' => 'Authorize via Instagram',
    'search_placeholder' => 'Search by name or tag',
    'rights' => 'All rights reserved ',
    'write_to_us' => 'Write to us',

    'like.set' => 'Like set',
    'like.unset' => 'Like unset',

    'dislike.set' => 'Dislike set',
    'dislike.unset' => 'Dislike unset',

    'need_auth_for_action' => 'You must be authorized to perform an action',

    'favorites.set' => 'Add to favorites',
    'favorites.unset' => 'Remove from favorites',

    'many_images' => 'Many images selected',
    'large_image' => 'File is too big',

    'post_description_empty' => 'User has not added a description',
    'post_comments_empty' => 'No comments',

    'sync_status_is-private' => 'Account is private, please open your account or add our account to friends',
    'sync_status_in-progress' => 'Synchronizing posts from instagram in progress, please wait...',
    'sync_status_done' => ':date last successful synchronizing',
    'sync_status_error' => 'An error occurred while synchronizing, get support at help@soclie.com',
    
];
