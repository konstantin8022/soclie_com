@extends('layouts.app')

@section('var_title', 'Каталог')
@section('var_bodyClass', 'my_catalog')

@section('content')

    @include('cabinet.partials.private-profile-info')

    @include('cabinet.partials.last-synced-items', ['lastPosts' => $lastPosts])

    @include('layouts.partials.filters', ['withOnlyPosts'=>1])

    @if(count($posts) > 0)
        <div class="post_title">
            <h1 class="post_title_item instagram_photo">{{ __('home.instagram') }}</h1>
            <h2 class="post_title_item add_photos">{{ __('home.additional_photo') }} <span>{{ __('cabinet.max_photo', ['count'=>\App\Models\Post::MAX_PHOTOS]) }}</span></h2>
            <h2 class="post_title_item comment">{{ __('cabinet.comments') }} <span>{{ __('cabinet.write_comment_top') }}</span></h2>
        </div>
        @foreach($posts as $post)
            <div class="post">
                <div class="post_item_block instagram_photo">
                    <h2 class="post_title_item_mob">{{ __('cabinet.instagram') }}</h2>
                    @include('layouts.partials.icons', ['post'=>$post])
                    <div class="photo">
                        <div class="photo_wrap">
                            <p>fejkhrtrejhjerty</p>
                            @foreach($post->instagramPhotos()->get() as $photo)
                                @if($photo->isInstagramImage())
{{--                                    <img {{ $side['isEqual'] ? '' : $side['biggestSide'] . '=' . '' . $side['number'] . '' }} src="{{ $photo->getImageSrc() }}" alt="inst_img">--}}
                                    <img src="{{ $photo->getImageSrc() }}" alt="inst_img">
                                @endif
                                @if($photo->isInstagramCarousel())
                                    <div class="owl-carousel owl-theme instagram_slider">
                                        @foreach($photo->getInstagramCarouselSrcs() as $src)
                                            <div class="slider-item">
                                                {{--<img {{ $side['isEqual'] ? '' : $side['biggestSide'] . '=' . '' . $side['number'] . '' }} src="{{ $src }}" alt="inst_img">--}}
                                                <img src="{{ $src }}" alt="inst_img">
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="instagram_album_ico">
                                        <i class="fa fa-clone" aria-hidden="true"></i>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        
                    </div>
                </div>

             
                


                <div class="add_photos post_item_block">
                    <h2 class="post_title_item_mob">{{ __('home.additional_photo') }} <span>{{ __('cabinet.max_photo', ['count'=>\App\Models\Post::MAX_PHOTOS]) }}</span></h2>
                    <form id="dropzone{{ $post->id }}" action="{{ route('cabinet.dropzoneStore') }}" method="post" class="drop_zone {{ $post->imagePhotos()->count() ? '' : 'empty' }}" enctype="multipart/form-data">
                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                        {{ csrf_field() }}
                        @forelse($post->imagePhotos()->get() as $photo)
                            <div class="add_img_wrap dragbox">
                                <img class="dragbox-content" src="{{ $photo->getThumbnailSrc() }}" alt="inst_img" data-id="{{ $photo->id }}">                               <span class="del_photos"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                        @empty
                        @endforelse
                        <div class="dz-default dz-message drop_zone_message add_img_wrap">
                            <div class="drop_zone_ico">+++</div>
                            <span class="drop_desk_text">{{ __('cabinet.drag_and_drop') }}</span>
                            <span class="drop_mob_text">{{ __('cabinet.add_photo') }}</span>
                        </div>
                        <div class="loader hidden"><div class="spiner"></div></div>
                        <div class="btn-group" data-toggle="buttons" >
                                
                     </div>


                    </form>
                    <div class="error hidden">
                        <span></span> {{ __('cabinet.big_file_text') }}
                    </div>
                </div>

                <div class="comment  post_item_block">
                        <h2 class="post_title_item_mob">{{ __('cabinet.comment') }} <span>{{ __('cabinet.write_comment') }}</span></h2>
                        <div class="comment_block">
                            {{ csrf_field() }}
                            <input type="hidden" name="post_id" value="{{ $post->id }}">
                            <div class="text_container" contenteditable="true">
                                {!! $post->description->content !!}
                            </div>
                            <button type="button" class="btn btn-primary"><input type="checkbox" value="Postdisact" name="Postdisact">Postdisact</button>
    


                            <div class="comment_action_block">
                                <div class="comment_action_icons hidden">
                                    <i class="flaticon-piggy-bank" aria-hidden="true"></i>
                                    <i class="flaticon-musical-note" aria-hidden="true"></i>
                                    <i class="flaticon-chat" aria-hidden="true"></i>
                                    <i class="flaticon-headphones" aria-hidden="true"></i>
                                </div>
                                <div class="comment_action_btn_block">
                                    <span class="send_comment_btn">отправить</span>
                                    <i class="flaticon-smile smiles_dock_btn" aria-hidden="true"></i>
                                    <i class="flaticon-photo-camera upload_photo_btn" aria-hidden="true"></i>
                                </div>
                                <div class="comment_modal hidden">
                                    <span>{{ __('cabinet.youtube_link') }}</span>
                                    <input type="text" name="youtube_link" placeholder="{{ __('cabinet.paste_link') }}">
                                    <button class="upload_btn upload_video_js">{{ __('cabinet.upload_video') }}</button>
                                    <form method="post" class="ajax_form" action="{{ route('cabinet.addPostCommentImage') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="post_id" value="{{ $post->id }}" />
                                        <span>{{ __('cabinet.upload_image') }}</span>
                                        <label class="label_upload" for="upload{{ ($post->id == 1) ? '' : $post->id }}">{{ __('cabinet.choose_file') }}</label>
                                        <input id="upload{{ ($post->id == 1) ? '' : $post->id }}" class="upload_photo_input_js" type="file" accept="image/*" name="imgs[]" multiple hidden>
                                        <input class="upload_btn" type="submit" value="{{ __('cabinet.upload') }}" />
                                    </form>
                                </div>
                            </div>
                        </div>
					<input class="tags_input_js" type="text" value="{{ $post->getTagsStr() }}" data-role="tagsinput" placeholder="{{ __('cabinet.add_tag') }}" />
                   

                </div>
                     
                     
            </div>         

            
        @endforeach
    @else
        <div style="padding-left: 110px; padding-top: 40px">{{ __('home.posts_not_found') }}</div>
    @endif
@endsection

@section('additionalDivs')
    @parent
    @if(!empty($posts) && $posts->currentPage() != $posts->lastPage())
        <button class="btn show_more_btn">{{ __('home.show_more') }}</button>
    @endif
@endsection