<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::any('adminer', '\Miroc\LaravelAdminer\AdminerController@index')->name('adminer');  // admin db mysql 



Route::get('/p/{id}/{segment?}', 'ShowPost')->name('showpost');
Route::get('/showsearch', 'ShowSearch')->name('showsearch');


//Route::any('pas', 'Pas')->name('pas');

Route::group(
    [
        'middleware' => [
            'auth'
        ],
        'namespace' => 'Cabinet',
        'prefix'    => 'cabinet',
        'as' => 'cabinet.'
    ],
    function() {
        Route::get('/', ['as' => 'index', 'uses' => 'Index']);
        //Route::get('/showcatalog', ['as' => 'showcatalog', 'uses' => 'Index']);
        Route::get('/showcatalog/{slug1?}/{slug2?}', ['as' => 'showcatalog', 'uses' => 'ShowCatalog']);
        Route::get('/showcomments', ['as' => 'showcomments', 'uses' => 'ShowComments']);
        Route::get('/showrating', ['as' => 'showrating', 'uses' => 'ShowRating']);
        Route::get('/showfavorites/{slug1?}/{slug2?}', ['as' => 'showFavorites', 'uses' => 'ShowFavorites']);
        Route::get('/show-follows/{slug1?}/{slug2?}', ['as' => 'showFollows', 'uses' => 'ShowFollows']);

        Route::get('/user/follow', ['as' => 'follow', 'uses' => 'User\Follow']);
        Route::get('/user/unfollow', ['as' => 'unfollow', 'uses' => 'User\Unfollow']);
        Route::get('/user/is-private', ['as' => 'isprivate', 'uses' => 'User\IsPrivate']); //main.js
    

        Route::get('/get-last-items', ['as' => 'getLastItems', 'uses' => 'Photo\LastItems']);// main.js

        Route::post('/showcatalog/store', ['as'=>'dropzoneStore', 'uses'=>'Photo\DropzoneStore']);
        Route::post('/showcatalog/destroy', ['as'=>'dropzoneDestroy', 'uses'=>'Photo\DropzoneDestroy']);
        Route::post('/showcatalog/photomove', ['as'=>'dropzonePhotoMove', 'uses'=>'Photo\DropzonePhotoMove']);

        Route::post('/post/addposttofavorites', ['as'=>'addPostToFavorites', 'uses'=>'Post\AddPostToFavorites']);

        Route::post('/post/postlikeset', ['as'=>'postLikeSet', 'uses'=>'Post\PostLikeSet']);


        Route::post('/post/postdislikeset', ['as'=>'postDislikeSet', 'uses'=>'Post\PostDislikeSet']);


        Route::post('/post/postdisactive', ['as'=>'postDisActive', 'uses'=>'Post\PostDisActive']);


        Route::post('/comment/commentlikeset', ['as'=>'commentLikeSet', 'uses'=>'Comment\CommentLikeSet']);
        Route::post('/comment/commentdislikeset', ['as'=>'commentDislikeSet', 'uses'=>'Comment\CommentDislikeSet']);

        Route::post('/comment/update-comment', ['as'=>'updateComment', 'uses'=>'Comment\UpdateComment']);
        Route::post('/comment/add-comment-image', ['as'=>'addCommentImage', 'uses'=>'Comment\AddCommentImage']);

        Route::post('/comment/mark-as-viewed', ['as'=>'markAsViewed', 'uses'=>'Comment\MarkAsViewed']);

        Route::post('/post/updatepostcomment', ['as'=>'updatePostComment', 'uses'=>'Post\UpdatePostComment']);
        Route::post('/post/addcommentimage', ['as'=>'addPostCommentImage', 'uses'=>'Post\AddCommentImage']);
    }
);


//todo повесить middleware "ни для кого" - 404?
Route::resource('photos', 'Cabinet\Photo\Resource');

Route::delete('/tags', 'Cabinet\Tag\Resource@destroy');
Route::resource('tags', 'Cabinet\Tag\Resource');

Route::resource('comments', 'Cabinet\Comment\Resource');


// Route::get('/login-as/{login}', function ($login)
// {
//     switch ($login)
//     {
//         case 'lolik_89':
//             session(['clientId' => '64d03de3ea344a31b7fa496d0c9b6df2']);
//             session(['clientSecret' => '884be34ffbd54c308f38193ee757ee9f']);
//             break;
//         case 'artem.stepochkin':
//             session(['clientId' => 'b00a737d5e24463a9bdf425a5c452125']);
//             session(['clientSecret' => 'a5be9c22f25b445ab66a7273033e0008']);
//             break;
//         case 'v.voloshyn96':
//             session(['clientId' => '768aa50b31154e99b58bd3c2e002a0f5']);
//             session(['clientSecret' => '5b83c79bcd04476e87bc7f73648a6f60']);
//             break;
//         default: break;
//     }
//     return redirect()->back();
//     //return redirect()->route('socialAuth', 'instagram');
// });


Auth::routes();    //all  !!!!!





//OAuth routes
Route::get('auth/{driver}', ['as' => 'socialAuth', 'uses' => 'Auth\Social@redirectToProvider']);
Route::get('auth/{driver}/callback', ['as' => 'socialAuthCallback', 'uses' => 'Auth\Social@handleProviderCallback']);

Route::get('/setlocale/{locale}', function ($locale) {
    $locale = (\Illuminate\Support\Facades\File::isDirectory('../resources/lang/'.$locale)) ? $locale : App::getLocale();
    session(['lang' => $locale]);
    return redirect()->back();
});

Route::get('/{slug1?}/{slug2?}/{slug3?}/{slug4?}', 'Index')->name('index');  // SearchController

//Route::get('/REMOVE_THIS_ROUTE', 'ShowProfile')->name('showprofile');