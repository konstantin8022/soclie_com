
    <header>
        <div class="header container">
            <a class="logo" href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('img/logo.png')); ?>" alt="logo"></a>

            <?php echo $__env->make('layouts.partials.search', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

            <div class="menu">
                <i class="fa fa-bars menu_mob_btn" aria-hidden="true"></i>
                <div class="nav_menu">
                    <div class="select_wrap lang">
                        <span class="select_title">GG<?php echo e(app()->getLocale()); ?></span>
                        <div class="select">
                            <ul>
                                <li><a href="<?php echo e(url('/setlocale/eng')); ?>">Eng</a></li>
                                <li><a href="<?php echo e(url('/setlocale/rus')); ?>">Rus</a></li>
                            </ul>
                        </div>
                    </div>

                    <?php if(Auth::check()): ?>
                    <div class="menu_icons">
                        <a class="<?php echo e(route('cabinet.showrating') == request()->url() ? 'be_active' : ''); ?>" href="<?php echo e(route('cabinet.showrating')); ?>">
                            <i class="flaticon-star" aria-hidden="true"></i>
                            <div class="rate_count"><?php echo e(Auth::user()->rating); ?></div>
                        </a>
                        <a class="<?php echo e(url('/') == request()->url() ? 'be_active' : ''); ?>" href="<?php echo e(url('/')); ?>"><i class="flaticon-house" aria-hidden="true"></i></a>
                        <a class="<?php echo e(route('cabinet.showFavorites') == request()->url() ? 'be_active' : ''); ?>" href="<?php echo e(route('cabinet.showFavorites')); ?>"><i class="flaticon-heart" aria-hidden="true"></i></a>
                        <a class="<?php echo e(route('cabinet.showcomments') == request()->url() ? 'be_active' : ''); ?>" href="<?php echo e(route('cabinet.showcomments')); ?>">
                            <i class="flaticon-edit" aria-hidden="true"></i>
                            <?php if($commentsCount = \App\Models\Comment::getNewCommentsForUser(Auth::user())->count()): ?>
                                <div class="bage"><?php echo e($commentsCount); ?></div>
                            <?php endif; ?>
                        </a>
                        <a class="<?php echo e(route('cabinet.showcatalog') == request()->url() ? 'be_active' : ''); ?>" href="<?php echo e(route('cabinet.showcatalog')); ?>"><i class="flaticon-picture-with-frame" aria-hidden="true"></i></a>
                        <a class="<?php echo e(route('cabinet.index') == request()->url() ? 'be_active' : ''); ?>" href="<?php echo e(route('cabinet.index')); ?>"><i class="flaticon-avatar" aria-hidden="true"></i></a>
                    </div>
                    <?php else: ?>
                        <a  href="<?php echo e(route('register')); ?>">
                            <button href="<?php echo e(route('register')); ?>" class="btn_for_auth"><?php echo e(__('auth.register_btn')); ?></button>
                        </a>
                        <a  href="<?php echo e(route('login')); ?>">
                            <button href="<?php echo e(route('login')); ?>" class="btn_for_auth"><?php echo e(__('auth.login_btn')); ?></button>
                        </a>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </header><?php /**PATH /home/test/public_html/resources/views/layouts/partials/header.blade.php ENDPATH**/ ?>