
    <div class="user">
        <div class="user_pick">
            <img src="<?php echo e($user->getAvatarSrc()); ?>" alt="user_pick">
        </div>
        <div class="user_info">
            <h1 class="user_info_item user_name"><span><?php echo e($user->i_login); ?></span><span><?php echo e($user->i_real_name); ?></span></h1>
            <div class="user_info_item">
                <div class="user_info_descr_wrap">
                    <div class="user_info_title">
                        <span><?php echo e(__('cabinet.instagram')); ?></span>
                    </div>
                    <div class="user_info_descr">
                        <div><span><?php echo e($user->i_publications); ?></span> <?php echo e(__('cabinet.instagram_media')); ?></div>
                        <div><span><?php echo e($user->i_count_subscribers); ?></span> <?php echo e(__('cabinet.followers_count')); ?></div>
                        <div><span><?php echo e($user->i_count_subscribes); ?></span> <?php echo e(__('cabinet.follows_count')); ?></div>
                    </div>
                </div>
                <?php if(Auth::check() && Auth::user()->id != $user->id): ?>
                <button class="btn snack_js"><?php echo e(__('cabinet.follow_on_instagram')); ?></button>
                <?php endif; ?>
            </div>
            <div class="user_info_item">
                <div class="user_info_descr_wrap">
                    <div class="user_info_title">
                        <span><?php echo e(__('cabinet.our_instagram')); ?></span>
                    </div>
                    <div class="user_info_descr">
                        <div><span><i class="fa fa-star-o" aria-hidden="true"></i> <?php echo e($user->rating); ?></span> <?php echo e(__('cabinet.rating')); ?></div>
                        <div><span><?php echo e($user->getAllOnlyPosts()->count()); ?></span> <?php echo e(__('cabinet.posts')); ?></div>
                        <div><span><?php echo e($user->followingUsers()->count()); ?></span> <?php echo e(__('cabinet.followers_count')); ?></div>
                        <div><span><?php echo e($user->getUserFavoritesCount()); ?></span> <?php echo e(__('cabinet.favorites_count')); ?></div>
                    </div>
                </div>

                <?php if(Auth::check() && Auth::user()->id != $user->id): ?>
                    <?php if(!Auth::user()->isFollower($user)): ?>
                        <a href="<?php echo e(route('cabinet.follow', ['id'=>$user->id])); ?>" class="btn snack_js"><?php echo e(__('cabinet.follow_on_site')); ?></a>
                    <?php else: ?>
                        <a href="<?php echo e(route('cabinet.unfollow', ['id'=>$user->id])); ?>" class="btn snack_js"><?php echo e(__('cabinet.unfollow_on_site')); ?></a>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php /**PATH /home/test/public_html/resources/views/layouts/partials/user.blade.php ENDPATH**/ ?>