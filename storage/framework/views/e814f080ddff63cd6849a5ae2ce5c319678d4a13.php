<section class="sync_status <?php echo e(Auth::user()->getSyncStatus()); ?>">
    <?php switch(Auth::user()->getSyncStatus()):
        case ('is-private'): ?>
            <?php echo e(__('layout.sync_status_is-private')); ?>

        <?php break; ?>;

        <?php case ('in-progress'): ?>
            <?php echo e(__('layout.sync_status_in-progress')); ?>

        <?php break; ?>;

        <?php case ('done'): ?>
            <?php echo e(__('layout.sync_status_done', ['date' => Auth::user()->getSyncedAt()])); ?>

        <?php break; ?>;

        <?php case ('error'): ?>
            <?php echo e(__('layout.sync_status_error')); ?>

        <?php break; ?>;
    <?php endswitch; ?>    
</section>    
