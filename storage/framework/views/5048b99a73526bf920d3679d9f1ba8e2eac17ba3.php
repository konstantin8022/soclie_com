<div class="last-synced-items <?php echo e(count($lastPosts) == 0 ? 'in-progress' : ''); ?>">
    <h2><?php echo e(__('cabinet.latest_widget_header')); ?></h2>

    <div class="posts owl-carousel owl-theme">
        <?php if(count($lastPosts)): ?>
            <?php $__currentLoopData = $lastPosts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lastPost): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $__currentLoopData = $lastPost->photos(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lastPhoto): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <a href="<?php echo e(route('showpost', $lastPost->getLinkPart())); ?>">
                        <div class="item">
                        <img src="<?php echo e($lastPhoto->getThumbnailSrc()); ?>" alt="inst_img">
                        <span class="date"><?php echo e(\Carbon\Carbon::parse($lastPhoto->updated_at)->diffForHumans(null, false, true)); ?></span>
                        </div>
                    </a>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </div>
</div>