    <div class="profile_menu">

        <div>
            <a href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="flaticon-logout" aria-hidden="true"></i><?php echo e(__('cabinet.exit')); ?>

            </a>
        </div>
        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
            <?php echo e(csrf_field()); ?>

        </form>

        <div>
            <a href="<?php echo e(route('cabinet.showFavorites')); ?>"><i class="flaticon-heart" aria-hidden="true"></i><?php echo e(__('cabinet.favorites')); ?>

            </a>
        </div>
        <div>
            <a style="position: relative" href="<?php echo e(route('cabinet.showFollows')); ?>"><i class="flaticon-commerce" aria-hidden="true"></i><?php echo e(__('cabinet.follows')); ?>

                <?php if($count = Auth::user()->followedUsers()->count()): ?>
                    <div class="be_bage"><?php echo e($count); ?></div>
                <?php endif; ?>
            </a>
        </div>
        
    </div>
<?php /**PATH /home/test/public_html/resources/views/cabinet/partials/profilemenu.blade.php ENDPATH**/ ?>