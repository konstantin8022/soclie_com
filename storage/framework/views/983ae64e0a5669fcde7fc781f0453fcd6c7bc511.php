<?php $__env->startSection('var_title', 'Комментарии76543'); ?>
<?php $__env->startSection('var_bodyClass', 'comments'); ?>

<?php $__env->startSection('content'); ?>
        <h3><?php echo e(__('cabinet.your_answers')); ?></h3>
 
	<p>cabinet show comments</>
       <?php $__empty_1 = true; $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <div class="post_comment">
                <!-- <h3><?php echo $comment->post->description->content; ?></h3> -->
                <p><?php echo $comment->post->description->getSmallDescription(50); ?></p>

                <div class="post_comment_hader">
                    <span class="post_comment_rate <?php echo e($comment->rating > 0  ? 'positive' : ($comment->rating != 0 ? 'negative' : '')); ?>"><?php echo e($comment->rating); ?></span>
                    <span>
                            <i class="fa fa-arrow-down dislike_btn <?php echo e($comment->isUserDislikeFound() ? 'active' : ''); ?>" aria-hidden="true"></i>
                            <i class="fa fa-arrow-up like_btn <?php echo e($comment->isUserLikeFound() ? 'active' : ''); ?>" aria-hidden="true"></i>
                        </span>
                    <img src="<?php echo e($comment->user->getAvatarSrc()); ?>" alt="user_pick">
                    <span class="post_comment_autor"><a href="<?php echo e(route('index', $comment->user->i_login)); ?>"><?php echo e($comment->user->i_login); ?></a></span>
                    <span><?php echo e($comment->user->rating); ?></span>
                    <span><?php echo e($comment->getFormattedTimeAgoString()); ?></span>
                </div>

                <p><?php echo $comment->content; ?></p>

                <a class="comment_btn reply_btn" href="<?php echo e(route('showpost', [$comment->post->getLinkPart(), $comment->id])); ?>#comment<?php echo e($comment->id); ?>"><?php echo e(__('cabinet.go_to_post')); ?></a>
                <a class="comment_btn read_btn snack_js" href="<?php echo e(route('cabinet.markAsViewed')); ?>"
                   onclick="event.preventDefault();
                    document.getElementById('_markAsViewed<?php echo e($comment->id); ?>').submit();">
                    <?php echo e(__('cabinet.mark_as_viewed')); ?>

                </a>
                <form id="_markAsViewed<?php echo e($comment->id); ?>" action="<?php echo e(route('cabinet.markAsViewed')); ?>" method="POST" style="display: none;">
                    <input type="hidden" name="comment_id" value="<?php echo e($comment->id); ?>">
                    <?php echo e(csrf_field()); ?>

                </form>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <?php echo e(__('cabinet.empty_answers')); ?>

        <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('additionalDivs'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/test/public_html/resources/views/cabinet/showcomments.blade.php ENDPATH**/ ?>