
    <div class="icons">
        <input class="post_id" type="text" hidden value="<?php echo e($post->id); ?>">
        <div class="icon_wrap like"><i class="flaticon-like icon_js <?php echo e($post->isUserLikeFound() ? 'active' : ''); ?>" data-value="like" aria-hidden="true"></i><span><?php echo e($post->likes); ?></span></div>
        <div class="icon_wrap dislike"><i class="flaticon-dislike icon_js <?php echo e($post->isUserDislikeFound() ? 'active' : ''); ?>" data-value="dislike" aria-hidden="true"></i><span><?php echo e($post->dislikes); ?></span></div>

        <div class="icon_wrap favor"><i class="flaticon-heart icon_js <?php if(Auth::check()): ?> <?php echo e(Auth::user()->isUserFavoritesFound($post) ? 'active' : ''); ?> <?php endif; ?>" data-value="favor" aria-hidden="true"></i><span><?php echo e($post->favorites_count); ?></span></div>


<!-- <div class="icon_wrap is_empty1"><i class="flaticon-piggy-bank icon_js <?php echo e($post->isPostDislikeFound() ?  'active' : ''); ?>   " data-value="is_empty" aria-hidden="true"></i><span><?php echo e($post->is_empty); ?></span></div> 
-->   
     <div class="icon_wrap"><a href="<?php echo e(route('showpost', $post->getLinkPart())); ?>"><i class="flaticon-edit" aria-hidden="true"></i></a><span><?php echo e($post->getCommentsCount()); ?></span></div>
        <div class="icon_wrap rating"><i class="flaticon-star" data-value="rating" aria-hidden="true"></i><span><?php echo e($post->rating); ?></span></div>
 

   </div>
