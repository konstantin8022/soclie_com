<?php $__env->startSection('content'); ?>
    <div class="auth_user container">
        <h1><?php echo e(__('auth.register_title')); ?></h1>

        <form method="POST" action="<?php echo e(route('register')); ?>">
            <?php echo e(csrf_field()); ?>


            <div class="form-item">
                <label for="login"><?php echo e(__('auth.login_label')); ?></label>
                <input id="login" type="text" name="i_login" value="<?php echo e(old('i_login')); ?>" required autofocus>
                <span class="help-block"><?php echo e(__('auth.login_info')); ?></span>
                <?php if($errors->has('i_login')): ?>
                    <span class="help-block has-error">
                        <?php echo e($errors->first('i_login')); ?>

                    </span>
                <?php endif; ?>
            </div>

            <div class="form-item">
                <label for="email"><?php echo e(__('auth.email_label')); ?></label>
                <input id="email" type="email" name="email" value="<?php echo e(old('email')); ?>" required>
                <?php if($errors->has('email')): ?>
                    <span class="help-block has-error">
                        <?php echo e($errors->first('email')); ?>

                    </span>
                <?php endif; ?>
            </div>

            <div class="form-item">
                <label for="password"><?php echo e(__('auth.password_label')); ?></label>
                <input id="password" type="password" name="password" required>
                <span class="help-block"><?php echo e(__('auth.password_info')); ?></span>
                <?php if($errors->has('password')): ?>
                    <span class="help-block has-error">
                        <?php echo e($errors->first('password')); ?>

                    </span>
                <?php endif; ?>
            </div>

            <div class="form-item">
                <button type="submit" class="btn"><?php echo e(__('auth.register_btn')); ?></button>
            </div>    
        </form>    
    </div>    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/test/public_html/resources/views/auth/register.blade.php ENDPATH**/ ?>