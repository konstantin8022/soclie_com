<div class="slide_text">
    <p>hello world55555</p>
    <div class="slide_comment">
        <?php if(!empty($post->description->content)): ?>
        <a href="<?php echo e(route('index', $post->user->i_login)); ?>"><span class="slide_username"><?php echo e($post->user->i_login); ?></span></a>
        <?php echo $post->description->content; ?>

        <?php else: ?>
        <p><?php echo e(__('layout.post_description_empty')); ?></p>
        <?php endif; ?>
        <?php $__currentLoopData = $post->tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <a href="<?php echo e(route('index', ['tag', $tag->name])); ?>"><span class="hashtag"><?php echo e($tag->name); ?></span></a>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <br /><br />
        <?php $__empty_1 = true; $__currentLoopData = $post->comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
        <a href="<?php echo e(route('index', $comment->user->i_user)); ?>"><span class="slide_username"><?php echo e($comment->user->i_login); ?></span></a>
        <p><?php echo $comment->content; ?></p>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
        <p><?php echo e(__('layout.post_comments_empty')); ?></p>
        <?php endif; ?>


        <i class="fa fa-times delete_comment hidden" aria-hidden="true"></i>
    </div>
</div>
<div class="slide_text_footer">
    <span><?php echo e($post->user->i_login); ?></span>
    <i class="fa fa-heart" aria-hidden="true"></i><span><?php echo e($post->i_likes_count); ?></span>
    <i class="fa fa-comment" aria-hidden="true"></i><span><?php echo e($post->i_comments_count); ?></span>
</div>
<?php /**PATH /home/test/public_html/resources/views/layouts/partials/modal-right.blade.php ENDPATH**/ ?>