    <div class="modal hidden">
        <div class="modal_back"><i class="fa fa-times close_modal" aria-hidden="true"></i></div>
        <div class="modal_content">
            <div class="owl-carousel owl-theme modal_slider">

                    <?php echo $__env->yieldPushContent('modal-item'); ?>

            </div>
        </div>
    </div>