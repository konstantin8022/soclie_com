<?php if(count($tags)): ?>
    <div class="tags">
        <?php $__empty_1 = true; $__currentLoopData = $tags; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tag): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <a href="<?php echo e(route('index', ['tag', $tag->name])); ?>"><?php echo e($tag->name); ?></a>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            
        <?php endif; ?>
    </div>
<?php endif; ?><?php /**PATH /home/test/public_html/resources/views/layouts/partials/tags.blade.php ENDPATH**/ ?>