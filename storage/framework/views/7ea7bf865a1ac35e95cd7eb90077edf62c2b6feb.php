<?php $__env->startSection('var_title', 'Каталог'); ?>
<?php $__env->startSection('var_bodyClass', 'my_catalog'); ?>

<?php $__env->startSection('content'); ?>

    <?php echo $__env->make('cabinet.partials.private-profile-info', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php echo $__env->make('cabinet.partials.last-synced-items', ['lastPosts' => $lastPosts], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php echo $__env->make('layouts.partials.filters', ['withOnlyPosts'=>1], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

    <?php if(count($posts) > 0): ?>
        <div class="post_title">
            <h1 class="post_title_item instagram_photo"><?php echo e(__('home.instagram')); ?></h1>
            <h2 class="post_title_item add_photos"><?php echo e(__('home.additional_photo')); ?> <span><?php echo e(__('cabinet.max_photo', ['count'=>\App\Models\Post::MAX_PHOTOS])); ?></span></h2>
            <h2 class="post_title_item comment"><?php echo e(__('cabinet.comments')); ?> <span><?php echo e(__('cabinet.write_comment_top')); ?></span></h2>
        </div>
        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="post">
                <div class="post_item_block instagram_photo">
                    <h2 class="post_title_item_mob"><?php echo e(__('cabinet.instagram')); ?></h2>
                    <?php echo $__env->make('layouts.partials.icons', ['post'=>$post], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    <div class="photo">
                        <div class="photo_wrap">
                            <?php $__currentLoopData = $post->instagramPhotos()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <li>  <video class=\"media video-js vjs-default-skin\" width=\"250\" height=\"250\" poster=\"{$poster}\"
                           data-setup='{\"controls\":true, \"preload\": \"auto\"}'>
                             <source src=\"{$source}\" type=\"video/mp4\" />
                           </video> </li>





                                <?php if($photo->isInstagramImage()): ?>

                                    <img src="<?php echo e($photo->getImageSrc()); ?>" alt="inst_img">
                                <?php endif; ?>
                                <?php if($photo->isInstagramCarousel()): ?>
                                    <div class="owl-carousel owl-theme instagram_slider">
                                        <?php $__currentLoopData = $photo->getInstagramCarouselSrcs(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $src): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="slider-item">
                                                
                                                <img src="<?php echo e($src); ?>" alt="inst_img">
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>
                                    <div class="instagram_album_ico">
                                        <i class="fa fa-clone" aria-hidden="true"></i>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php echo $__env->make('layouts.partials.photo-info', ['post'=>$post], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </div>
                </div>
                <div class="post_item_block add_photos">
                    <h2 class="post_title_item_mob"><?php echo e(__('home.additional_photo')); ?> <span><?php echo e(__('cabinet.max_photo', ['count'=>\App\Models\Post::MAX_PHOTOS])); ?></span></h2>
                    
			<form id="11dropzone<?php echo e($post->id); ?>" action="<?php echo e(route('cabinet.dropzoneStore')); ?>" method="post" class="drop_zone <?php echo e($post->imagePhotos()->count() ? '' : 'empty'); ?>" enctype="multipart/form-data">
                        <input type="hidden" name="post_id" value="<?php echo e($post->id); ?>">
                        <?php echo e(csrf_field()); ?>

                        <?php $__empty_1 = true; $__currentLoopData = $post->imagePhotos()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                            <div class="add_img_wrap dragbox">
                                <img class="dragbox-content" src="<?php echo e($photo->getThumbnailSrc()); ?>" alt="inst_img" data-id="<?php echo e($photo->id); ?>">                               <span class="del_photos"><i class="fa fa-times" aria-hidden="true"></i></span>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                        <?php endif; ?>
                        <div class="dz-default dz-message drop_zone_message add_img_wrap">
                            <div class="drop_zone_ico">--</div>
                            <span class="drop_desk_text"><?php echo e(__('cabinet.drag_and_drop')); ?></span>
                            <span class="drop_mob_text"><?php echo e(__('cabinet.add_photo')); ?></span>
                        </div>
                        <div class="loader hidden"><div class="spiner"></div></div>
                    </form>

                    <div class="error hidden">
                        <span></span> <?php echo e(__('cabinet.big_file_text')); ?>

                    </div>
                </div>
                <div class="post_item_block comment">
                        <h2 class="post_title_item_mob"><?php echo e(__('cabinet.comment')); ?> <span><?php echo e(__('cabinet.write_comment')); ?></span></h2>
                        <div class="comment_block">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" name="post_id" value="<?php echo e($post->id); ?>">
                            <div class="text_container" contenteditable="true">
                                <?php echo $post->description->content; ?>

                            </div>
                            <div class="comment_action_block">
                                <div class="comment_action_icons hidden">
                                    <i class="flaticon-piggy-bank" aria-hidden="true"></i>
                                    <i class="flaticon-musical-note" aria-hidden="true"></i>
                                    <i class="flaticon-chat" aria-hidden="true"></i>
                                    <i class="flaticon-headphones" aria-hidden="true"></i>
                                </div>
                                <div class="comment_action_btn_block">
                                    <span class="send_comment_btn">отправить</span>
                                    <i class="flaticon-smile smiles_dock_btn" aria-hidden="true"></i>
                                    <i class="flaticon-photo-camera upload_photo_btn" aria-hidden="true"></i>
                                </div>
                                <div class="comment_modal hidden">
                                    <span><?php echo e(__('cabinet.youtube_link')); ?></span>
                                    <input type="text" name="youtube_link" placeholder="<?php echo e(__('cabinet.paste_link')); ?>">
                                    <button class="upload_btn upload_video_js"><?php echo e(__('cabinet.upload_video')); ?></button>
                                    <form method="post" class="ajax_form" action="<?php echo e(route('cabinet.addPostCommentImage')); ?>" enctype="multipart/form-data">
                                        <?php echo e(csrf_field()); ?>

                                        <input type="hidden" name="post_id" value="<?php echo e($post->id); ?>" />
                                        <span><?php echo e(__('cabinet.upload_image')); ?></span>
                                        <label class="label_upload" for="upload<?php echo e(($post->id == 1) ? '' : $post->id); ?>"><?php echo e(__('cabinet.choose_file')); ?></label>
                                        <input id="upload<?php echo e(($post->id == 1) ? '' : $post->id); ?>" class="upload_photo_input_js" type="file" accept="image/*" name="imgs[]" multiple hidden>
                                        <input class="upload_btn" type="submit" value="<?php echo e(__('cabinet.upload')); ?>" />
                                    </form>
                                </div>
                            </div>
                        </div>
					<input class="tags_input_js" type="text" value="<?php echo e($post->getTagsStr()); ?>" data-role="tagsinput" placeholder="<?php echo e(__('cabinet.add_tag')); ?>" />
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php else: ?>
        <div style="padding-left: 110px; padding-top: 40px"><?php echo e(__('home.posts_not_found')); ?></div>

          <p>created by <a href="https://github.com/cosenary/Instagram-PHP-API">cosenary's Instagram class</a>,
                available on GitHub</p>
            <iframe width="95px" scrolling="0" height="20px" frameborder="0" allowtransparency="true"
                    src="http://ghbtns.com/github-btn.html?user=cosenary&repo=Instagram-PHP-API&type=fork&count=true"></iframe>
        
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('additionalDivs'); ?>
    ##parent-placeholder-57a4a7a497539777122e9edc50726cae2eecd1c8##
    <?php if(!empty($posts) && $posts->currentPage() != $posts->lastPage()): ?>
        <button class="btn show_more_btn"><?php echo e(__('home.show_more')); ?></button>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/test/public_html/resources/views/cabinet/showcatalog.blade.php ENDPATH**/ ?>