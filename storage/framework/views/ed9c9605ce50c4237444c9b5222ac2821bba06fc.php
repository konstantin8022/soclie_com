
    <div class="filters">
        <div class="select_wrap ">
            
            <span class="filter_title select_title"><?php echo e(__('home.sort_by.' . (!empty($defaults['filter1']) ? $defaults['filter1'] : $defaults['by']))); ?></span><i class="fa fa-angle-down" aria-hidden="true"></i>
            <div class="select">
                <ul>
                    <li><a href="<?php echo e(\App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['by'=>'instagram'], $defaults)); ?>"><?php echo e(__('home.sort_by.instagram')); ?></a></li>
                    <?php if(isset($withOnlyPosts) && $withOnlyPosts): ?>
                        
                        <li><a href="<?php echo e(\App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['by'=>'only_posts'], $defaults)); ?>"><?php echo e(__('home.sort_by.only_posts')); ?></a></li>
                    <?php endif; ?>
                    <li><a href="<?php echo e(\App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['by'=>'im_likes'], $defaults)); ?>"><?php echo e(__('home.sort_by.im_likes')); ?></a></li>
                    <li><a href="<?php echo e(\App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['by'=>'rating'], $defaults)); ?>"><?php echo e(__('home.sort_by.rating')); ?></a></li>
                    <li><a href="<?php echo e(\App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['by'=>'favorites'], $defaults)); ?>"><?php echo e(__('home.sort_by.favorites')); ?></a></li>
                    <li><a href="<?php echo e(\App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['by'=>'by_comments'], $defaults)); ?>"><?php echo e(__('home.sort_by.by_comments')); ?></a></li>
                </ul>
            </div>
        </div>
        <div class="select_wrap ">
            <span class="filter_title select_title"><?php echo e(__('home.time.' . (!empty($defaults['filter2']) ? $defaults['filter2'] : $defaults['on']))); ?></span><i class="fa fa-angle-down" aria-hidden="true"></i>
            <div class="select">
                <ul>
                    <li><a href="<?php echo e(\App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['on'=>'all'], $defaults)); ?>"><?php echo e(__('home.time.all')); ?></a></li>
                    <li><a href="<?php echo e(\App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['on'=>'week'], $defaults)); ?>"><?php echo e(__('home.time.week')); ?></a></li>
                    <li><a href="<?php echo e(\App\Services\StringService::generateFiltersUrl($numOfLeftSlugs, ['on'=>'month'], $defaults)); ?>"><?php echo e(__('home.time.month')); ?></a></li>
                </ul>
            </div>
        </div>
    </div>

