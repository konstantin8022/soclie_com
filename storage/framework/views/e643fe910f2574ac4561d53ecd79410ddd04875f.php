<?php $__env->startSection('content'); ?>
    ##parent-placeholder-040f06fd774092478d450774f5ba30c5da78acc8##
 	
    <?php echo $__env->make('layouts.partials.filters', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
 
   <?php if(count($posts) > 0): ?>
        <div class="post_title">
            <h1 class="post_title_item instagram_photo"><?php echo e(__('home.instagram')); ?></h1>
            <h2 class="post_title_item add_photos"><?php echo e(__('home.additional_photo')); ?></h2>
            <h2 class="post_title_item comment"><?php echo e(__('home.comments')); ?></h2>
        </div>
        <?php echo $__env->make('home.partials.pagination-item', $posts, array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php else: ?>
        <div style="padding-left: 110px; padding-top: 40px"><?php echo e(__('home.posts_not_found')); ?></div>
    <?php endif; ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('additionalDivs'); ?>
    ##parent-placeholder-57a4a7a497539777122e9edc50726cae2eecd1c8##
    <?php if(!empty($posts) && $posts->currentPage() != $posts->lastPage()): ?>
        <button class="btn show_more_btn"><?php echo e(__('home.show_more')); ?></button>
    <?php endif; ?>
<?php $__env->stopSection(); ?>
