<div class="post_comment <?php echo e($parent_id != 0 ? 'comment_answer' : ''); ?>"><span id="comment<?php echo e($comment->id); ?>"></span>
    <div class="post_comment_hader">
        <span class="post_comment_rate">0</span>

        <span>
			<i class="fa fa-arrow-down dislike_btn" aria-hidden="true"></i>
            <i class="fa fa-arrow-up like_btn" aria-hidden="true"></i>
		</span>

        <img src="<?php echo e($comment->user->getAvatarSrc()); ?>" alt="user_pick">

        <span class="post_comment_autor"><a href="<?php echo e(route('index', $comment->user->i_login)); ?>"><?php echo e($comment->user->i_login); ?></a></span>
        <span><?php echo e($comment->user->rating); ?></span>
        <span><?php echo e($comment->getFormattedTimeAgoString()); ?></span>
    </div>

    <p><?php echo $comment->content; ?></p>

    <div class="comment reply_block hidden">
        <div class="comment_block">
            <input type="hidden" name="post_id" value="<?php echo e($post->id); ?>" />
            <input type="hidden" name="comment_id" value="<?php echo e($comment->id); ?>" />
            <input type="hidden" name="parent_id" value="<?php echo e($parent_id); ?>" />
            <div class="comment_text_field text_container" contenteditable="true"></div>
            <div class="comment_action_block">
                <div class="comment_action_icons hidden">
                    <i class="flaticon-piggy-bank" aria-hidden="true"></i>
                    <i class="flaticon-musical-note" aria-hidden="true"></i>
                    <i class="flaticon-chat" aria-hidden="true"></i>
                    <i class="flaticon-headphones" aria-hidden="true"></i>
                </div>

                <div class="comment_action_btn_block">
                    <i class="flaticon-smile smiles_dock_btn" aria-hidden="true"></i>
                    <i class="flaticon-photo-camera upload_photo_btn" aria-hidden="true"></i>
                </div>

                <div class="comment_modal hidden">
                    <span><?php echo e(__('cabinet.youtube_link')); ?></span>
                    <input type="text" name="youtube_link" placeholder="<?php echo e(__('cabinet.paste_link')); ?>">
                    <button class="upload_btn upload_video_js"><?php echo e(__('cabinet.upload_video')); ?></button>
                    <form method="post" class="ajax_form" action="<?php echo e(route('cabinet.addCommentImage')); ?>" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <input type="hidden" name="comment_id" value="" />
                        <span><?php echo e(__('cabinet.upload_image')); ?></span>
                        <label class="label_upload" for="upload<?php echo e($comment->id); ?>"><?php echo e(__('cabinet.choose_file')); ?></label>
                        <input id="upload<?php echo e($comment->id); ?>" class="upload_photo_input_js" type="file" accept="image/*" name="imgs[]" multiple hidden>
                        <input class="upload_btn" type="submit" value="<?php echo e(__('cabinet.upload')); ?>" />
                    </form>
                </div>
            </div>

            <?php if($parent_id == 0): ?>
                <div class="comment_btn send_btn send_btn_js"><?php echo e(__('home.comment_send')); ?></div>
                <div class="comment_btn cansel_btn cancel_btn_js"><?php echo e(__('home.comment_cancel')); ?></div>
            <?php endif; ?>
        </div>
    </div>
    <?php if($parent_id == 0): ?>
        <div class="comment_btn reply_btn"><?php echo e(__('home.comment_answer')); ?></div>
    <?php endif; ?>
</div>
