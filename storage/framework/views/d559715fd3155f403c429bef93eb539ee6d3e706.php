<?php $__env->startSection('var_title', 'Профиль пользователя ' . $user->i_login); ?>
<?php $__env->startSection('var_bodyClass', 'profile user_profile'); ?>

<?php echo $__env->make('layouts.partials.posts-list', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('layouts.partials.user', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>






<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/test/public_html/resources/views/home/showprofile.blade.php ENDPATH**/ ?>