<?php $__env->startSection('var_title', 'Рейтинг'); ?>
<?php $__env->startSection('var_bodyClass', 'rating'); ?>

<?php $__env->startSection('content'); ?>
        
        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <a class="user" href="<?php echo e(route('index', $user->i_login)); ?>">
            <div class="user_order_number"><?php echo e($loop->iteration); ?></div>
            <div class="user_pick">
                <img src="<?php echo e($user->getAvatarSrc()); ?>" alt="user_pick">
            </div>
            <div class="user_info_item user_name">
                <h2><span><?php echo e($user->i_login); ?></span><span><?php echo e($user->i_real_name); ?></span></h2>
            </div>
            <div class="user_info info_descr">
                <div class="user_info_item">
                    <div class="user_info_descr">
                        <div><span><i class="fa fa-star-o" aria-hidden="true"></i><?php echo e($user->rating); ?></span> <?php echo e(__('cabinet.rating')); ?></div>
                        <div><span><?php echo e($user->getAllOnlyPosts()->count()); ?></span> <?php echo e(__('cabinet.posts')); ?></div>
                        <div><span><?php echo e($user->followingUsers()->count()); ?></span> <?php echo e(__('cabinet.followers_count')); ?></div>
                        <div><span><?php echo e($user->getPostsFavoritesCount()); ?></span> <?php echo e(__('cabinet.favorites_count')); ?></div>
                    </div>
                </div>
                <div class="user_info_item">
                    <div class="user_info_descr inst_item">
                        <div><span><?php echo e($user->i_publications); ?></span> <?php echo e(__('cabinet.instagram_media')); ?></div>
                        <div><span><?php echo e($user->i_count_subscribers); ?></span> <?php echo e(__('cabinet.followers_count')); ?></div>
                        <div><span><?php echo e($user->i_count_subscribes); ?></span> <?php echo e(__('cabinet.follows_count')); ?></div>
                        <div>instagram</div>
                    </div>
                </div>
            </div>
        </a>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('additionalDivs'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>