<div class="slider-item">
    <div class="slide_img">
        <img src="<?php echo e($src); ?>" alt="slide" data-index="<?php echo e($y); ?>" />
    </div>
    <div class="slide_text_wrap">
        <span class="photo_title"><?php echo e(__('home.modal_instagram_photo')); ?></span>
        <div class="slide_text_header">
            <div class="slide_text_img">
                <a href="<?php echo e(route('index', $post->user->i_login)); ?>"><img src="<?php echo e($post->user->getAvatarSrc()); ?>" alt="<?php echo e($post->user->i_login); ?>"></a>
            </div>
            <div class="slide_header_text">
                <a href="<?php echo e(route('index', $post->user->i_login)); ?>"><p class="slide_username"><?php echo e($post->user->i_login); ?></p></a>
            </div>
            <span class="slide_header_date">
                <?php echo e(date('d.m.Y H:i:s', $post->created_at->timestamp)); ?>

            </span>
        </div>
        <?php echo $__env->make('layouts.partials.modal-right', ['post'=>$post], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
</div>