<?php $__currentLoopData = $replies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $reply): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="post_comment comment_answer"><span id="comment<?php echo e($reply->id); ?>"></span>
        <div class="post_comment_hader">
            <span class="post_comment_rate <?php echo e($reply->rating > 0  ? 'positive' : ($reply->rating != 0 ? 'negative' : '')); ?>"><?php echo e($reply->rating); ?></span>

            <span>
                <i class="fa fa-arrow-down dislike_btn <?php echo e($reply->isUserDislikeFound() ? 'active' : ''); ?>" aria-hidden="true"></i>
                <i class="fa fa-arrow-up like_btn <?php echo e($reply->isUserLikeFound() ? 'active' : ''); ?>" aria-hidden="true"></i>
			</span>

            <img src="<?php echo e($reply->user->getAvatarSrc()); ?>" alt="user_pick">

            <span class="post_comment_autor"><a href="<?php echo e(route('index', $reply->user->i_login)); ?>"><?php echo e($reply->user->i_login); ?></a></span>
            <span><?php echo e($reply->user->rating); ?></span>
            <span><?php echo e($reply->getFormattedTimeAgoString()); ?></span>
        </div>

        <p><?php echo $reply->content; ?></p>

        <div class="comment reply_block hidden">
            <div class="comment_block">
                <input type="hidden" name="post_id" value="<?php echo e($post->id); ?>" />
                <input type="hidden" name="comment_id" value="<?php echo e($reply->id); ?>" />
                <input type="hidden" name="parent_id" value="<?php echo e($reply->parent->id); ?>" />

                <div class="comment_text_field text_container" contenteditable="true"></div>

                <div class="comment_action_block">
                    <div class="comment_action_icons hidden">
                        <i class="flaticon-piggy-bank" aria-hidden="true"></i>
                        <i class="flaticon-musical-note" aria-hidden="true"></i>
                        <i class="flaticon-chat" aria-hidden="true"></i>
                        <i class="flaticon-headphones" aria-hidden="true"></i>
                    </div>
                    <div class="comment_action_btn_block">
                        <i class="flaticon-smile smiles_dock_btn" aria-hidden="true"></i>
                        <i class="flaticon-photo-camera upload_photo_btn" aria-hidden="true"></i>
                    </div>
                    <div class="comment_modal hidden">
                        <span><?php echo e(__('cabinet.youtube_link')); ?></span>
                        <input type="text" name="youtube_link" placeholder="<?php echo e(__('cabinet.paste_link')); ?>">
                        <button class="upload_btn upload_video_js"><?php echo e(__('cabinet.upload_video')); ?></button>
                        <form method="post" class="ajax_form" action="<?php echo e(route('cabinet.addCommentImage')); ?>" enctype="multipart/form-data">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" name="comment_id" value="" />
                            <span><?php echo e(__('cabinet.upload_image')); ?></span>
                            <label class="label_upload" for="upload<?php echo e($reply->id); ?>"><?php echo e(__('cabinet.choose_file')); ?></label>
                            <input id="upload<?php echo e($reply->id); ?>" class="upload_photo_input_js" type="file" accept="image/*" name="imgs[]" multiple hidden>
                            <input class="upload_btn" type="submit" value="<?php echo e(__('cabinet.upload')); ?>" />
                        </form>
                    </div>
                </div>

                <?php if($reply->parent->id == 0): ?>
                    <div class="comment_btn send_btn send_btn_js"><?php echo e(__('home.comment_send')); ?></div>
                    <div class="comment_btn cansel_btn "><?php echo e(__('home.comment_cancel')); ?></div>
                <?php endif; ?>
            </div>
        </div>

        <?php if($reply->parent->id == 0): ?>
            <div class="comment_btn reply_btn"><?php echo e(__('home.comment_answer')); ?></div>
        <?php endif; ?>
    <?php if($reply->replies->count()): ?>
        <?php echo $__env->make('home.partials.replies', ['replies' => $reply->replies], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endif; ?>
    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
