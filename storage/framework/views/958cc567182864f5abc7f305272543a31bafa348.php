<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0, width=device-width, shrink-to-fit=no">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

        <title><?php echo $__env->yieldContent('var_title'); ?></title>

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous"> 
        <link rel="stylesheet" href="<?php echo e(asset('fonts/font-awesome/css/font-awesome.min.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('fonts/ubuntu/font.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('fonts/icons/flaticon.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('fonts/nautiluspompilius/font.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('css/jquery-ui.min.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('css/owl.carousel.css')); ?>">
        <link rel="stylesheet" href="<?php echo e(asset('css/slider-pro.min.css')); ?>"/>
        <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap-tagsinput.css')); ?>"/>
        <link rel="stylesheet" href="<?php echo e(asset('css/easy-autocomplete.min.css')); ?>"/>
        <link rel="stylesheet" href="<?php echo e(asset('css/dropzone.css')); ?>"/>
        <link rel="stylesheet" href="<?php echo e(asset('css/style.css?4')); ?>">
        
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"
                integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
                crossorigin="anonymous">
        </script>
        <script src="<?php echo e(asset('js/jquery-ui.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/owl.carousel.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/jquery.sliderPro.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/bootstrap-tagsinput.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/jquery.easy-autocomplete.min.js')); ?>"></script>
        <script src="<?php echo e(asset('js/dropzone.js')); ?>"></script>
        <script src="<?php echo e(asset('js/main.js?7')); ?>"></script>

        <link  href="<?php echo e(asset('js/fancybox/dist/jquery.fancybox.css')); ?>" rel="stylesheet">
        <script src="<?php echo e(asset('js/fancybox/dist/jquery.fancybox.js')); ?>"></script>

        <!-- FAVICON -->
       <link rel="apple-touch-icon" sizes="180x180" href="<?php echo e(asset('apple-touch-icon.png')); ?>">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo e(asset('favicon-32x32.png')); ?>">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo e(asset('favicon-16x16.png')); ?>">
        <link rel="manifest" href="<?php echo e(asset('manifest.json')); ?>">
        <link rel="mask-icon" href="<?php echo e(asset('safari-pinned-tab.svg')); ?>" color="#5bbad5">
        <meta name="theme-color" content="#ffffff">

    </head>
    <body class="<?php echo $__env->yieldContent('var_bodyClass'); ?>">

        <?php echo $__env->make('layouts.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

       
 

        <section>
            <div class="container">
                <?php echo $__env->yieldContent('content'); ?>
            </div>
        </section>
        <?php $__env->startSection('additionalDivs'); ?>
        <?php echo $__env->yieldSection(); ?>

        <?php echo $__env->make('layouts.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    </body>
</html>
