
    <div class="modal hidden">
        <div class="modal_back"><i class="fa fa-times close_modal" aria-hidden="true"></i></div>
        <div class="modal_content">
            <div class="owl-carousel owl-theme modal_slider">

                    <?php if(!empty($carouselSrcs)): ?>
                    <?php $c = 0; ?>
                    <?php $__currentLoopData = $carouselSrcs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $src): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php $c++; ?>
                    <div class="slider-item">
                        <div class="slide_img">
                            <img src="<?php echo e($src); ?>" data-index="<?php echo e($c); ?>" alt="slide"/>
                        </div>
                        <div class="slide_text_wrap">
                            <span class="photo_title"><?php echo e(__('home.modal_instagram_photo')); ?></span>
                            <div class="slide_text_header">
                                <div class="slide_text_img">
                                    <a href="<?php echo e(route('index', $post->user->i_login)); ?>"><img src="<?php echo e($post->user->getAvatarSrc()); ?>" alt="user_pick"></a>
                                </div>
                                <div class="slide_header_text">
                                    <a href="<?php echo e(route('index', $post->user->i_login)); ?>"><p class="slide_username"><?php echo e($post->user->i_login); ?></p></a>
                                </div>
                                <span class="slide_header_date">
                                    <?php echo e(date('d.m.Y H:i:s', $post->created_at->timestamp)); ?>

                                </span>
                            </div>
                            <?php echo $__env->make('layouts.partials.modal-right', ['post'=>$post], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>

                    <?php $c = 0; ?>
                    <?php $__currentLoopData = $imagePhotos; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $c++; ?>
                        <?php if($loop->first && is_object($photo) && $photo->isInstagramCarousel()): ?> <?php continue; ?> <?php endif; ?>
                        <div class="slider-item">
                            <div class="slide_img">
                                <img src="<?php echo e(isset($mode) && $mode == 'carousel' ? $photo : $photo->getImageSrc()); ?>" data-index="<?php echo e($c); ?>" alt="slide"/>
                            </div>
                            <div class="slide_text_wrap">
                                <span class="photo_title"><?php echo e(is_object($photo) && $photo->isSiteImage() ? __('home.modal_additional_photo') : __('home.modal_instagram_photo')); ?></span>
                                <div class="slide_text_header">
                                    <div class="slide_text_img">
                                        <a href="<?php echo e(route('index', $post->user->i_login)); ?>"><img src="<?php echo e($post->user->getAvatarSrc()); ?>" alt="user_pick"></a>
                                    </div>
                                    <div class="slide_header_text">
                                        <a href="<?php echo e(route('index', $post->user->i_login)); ?>"><p class="slide_username"><?php echo e($post->user->i_login); ?></p></a>
                                    </div>
                                    <span class="slide_header_date">
                                        <?php if(is_object($photo) && $photo->isSiteImage()): ?>
                                            <?php echo e(date('d.m.Y H:i:s', $photo->created_at->timestamp)); ?>

                                        <?php elseif(is_object($photo) && !$photo->isSiteImage()): ?>
                                            <?php echo e(date('d.m.Y H:i:s', $photo->i_created_time)); ?>

                                        <?php else: ?>
                                            <?php echo e(date('d.m.Y H:i:s', $post->created_at->timestamp)); ?>

                                        <?php endif; ?>
                                        
                                    </span>
                                </div>
                                <?php echo $__env->make('layouts.partials.modal-right', ['post'=>$post], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                            </div>
                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </div>