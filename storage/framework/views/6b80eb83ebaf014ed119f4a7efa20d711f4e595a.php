<?php $__env->startSection('var_title', 'Мой профиль'); ?>
<?php $__env->startSection('var_bodyClass', 'profile'); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('cabinet.partials.profilemenu', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('layouts.partials.user', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('cabinet.partials.aboutme', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>  	 
<?php $__env->stopSection(); ?>

<?php $__env->startSection('additionalDivs'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>