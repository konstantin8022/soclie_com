<?php $__env->startSection('var_title', 'Мой профиль'); ?>
<?php $__env->startSection('var_bodyClass', 'profile'); ?>

<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('cabinet.partials.profilemenu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->make('layouts.partials.user', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('additionalDivs'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/test/public_html/resources/views/cabinet/index.blade.php ENDPATH**/ ?>