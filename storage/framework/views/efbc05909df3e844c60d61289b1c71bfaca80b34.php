<?php $__empty_1 = true; $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
    <div id="post<?php echo e($post->id); ?>" class="post modal_block">
        <?php $x=0; ?>
        <div class="post_item_block instagram_photo">
            
            <h2 class="post_title_item_mob">modal_block<?php echo e($post->user->i_login); ?></h2>
            <?php echo $__env->make('layouts.partials.icons', ['post'=>$post], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <div class="photo">
                <div class="photo_wrap">
                    <div class="loader">
                        <div class="spiner"></div>
                    </div>
                    <?php $carouselSrcs = []; ?>

                    <?php $__currentLoopData = $post->instagramPhotos()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="owl-carousel owl-theme instagram_slider">
                        <?php if($photo->isInstagramImage()): ?>
                            <?php $x++; ?>
                            <img class="open_modal owl-lazy" src="<?php echo e($photo->getThumbnailSrc()); ?>" data-src="<?php echo e($photo->getThumbnailSrc()); ?>"  data-index="<?php echo e($x); ?>" alt="inst_img">
                        <?php endif; ?>

                        <?php if($photo->isInstagramCarousel()): ?>
                                <?php $__currentLoopData = $photo->getInstagramCarouselSrcs(true); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $src): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php $x++; ?>
                                    <?php $carouselSrcs[] = $src; ?>
                                    <div class="slider-item">
                                        
                                        <img class="open_modal owl-lazy" src="<?php echo e($src); ?>"  data-src="<?php echo e($src); ?>" data-index="<?php echo e($x); ?>" alt="inst_img">
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <div class="instagram_album_ico">
                                <i class="fa fa-clone" aria-hidden="true"></i>
                            </div>
                        <?php endif; ?>

                        <?php $__empty_2 = true; $__currentLoopData = $post->imagePhotos()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_2 = false; ?>
                            <?php $x++; ?>

                            <img class="open_modal owl-lazy" src="<?php echo e($photo->getImageSrc()); ?>" data-src="<?php echo e($photo->getImageSrc()); ?>" data-index="<?php echo e($x); ?>" alt="add_img">
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_2): ?>
                            
                        <?php endif; ?>

                        </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <?php echo $__env->make('layouts.partials.photo-info', ['post'=>$post], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            
            
            
            
            
            
            
            
        </div>
        
        <div class="post_item_block add_photos">
            <?php if($post->imagePhotos()->count()): ?>
                <h2 class="post_title_item_mob"><?php echo e(__('home.additional_photo')); ?></h2>
            <?php endif; ?>
            <div class="photos_block">
                <?php $__empty_2 = true; $__currentLoopData = $post->imagePhotos()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_2 = false; ?>
                    <?php $x++; ?>
                    <div class="add_img_wrap">
                        <img class="open_modal" src="<?php echo e($photo->getThumbnailSrc()); ?>" data-index="<?php echo e($x); ?>" alt="add_img" thumb="true">
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_2): ?>
                    
                <?php endif; ?>
            </div>
            <?php if(!empty($carouselSrcs)): ?>
                <?php echo $__env->make('home.partials.modal', ['carouselSrcs'=>$carouselSrcs, 'imagePhotos' => $post->imagePhotos, 'post'=>$post], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php else: ?>
                <?php echo $__env->make('home.partials.modal', ['imagePhotos' => $post->instagramAndImagePhotos, 'post'=>$post], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <?php endif; ?>
        </div>
        



        <div class="post_item_block comment">
            <?php if(($content = $post->description->content)): ?>
                <h2 class="post_title_item_mob"><?php echo e(__('home.comments')); ?></h2>
            <?php endif; ?>
            <div class="text">
                
                <div class="text_wrap"><?php echo $post->description->content; ?></div>
                <a href="<?php echo e(route('showpost', $post->getLinkPart())); ?>"><?php echo e(__('home.read_more')); ?></a>
            </div>
            <?php echo $__env->make('layouts.partials.tags', ['tags'=>$post->tags], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        </div>

        

    </div>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
<?php endif; ?>
<?php /**PATH /home/test/public_html/resources/views/home/partials/pagination-item.blade.php ENDPATH**/ ?>