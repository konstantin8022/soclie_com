<?php $__env->startSection('var_title', 'Пост №' . $post->id); ?>
<?php $__env->startSection('var_bodyClass', 'full_post'); ?>

<?php $__env->startSection('content'); ?>
    <div class="photo_info">
        <div>
            <span><a href="<?php echo e(route('index', $post->user->i_login)); ?>"><?php echo e($post->user->i_login); ?></a></span>
            <span><i class="fa fa-heart" aria-hidden="true"></i><?php echo e($post->i_likes_count); ?></span>
            <span><i class="fa fa-comment" aria-hidden="true"></i><?php echo e($post->i_comments_count); ?></span>
            <span><i class="fa fa-star-o" aria-hidden="true"></i><?php echo e($post->user->rating); ?></span>
        </div>
    </div>
    <?php echo $__env->make('layouts.partials.icons', ['post'=>$post], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<p> slidebar new</p>

    <div class="post_slider slider_inst">
        <div class="slider-pro modal_block" id="my-slider">

            <?php $x=0; ?>
            <?php $y=0; ?>

            <?php $__currentLoopData = $post->instagramPhotos()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                <?php if($photo->isInstagramImage()): ?>

                    <?php $x++; ?>
                    <?php $y++; ?>
                    <div class="sp-slides">
                        <div class="sp-slide">
                            
                                <img class="open_modal_2 sp-image" src="<?php echo e($photo->getImageSrc()); ?>" data-index="1" alt="slide" />
<p>open modal</p>
                                <?php $__env->startPush('modal-item'); ?>
                                    <?php echo $__env->make('home.partials.modal-items', ['src'=>$photo->getImageSrc(), 'post'=>$post], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                <?php $__env->stopPush(); ?>
                            
                            <img class="sp-thumbnail" src="<?php echo e($photo->getImageSrc()); ?>" alt="slide_thumb"/>
                        </div>
                    </div>

                    <?php echo $__env->make('home.partials.modal', ['imagePhotos' => $post->instagramPhotos, 'post'=>$post], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

                <?php elseif($photo->isInstagramCarousel()): ?>
                    <div class="sp-slides">
                        <?php $__currentLoopData = $photo->getInstagramCarouselSrcs(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $src): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php $x++; ?>
                            <?php $y++; ?>
                            <div class="sp-slide">
                                
                                <img class="open_modal_2 sp-image" src="<?php echo e($src); ?>" data-index="<?php echo e($x); ?>" alt="slide"/>
                                <?php $__env->startPush('modal-item'); ?>
                                    <?php echo $__env->make('home.partials.modal-items', ['src'=>$src, 'post'=>$post], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                                <?php $__env->stopPush(); ?>
                                
                                <img class="sp-thumbnail" src="<?php echo e($src); ?>" alt="slide_thumb"/>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                    <div class="slider-nav-btn prew-btn"><img src="<?php echo e(asset('img/arr_left.png')); ?>" alt="arr_left"></div>
                    <div class="slider-nav-btn next-btn"><img src="<?php echo e(asset('img/arr_right.png')); ?>" alt="arr_right"></div>

                    

                <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <?php echo $__env->yieldPushContent('modal'); ?>
        </div>
        <div class="photo_info_mob">
            <div>
                <span><a href="<?php echo e(route('index', $post->user->i_login)); ?>"><?php echo e($post->user->i_user); ?></a></span>
                <span><i class="fa fa-heart" aria-hidden="true"></i><?php echo e($post->i_likes_count); ?></span>
                <span><i class="fa fa-comment" aria-hidden="true"></i><?php echo e($post->i_comments_count); ?></span>
                <span><i class="fa fa-star-o" aria-hidden="true"></i><?php echo e($post->rating); ?></span>
            </div>
        </div>
        <!--div class="tags hidden">
            <div>Солнце</div>
            <div>Пляж</div>
        </div-->
    </div>


    <!-- SLIDER-2 -->
    <div class="post_slider slider_new_inst">
        <div class="slider-pro modal_block" id="my-slider2">
            <div class="sp-slides">
                <?php $__empty_1 = true; $__currentLoopData = $post->imagePhotos()->get(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $photo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                    <?php $x++; ?>
                    <?php $y++; ?>
                    <div class="sp-slide">
                        
                        <img class="open_modal_2 sp-image" src="<?php echo e($photo->getImageSrc()); ?>" alt="slide" data-index="<?php echo e($x); ?>" />
                        <?php $__env->startPush('modal-item'); ?>
                            <?php echo $__env->make('home.partials.modal-items', ['src'=>$photo->getImageSrc(), 'post'=>$post], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <?php $__env->stopPush(); ?>
                        
                        <img class="sp-thumbnail" src="<?php echo e($photo->getImageSrc()); ?>" alt="slide_thumb"/>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
                    <?php $x++; ?>
                    <?php $y++; ?>
                    <div class="sp-slide">
                        
                        <img class="open_modal_2 sp-image" src="<?php echo e(asset('uploads/no-image.png')); ?>" data-index="<?php echo e($x); ?>" alt="slide"/>
                        <?php $__env->startPush('modal-item'); ?>
                            <?php echo $__env->make('home.partials.modal-items', ['src'=>asset('uploads/no-image.png'), 'post'=>$post], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <?php $__env->stopPush(); ?>
                        
                    </div>
                <?php endif; ?>
            </div>
            <!-- <div class="slider-resize-btn"><img src="img/full_size.png" alt="arr_left"></div> -->
            <div class="slider-nav-btn prew-btn"><img src="<?php echo e(asset('img/arr_left.png')); ?>" alt="arr_left"></div>
            <div class="slider-nav-btn next-btn"><img src="<?php echo e(asset('img/arr_right.png')); ?>" alt="arr_right"></div>


                





        </div>
        <?php echo $__env->make('layouts.partials.tags', ['tags' => $post->tags], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>

    <div class="full_common_modal">
<p>hhhhhhhhhhhhhhhhhhhhh</p>
        <?php echo $__env->make('home.partials.modal-with-stack-items', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>





    <div class="post_descr">
        <h2><?php echo e(__('home.post_description')); ?></h2>
        <p><?php echo $post->description->content; ?></p>
    </div>
    <div class="post_comments">
        <?php $__empty_1 = true; $__currentLoopData = $comments; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $comment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
            <div class="post_comment"><span id="comment<?php echo e($comment->id); ?>"></span>
                <div class="post_comment_hader">
                    <span class="post_comment_rate <?php echo e($comment->rating > 0  ? 'positive' : ($comment->rating != 0 ? 'negative' : '')); ?>"><?php echo e($comment->rating); ?></span>
                    <span>
							<i class="fa fa-arrow-down <?php echo e(Auth::check() ? 'dislike_btn' : ''); ?> <?php echo e($comment->isUserDislikeFound() ? 'active' : ''); ?>" aria-hidden="true"></i>
							<i class="fa fa-arrow-up <?php echo e(Auth::check() ? 'like_btn' : ''); ?> <?php echo e($comment->isUserLikeFound() ? 'active' : ''); ?>" aria-hidden="true"></i>
						</span>
                    <img src="<?php echo e($comment->user->getAvatarSrc()); ?>" alt="user_pick">
                    <span class="post_comment_autor"><a href="<?php echo e(route('index', $comment->user->i_login)); ?>"><?php echo e($comment->user->i_login); ?></a></span>
                    <span><?php echo e($comment->user->rating); ?></span>
                    <span><?php echo e($comment->getFormattedTimeAgoString()); ?></span>
                </div>
                <p><?php echo $comment->content; ?></p>
                <div class="comment reply_block hidden">
                    <div class="comment_block">
                        <input type="hidden" name="post_id" value="<?php echo e($post->id); ?>" />
                        <input type="hidden" name="comment_id" value="<?php echo e($comment->id); ?>" />
                        <input type="hidden" name="parent_id" value="0" />
                        <div class="comment_text_field text_container" contenteditable="true"></div>
                        <div class="comment_action_block">
                            <div class="comment_action_icons">
                                <i class="flaticon-piggy-bank" aria-hidden="true"></i>
                                <i class="flaticon-musical-note" aria-hidden="true"></i>
                                <i class="flaticon-chat" aria-hidden="true"></i>
                                <i class="flaticon-headphones" aria-hidden="true"></i>
                            </div>
                            <div class="comment_action_btn_block">
                                <i class="flaticon-smile smiles_dock_btn" aria-hidden="true"></i>
                                <i class="flaticon-photo-camera upload_photo_btn" aria-hidden="true"></i>
                            </div>
                            <div class="comment_modal hidden">
                                <span><?php echo e(__('cabinet.youtube_link')); ?></span>
                                <input type="text" name="youtube_link" placeholder="<?php echo e(__('cabinet.paste_link')); ?>">
                                <button class="upload_btn upload_video_js"><?php echo e(__('cabinet.upload_video')); ?></button>
                                <form method="post" class="ajax_form" action="<?php echo e(route('cabinet.addCommentImage')); ?>" enctype="multipart/form-data">
                                    <?php echo e(csrf_field()); ?>

                                    <input type="hidden" name="comment_id" value="" />
                                    <span><?php echo e(__('cabinet.upload_image')); ?></span>
                                    <label class="label_upload" for="upload<?php echo e($comment->id); ?>"><?php echo e(__('cabinet.choose_file')); ?></label>
                                    <input id="upload<?php echo e($comment->id); ?>" class="upload_photo_input_js" type="file" accept="image/*" name="imgs[]" multiple hidden>
                                    <input class="upload_btn" type="submit" value="<?php echo e(__('cabinet.upload')); ?>" />
                                </form>
                            </div>
                        </div>
                        <div class="comment_btn send_btn send_btn_js"><?php echo e(__('home.comment_send')); ?></div>
                        <div class="comment_btn cansel_btn"><?php echo e(__('home.comment_cancel')); ?></div>
                    </div>
                </div>
                <?php if(Auth::check()): ?>
			<p>REplayBTN</p>
                    <div class="comment_btn reply_btn"><?php echo e(__('home.comment_answer')); ?></div>
                <?php endif; ?>
                <?php if($comment->replies->count()): ?>
                    <?php echo $__env->make('home.partials.replies', ['replies' => $comment->replies], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                <?php endif; ?>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <div class="post_comment"><?php echo e(__('home.comments_empty')); ?></div>
        <?php endif; ?>
    </div>
    <div class="comment reply_block post_comment new_comment">
        <div class="comment_block">
            <?php if(Auth::check()): ?>
                <h2><?php echo e(__('home.comment_new_comment')); ?></h2>
            <!-- <form action="<?php echo e(route('comments.store')); ?>" method="post"> -->
            <!-- <?php echo e(csrf_field()); ?> -->
                <input type="hidden" name="post_id" value="<?php echo e($post->id); ?>" />
                <input type="hidden" name="parent_id" value="0" />
            <!-- <textarea class="comment_text_field" name="comment" cols="30" rows="10" placeholder="<?php echo e(__('home.comment_textarea_placeholder')); ?>"></textarea> -->
                <div class="comment_text_field text_container" contenteditable="true"></div>
                <div class="comment_action_block">
                    <div class="comment_action_icons hidden">
                        <i class="flaticon-piggy-bank" aria-hidden="true"></i>
                        <i class="flaticon-musical-note" aria-hidden="true"></i>
                        <i class="flaticon-chat" aria-hidden="true"></i>
                        <i class="flaticon-headphones" aria-hidden="true"></i>
                    </div>
                    <div class="comment_action_btn_block">
                        <i class="flaticon-smile smiles_dock_btn" aria-hidden="true"></i>
                        <i class="flaticon-photo-camera upload_photo_btn" aria-hidden="true"></i>
                    </div>
                    <div class="comment_modal hidden">
                        <span><?php echo e(__('cabinet.youtube_link')); ?></span>
                        <input type="text" name="youtube_link" placeholder="<?php echo e(__('cabinet.paste_link')); ?>">
                        <button class="upload_btn upload_video_js"><?php echo e(__('cabinet.upload_video')); ?></button>
                        <form method="post" class="ajax_form" action="<?php echo e(route('cabinet.addCommentImage')); ?>" enctype="multipart/form-data">
                            <?php echo e(csrf_field()); ?>

                            <input type="hidden" name="comment_id" value="" />
                            <span><?php echo e(__('cabinet.upload_image')); ?></span>
                            <label class="label_upload" for="upload"><?php echo e(__('cabinet.choose_file')); ?></label>
                            <input id="upload" class="upload_photo_input_js" type="file" accept="image/*" name="imgs[]" multiple hidden>
                            <input class="upload_btn" type="submit" value="<?php echo e(__('cabinet.upload')); ?>" />
                        </form>
                    </div>
                </div>
                <div class="comment_btn send_btn send_btn_js"><?php echo e(__('home.comment_send')); ?></div>
            <!-- div class="comment_btn cansel_btn"><?php echo e(__('home.comment_cancel')); ?></div -->
                <!-- </form> -->
            <?php else: ?>
                <h2><?php echo e(__('home.comment_need_authorize')); ?></h2>
            <?php endif; ?>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('additionalDivs'); ?>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/test/public_html/resources/views/home/showpost.blade.php ENDPATH**/ ?>