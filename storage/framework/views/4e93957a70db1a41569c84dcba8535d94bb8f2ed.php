<?php $__env->startSection('content'); ?>
    <div class="auth_user container">
        <h1><?php echo e(__('auth.resetpassword_title')); ?></h1>

        <?php if(session('status')): ?>
            <div class="info-block success">
                <?php echo e(session('status')); ?>

            </div>    
        <?php endif; ?>

        <form method="POST" action="<?php echo e(route('password.email')); ?>">
            <?php echo e(csrf_field()); ?>


            <div class="form-item">
                <label for="email_or_login"><?php echo e(__('auth.resetpassword_label')); ?></label>
                <input id="email_or_login" type="text" name="email_or_login" value="<?php echo e(old('email_or_login')); ?>" required>
                <?php if($errors->has('email_or_login')): ?>
                    <span class="help-block has-error">
                        <?php echo e($errors->first('email_or_login')); ?>

                    </span>
                <?php endif; ?>
            </div>

            <div class="form-item">
                <button type="submit" class="btn"><?php echo e(__('auth.sendresetlink_btn')); ?></button>
            </div>    
        </form>    
    </div>    
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>